package ttg.gelesneeuw.nswatchproxy.internalresponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCStatsResponse extends RPCResponseBase {
	
	public static class UpdateStatsItem {
		@JsonProperty
		public Date timestamp;
		@JsonProperty
		public long departure;
		@JsonProperty
		public long station;
	}
	
	private List<UpdateStatsItem> _updateStats = new ArrayList<UpdateStatsItem>();

	public List<UpdateStatsItem> getUpdateStatsItems() { return _updateStats; }
	public RPCStatsResponse setUpdateStatsItems( List<UpdateStatsItem> value ) { _updateStats = value; return this; }

	public RPCStatsResponse() {
	}
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCStatsResponse {}", parent );
	}
}
