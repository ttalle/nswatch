package ttg.gelesneeuw.nswatchproxy.internalresponse;

import java.util.List;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatchproxy.internaldata.LogItem;

public class RPCLogResponse extends RPCResponseBase {
	private List<LogItem> _logs = null;

	public List<LogItem> getLogItems() { return _logs; }
	public RPCLogResponse setLogItems( List<LogItem> value ) { _logs = value; return this; }

	public RPCLogResponse() {
	}
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCLogResponse { logs: %s }", parent, ( null == _logs ? "null" : String.valueOf( _logs.size() ) ) );
	}
}
