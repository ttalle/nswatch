package ttg.gelesneeuw.nswatchproxy.internalresponse;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCAdminResponse extends RPCResponseBase {

	public RPCAdminResponse() {
	}
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCAdminResponse { }", parent );
	}
}
