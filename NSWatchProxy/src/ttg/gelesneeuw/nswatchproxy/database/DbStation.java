package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import nl.gelesneeuw.utils.text.StringUtils;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.database.contract.StationContract;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internaldata.Subscription;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.stmt.ArgumentHolder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbStation {
	public static final String TAG = DbStation.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<Station, String> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;
	
	public DbStation() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, Station.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					TableUtils.createTableIfNotExists(_src, Station.class);
		
					_dbcreated = true;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Dao<Station, String> getDao() throws SQLException {
		return _dao;
	}
	
	public List<Station> getStations() {
		List<Station> result = null;
		
		try {
			result = _dao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<Station> getStations( Registration registration ) {
		DbSubscription dbsub = new DbSubscription();
//		DbRegistration dbreg = new DbRegistration();
		DbStation dbstat = new DbStation();
//		DbDeparture dbdep = new DbDeparture();
		
		List<Station> result = null;

		try {
			QueryBuilder<Subscription, Long> qbSubscription = dbsub.getDao().queryBuilder();
	//		QueryBuilder<Registration, Long> dbregistration = dbsub.getDao().queryBuilder();
			QueryBuilder<Station, String> qbStation = dbstat.getDao().queryBuilder();
	//		QueryBuilder<Departure, Long> qbDeparture = dbdep.getDao().queryBuilder();
			
	//		qbStation.join( qbSubscription );
	//		qbDeparture.join(qbStation).orderBy( "realdeparture", true ).where().gt( "realdeparture", new Date() );
	//		result = qbDeparture.query();
			qbSubscription.where().eq( "registration", registration );
			result = qbStation.join( qbSubscription ).query();
		} catch (SQLException e) {
			e.printStackTrace();
		}
				
		return result;
	}
	
	public Station getStation(String id) {
		Station station = null;
		
		try {
			station = _dao.queryForId(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return station;
	}
	
	public Station getStation( double lat, double lon ) {
		/*
SELECT * FROM station
ORDER BY 
ABS( latitude - 52.04561996 ) + ABS( longitude - 4.478859901 ) ASC
LIMIT 1

		 */
		Station station = null;
		
		try {
			ArgumentHolder argLat = new SelectArg(SqlType.DOUBLE, lat);
			ArgumentHolder argLon = new SelectArg(SqlType.DOUBLE, lon);

			List<Station> result = _dao.queryBuilder().orderByRaw("ABS( latitude - ? ) + ABS( longitude - ? ) ASC", argLat, argLon ).limit(1L).query();
			
			if( result.size() == 1 )
				station = result.get(0);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return station;
	}

	public int updateStation( Station station ) {
		int result = 0;
		try {
			result = _dao.update(station);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public void setStations( final List<Station> stations ) {

		if( null == stations ) {
			Log.e( TAG, "setStations: called with stations = null");
			return;
		}
		
		try {
			_dao.callBatchTasks(new Callable<Void>() {
			    public Void call() throws Exception {
			        for (Station s : stations ) {
			        	_dao.createOrUpdate(s);
			        }
					return null;
			    }
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
