package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ttg.gelesneeuw.nswatchproxy.internaldata.UpdateLogItem;
import ttg.gelesneeuw.nswatchproxy.internalresponse.RPCStatsResponse.UpdateStatsItem;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbUpdateLog {
	public static final String TAG = DbUpdateLog.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<UpdateLogItem, Long> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;
	
	public DbUpdateLog() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, UpdateLogItem.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					TableUtils.createTableIfNotExists(_src, UpdateLogItem.class);
		
					_dbcreated = true;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Dao<UpdateLogItem, Long> getDao() throws SQLException {
		return _dao;
	}

	public boolean add( UpdateLogItem item ) {
		boolean result = false;
		
		try {
			_dao.create(item);
			result = true;
		} catch (SQLException e) {
			result = false;
		}
		
		return result;
	}
	
	public List<UpdateLogItem> get() {
		List<UpdateLogItem> result = null;
		
		try {
			result = _dao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public List<UpdateStatsItem> getCounts() {
		List<UpdateStatsItem> items = new ArrayList<UpdateStatsItem>();
		
		try {
			GenericRawResults<Object[]> result = _dao.queryRaw(
//				"SELECT MIN(timestamp) Date, type, COUNT(*) AS Count FROM updatelog GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp), type;",
"SELECT * FROM " +
"( SELECT DATE(timestamp) AS stamp, COUNT(*) AS DepartureCount FROM updatelog WHERE type='departure' GROUP BY DATE(timestamp) ) AS Departures " +
"LEFT OUTER JOIN " +
"( SELECT DATE(timestamp) AS stamp, COUNT(*) AS StationCount FROM updatelog WHERE type='station' GROUP BY DATE(timestamp) ) AS Stations " +
"ON Departures.stamp = Stations.stamp " +
"UNION " +
"SELECT * FROM " +
"( SELECT DATE(timestamp) AS stamp, COUNT(*) AS DepartureCount FROM updatelog WHERE type='departure' GROUP BY DATE(timestamp) ) AS Departures " +
"RIGHT OUTER JOIN " +
"( SELECT DATE(timestamp) AS stamp, COUNT(*) AS StationCount FROM updatelog WHERE type='station' GROUP BY DATE(timestamp) ) AS Stations " +
"ON Departures.stamp = Stations.stamp " +
"WHERE DepartureCount IS NULL",
				new DataType[] { DataType.DATE, DataType.LONG, DataType.DATE, DataType.LONG } );
			
			for (Object[] resultArray : result) {
				UpdateStatsItem item = new UpdateStatsItem();
				
				item.timestamp = (Date)resultArray[0];
				item.departure = (long)resultArray[1];
				item.station = (long)resultArray[3];
				
				items.add( item );
			}
			
			result.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return items;
	}
}
