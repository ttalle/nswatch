package ttg.gelesneeuw.nswatchproxy.database.contract;


public class RegistrationContract implements BaseColumns {
	public static final String TABLE_NAME = "registration";
	public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS '" + TABLE_NAME + "' ('_id' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'key' TEXT NOT NULL , 'name' TEXT NOT NULL, 'device' TEXT NOT NULL )";

	public static final String COLUMN_NAME_KEY = "key";
	public static final String COLUMN_NAME_NAME = "name";
	public static final String COLUMN_NAME_DEVICE = "device";
	
    public static final int COLUMN_INDEX_ID = 1;
    public static final int COLUMN_INDEX_KEY = 2;
    public static final int COLUMN_INDEX_NAME = 3;
    public static final int COLUMN_INDEX_DEVICE = 4;
}
