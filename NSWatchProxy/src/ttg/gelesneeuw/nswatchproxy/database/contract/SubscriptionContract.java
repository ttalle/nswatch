package ttg.gelesneeuw.nswatchproxy.database.contract;


public class SubscriptionContract implements BaseColumns {
	public static final String TABLE_NAME = "subscription";
	public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS 'subscription' ('_id' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'registration' INTEGER NOT NULL , 'station' TEXT NOT NULL )";

	public static final String COLUMN_NAME_REGISTRATION = "registration";
	public static final String COLUMN_NAME_STATION = "station";

    public static final int COLUMN_INDEX_ID = 1;
    public static final int COLUMN_INDEX_REGISTRATION = 2;
    public static final int COLUMN_INDEX_STATION = 3;
}
