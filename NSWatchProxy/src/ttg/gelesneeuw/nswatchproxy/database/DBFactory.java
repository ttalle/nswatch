package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import ttg.gelesneeuw.nswatchproxy.NSWatchConfig;
import ttg.gelesneeuw.nswatchproxy.debug.Log;

import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

public abstract class DBFactory {
	private static final String TAG = DBFactory.class.getName();
	
//	private static final String _connectionString = "jdbc:derby:%s;create=true";
//	private static final String _connectionString = "jdbc:derby:c:/Data/nswatch.db;create=true";
//	private static final String _connectionString = "jdbc:derby:nswatch.db;create=true";
	//private static final String _connectionString = "jdbc:sqlite:c:/Data/nswatch.db";
	/*
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		return getConnection( NSWatchConfig.getInstance().getDatabase() );
	}

	public static Connection getConnection( String source ) throws SQLException, ClassNotFoundException {
		return getConnection(source, new Properties() );
	}

	public static Connection getConnection( String source, String username, String password ) throws SQLException, ClassNotFoundException {
		Properties properties = new Properties();
		
		properties.put("user", username);
		properties.put("password", password );
		
		return getConnection(source, properties );
	}
	
	public static Connection getConnection( String source, Properties properties ) throws SQLException, ClassNotFoundException {
		// load the sqlite-JDBC driver using the current class loader
	    Class.forName("org.sqlite.JDBC");
	    
	    Connection connection = DriverManager.getConnection(source, properties);
	    
	    
	    
//	    Statement statement = connection.createStatement();
//	    statement.setQueryTimeout(30);  // set timeout to 30 sec.

		return connection;
	}
	*/
	
	private static JdbcPooledConnectionSource _pool = null;
	private static Object _lock = new Object();
	
	public static ConnectionSource getConnectionSource() throws SQLException {
		
//		Log.d( TAG, "Configured database: %s", NSWatchConfig.getInstance().getDatabase() );
		
		synchronized (_lock) {
			if( null == _pool ) {
				String connection = NSWatchConfig.getInstance().getDatabase();
				String user = NSWatchConfig.getInstance().getDatabaseUser();
				String pass = NSWatchConfig.getInstance().getDatabasePassword();
				
				if( null != user && !user.isEmpty() )
					_pool = new JdbcPooledConnectionSource( connection, user, pass );
				else
					_pool = new JdbcPooledConnectionSource( connection );
			}
			
			return _pool;
		}
		
		// single connection source example for a database URI
		// ConnectionSource connectionSource = new JdbcConnectionSource(_connectionString);

	}
}
