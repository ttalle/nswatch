package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internaldata.Subscription;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbDeparture {
	public static final String TAG = DbDeparture.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<Departure, Long> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;
	
	public DbDeparture() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, Departure.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					_src = DBFactory.getConnectionSource();
					_dao = DaoManager.createDao(_src, Departure.class );
					
					TableUtils.createTableIfNotExists(_src, Departure.class);
					
					_dbcreated = true;
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Dao<Departure, Long> getDao() throws SQLException {
		return _dao;
	}
	
	public void addDepartures( final List<Departure> departures ) {

		if( null == departures )
			return;
		
		try {
			_dao.callBatchTasks(new Callable<Void>() {
			    public Void call() throws Exception {
			    	
			    	if( departures.size() > 0 ) {
			    		DeleteBuilder<Departure, Long> db = _dao.deleteBuilder();
			    		db.where().eq( "station", departures.get(0).getStation() );
			    		_dao.delete( db.prepare() );
			    	}
			    	
			        for (Departure d : departures) {
			        	_dao.create(d);
			        }
					return null;
			    }
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<Departure> getDepartures( String station ) {
		List<Departure> result = null;
		
		Calendar bottom = Calendar.getInstance();
		bottom.add( Calendar.MINUTE, -5 );
				
		try {
			result = _dao.queryBuilder().orderBy( "realdeparture", true ).where().gt( "realdeparture", bottom.getTime() ).and().eq("station", station ).query();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	

	public List<Departure> getDepartures( Station station ) {
		List<Departure> result = null;
		
		Calendar bottom = Calendar.getInstance();
		bottom.add( Calendar.MINUTE, -5 );

		try {
			result = _dao.queryBuilder().orderBy( "realdeparture", true ).where().gt( "realdeparture", bottom.getTime() ).and().eq("station", station ).query();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<Departure> getDepartures( Registration registration ) {
		List<Departure> result = null;
		
		try {
			DbSubscription dbsub = new DbSubscription();
//			DbRegistration dbreg = new DbRegistration();
			DbStation dbstat = new DbStation();
			DbDeparture dbdep = new DbDeparture();
			
			QueryBuilder<Subscription, Long> qbSubscription = dbsub.getDao().queryBuilder();
//			QueryBuilder<Registration, Long> dbregistration = dbsub.getDao().queryBuilder();
			QueryBuilder<Station, String> qbStation = dbstat.getDao().queryBuilder();
			QueryBuilder<Departure, Long> qbDeparture = dbdep.getDao().queryBuilder();

			Calendar bottom = Calendar.getInstance();
			bottom.add( Calendar.MINUTE, -5 );

			
			qbSubscription.where().eq( "registration", registration );
			qbStation.join( qbSubscription );
			qbDeparture.join(qbStation).orderBy( "realdeparture", true ).where().gt( "realdeparture", bottom.getTime() );
			result = qbDeparture.query();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	

}
