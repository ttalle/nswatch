package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.SQLException;
import java.util.List;

import ttg.gelesneeuw.nswatchproxy.internaldata.LocationItem;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbLocation {
	public static final String TAG = DbLocation.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<LocationItem, Long> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;
	
	public DbLocation() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, LocationItem.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					TableUtils.createTableIfNotExists(_src, LocationItem.class);
		
					_dbcreated = true;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Dao<LocationItem, Long> getDao() throws SQLException {
		return _dao;
	}

	public boolean add( LocationItem item ) {
		boolean result = false;
		
		try {
			_dao.create(item);
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
			result = false;
		}
		
		return result;
	}
	
	public List<LocationItem> get() {
		List<LocationItem> result = null;
		
		try {
			result = _dao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public List<LocationItem> getLatest() {
		List<LocationItem> result = null;
		
		try {
			result = _dao.queryBuilder().limit(100L).orderBy("timestamp", false ).query();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
