package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

import ttg.gelesneeuw.nswatchproxy.internaldata.LogItem;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbLog {
	public static final String TAG = DbLog.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<LogItem, Long> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;
	
	public DbLog() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, LogItem.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					TableUtils.createTableIfNotExists(_src, LogItem.class);
		
					_dbcreated = true;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Dao<LogItem, Long> getDao() throws SQLException {
		return _dao;
	}

	public boolean add( LogItem item ) {
		boolean result = false;
		
		try {
			_dao.create(item);
			result = true;
		} catch (SQLException e) {
			result = false;
		}
		
		return result;
	}
	
	public List<LogItem> get() {
		List<LogItem> result = null;
		
		try {
			result = _dao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public List<LogItem> getLatest() {
		return this.getLatest(null);
	}

	public List<LogItem> getLatest( UUID registration ) {
		List<LogItem> result = null;
		
		try {
			QueryBuilder<LogItem,Long> qb = _dao.queryBuilder();
			
			qb.limit(100L);
			
			if( null != registration )
				qb.where().eq( "client", registration );
			
			result = qb.orderBy("timestamp", false ).query();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
