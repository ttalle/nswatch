package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import ttg.gelesneeuw.nswatchproxy.database.contract.RegistrationContract;
import ttg.gelesneeuw.nswatchproxy.database.contract.StationContract;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internaldata.Subscription;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbRegistration {
	public static final String TAG = DbRegistration.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<Registration, UUID> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;
	
	public DbRegistration() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, Registration.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					TableUtils.createTableIfNotExists(_src, Registration.class);
		
					_dbcreated = true;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Dao<Registration, UUID> getDao() throws SQLException {
		return _dao;
	}

	public Registration register( Registration data ) {
		Registration result = data;
		
		try {
//			long count = _dao.countOf(_dao.queryBuilder().setCountOf(true).where().eq( "device", data.getDevice() ).prepare());
			
//			if( count == 0 )
//				result = ( 1 == _dao.create(data) );
//			else
			result = _dao.createIfNotExists(data);
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public boolean remove( UUID key ) {
		boolean result = false;
		
		try {
			result = ( 1 == _dao.deleteById(key) );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Registration get( UUID key ) {
		Registration result = null;
		
		try {
			result = _dao.queryForId(key);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Registration get( String device ) { 
		Registration result = null;
		
		try {
			result = _dao.queryBuilder().where().eq(RegistrationContract.COLUMN_NAME_DEVICE, device ).queryForFirst();
		} catch( SQLException e ) {
			e.printStackTrace();
		}
		
		return result;
	}
	
}
