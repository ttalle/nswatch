package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internaldata.Subscription;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbSubscription {
	public static final String TAG = DbSubscription.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<Subscription, Long> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;

	public DbSubscription() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, Subscription.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					TableUtils.createTableIfNotExists(_src, Subscription.class);
					_dbcreated = true;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public Dao<Subscription, Long> getDao() throws SQLException {
		return _dao;
	}
	
	public boolean subscribe( Subscription data ) {
		boolean result = false;
		
		try {
			CreateOrUpdateStatus status =  _dao.createOrUpdate(data);
			result = ( status.isCreated() || status.isUpdated() );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public boolean remove( Registration registration, String station ) {
		boolean result = false;
		
		try {
			
			DeleteBuilder<Subscription,Long> db = _dao.deleteBuilder();
			db.where().eq("registration", registration ).and().eq("station", station );
			result = ( _dao.delete(db.prepare()) > 0 );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Subscription get( long id ) {
		Subscription result = null;
		
		try {
			result = _dao.queryForId(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<Subscription> getSubscriptions( Registration registration ) {
		List<Subscription> result = null;
		
		try {
			result = _dao.queryForEq("registration", registration );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public List<String> getSubscribedStations() {
//		QueryBuilder<Subscription, Long> qb = _dao.queryBuilder();
		List<String> result = new ArrayList<String>();
		
		try {
			GenericRawResults<String[]> rows = _dao.queryRaw("SELECT DISTINCT station FROM subscription" );
			
			for( String[] s : rows ) {
				result.add( s[0] );
			}
			// return _dao.query(qb.distinct().selectColumns("station").prepare());
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public Subscription getSubscription(Registration registration, Station station) {
		Subscription subscription = null;
		
		try {
			subscription = _dao.queryForFirst( _dao.queryBuilder().where().eq( "registration", registration ).and().eq("station", station ).prepare() );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return subscription;
	}

	public long getSubscriptionCount(Registration registration) {
		long count = Long.MAX_VALUE;
		try {
			count = _dao.queryBuilder().where().eq("registration", registration ).countOf();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
}
