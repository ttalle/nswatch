package ttg.gelesneeuw.nswatchproxy.database;

import java.sql.SQLException;
import java.util.List;

import ttg.gelesneeuw.nswatch.data.Version;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class DbVersion {
	public static final String TAG = DbVersion.class.toString();
	
	// instantiate the dao with the connection source
	private Dao<Version, Long> _dao = null;
	private ConnectionSource _src = null;
	
	private final static Object _lock = new Object();
	private static boolean _dbcreated = false;
	
	public DbVersion() {
		try {
			_src = DBFactory.getConnectionSource();
			_dao = DaoManager.createDao(_src, Version.class );
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		synchronized (_lock) {
			
			if( !_dbcreated ) {
				try {
					TableUtils.createTableIfNotExists(_src, Version.class);
		
					_dbcreated = true;
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public Dao<Version, Long> getDao() throws SQLException {
		return _dao;
	}

	public List<Version> get() {
		List<Version> result = null;
		
		try {
			result = _dao.queryForAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public Version getVersion( long id ) {
		Version result = null;
				
		try {
			result =  _dao.queryForId(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public Version getLatest() {
		Version result = null;
		
		try {
			result = _dao.queryBuilder().orderBy("version", false ).where().eq("visible", true ).queryForFirst();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
