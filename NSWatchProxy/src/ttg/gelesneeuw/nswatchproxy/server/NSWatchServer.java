package ttg.gelesneeuw.nswatchproxy.server;


import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatchproxy.NSWatchProxy;
import ttg.gelesneeuw.nswatchproxy.database.DbLog;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.LogItem;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.rpc.RPCClientValidation;
import ttg.gelesneeuw.nswatchproxy.rpc.RPCHandler;
import ttg.gelesneeuw.nswatchproxy.rpc.RPCHandlerFactory;
import ttg.gelesneeuw.nswatchproxy.rpc.RPCLogHandler;
import ttg.gelesneeuw.nswatchproxy.web.WebResource;
import ttg.gelesneeuw.nswatchproxy.web.WebResourceFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;




public class NSWatchServer extends NanoHTTPD
{
	public final static String TAG = NSWatchServer.class.getName();
	
	private final ObjectMapper _mapper = new ObjectMapper();
	
	public NSWatchServer( int port, File path ) throws IOException
	{
		super( port, path );
		
//		_mapper = new ObjectMapper();
	}

	@Override
	public Response serve( String uri, String method, Properties header, Properties parms, Properties files, Socket client )
	{
		Response r = internalServe(uri, method, header, parms, files, client);

		if( null != r ) {
			try
			{
				final Date now = new Date();
				
				Log.i( TAG, "%s - - [%s] \"%s %s%s\" %s %d \"%s\"",
						client.getInetAddress().getCanonicalHostName(),
						now.toString(),
						method,
						uri,
						parametersToString(parms),
						r.status.substring(0, 3),
						( null == r.data ? 0 : r.data.available() ),
						( null == header.getProperty("user-agent") ? "no-user-agent" : header.getProperty("user-agent") )
				);				
			} catch (Exception e ) {
				e.printStackTrace();
			}
		}
		
		return r;
	}
	
	private Response internalServe( String uri, String method, Properties header, Properties parms, Properties files, Socket client )
	{
		final DbLog dblog = new DbLog();
		
		Log.d( TAG, "Request:\t%s '%s'", method, uri );
		Log.d( TAG, "Headers:\t%s", header.toString() );
		Log.d( TAG, "Parms:\t%s", parms.toString() );
		Log.d( TAG, "Files:\t%s", files.toString() );
		Log.d( TAG, "Client:\t%s:%d", client.getInetAddress().getCanonicalHostName(), client.getPort() );

		LogItem logitem = new LogItem()
			.setDate(new Date())
			.setHost(client.getInetAddress().getCanonicalHostName())
			.setUrl(uri)
			.setMethod(method)
			.setArguments(parametersToString(parms));
		

		WebResource resource = null;
		RPCHandler handler = null;
		NanoHTTPD.Response response = null;
		Registration registration = null;
		
		if( RPCHandler.checkRequiredParameters( parms, new String[] { "key" } ) ) {
			registration = getRegistration(parms);
			logitem.setClient(registration);
		}

		if( null == uri || uri.isEmpty() || uri.equals("/") ) {
			response = getRedirect( NSWatchProxy.website );
			logitem.setSuccess(false).setHandler("redirect").setData( String.format( "Redirected to: %s", NSWatchProxy.website ) );
		} else if( null != ( resource = WebResourceFactory.getWebResource(uri) ) ) {
			if( resource.isStream() )
				response = new Response(HTTP_OK, resource.getMime(), resource.getStream() );
			else
				response = new Response(HTTP_OK, resource.getMime(), (String)resource.getData() );
			
			logitem.setSuccess(true).setData(resource.getResource() ).setHandler("resource").setSkip(!resource.getLog());
		} else if( null != ( handler = RPCHandlerFactory.getHandler(uri) ) ) {
		
			RPCResponseBase rpcresponse = new RPCResponseBase();
	
			if( null != handler ) {
				RPCResponseBase result = null;
			
				handler.setRegistration(registration);
				
				logitem.setHandler(handler.getName() );
				
				if( handler instanceof RPCLogHandler )
					logitem.setSkip(true);
				
				logitem.setHandler(handler.getName());
				
				try {
					result = handler.handleRequest(header, parms, files);
				} catch( Exception e ) {
					Log.e( TAG, "Error while handling request: %s", e.getMessage() );
					
					e.printStackTrace();
				}
				
				if( null != result )
					rpcresponse = result;
				else
					rpcresponse.setCode(500).setMessage(HTTP_INTERNALERROR);
			}

			String responsedata = "INTERNAL ERROR";
			
			try {
//				response = _mapper.writeValueAsString(rpcresponse);
				ObjectWriter writer = _mapper.writer().withDefaultPrettyPrinter();
				responsedata = writer.writeValueAsString(rpcresponse);
			} catch (JsonProcessingException e) {
				Log.e( TAG, "Fatal error encoding RPCRespnse to JSON" );
				e.printStackTrace();
			}

			Log.d( TAG, "RPC Response:\n%s",rpcresponse.toString() );

			response = new Response( HTTP_OK, MIME_JSON, responsedata );

			logitem.setSuccess(true).setData( rpcresponse.toString() );

		}
		else {
			response = new Response( HTTP_NOTFOUND, MIME_PLAINTEXT, "404: Not found" );
			logitem.setSuccess(false).setData("404");
		}
		
		if( !logitem.getSkip() )
			dblog.add(logitem);
		
		return response;
	}

	public Registration getRegistration( Properties parms ) {
		Registration result = null;
		
		result = RPCClientValidation.isValid(parms.getProperty("key" ));
		
		return result;
	}

	private Response getRedirect( String target ) {
		// String target = NSWatchProxy.website;
		
		Response redirect = new Response( HTTP_REDIRECT, MIME_HTML, String.format( 
				"<html><body>Redirected: <a href=\"%s\">%s</a></body></html>", target, target ) );
		
		redirect.addHeader( "Location", target );
		
		return redirect;
	}

	private String parametersToString( Properties parms ) {
		 Enumeration<Object> em = parms.keys();
		 
		 StringBuilder result  = new StringBuilder();
		 
		 int i = 0;
		 
		 if( parms.size() > 0 ) {
			 while(em.hasMoreElements()) {
				 String str = (String)em.nextElement();
				 
				 result.append(String.format( "%s%s=%s",
						 ( i++ > 0 ? "&" : "?" ),
						 str,
						 parms.get(str)) );
			 }
		 }
		 
		 return result.toString();
	}
}
