package ttg.gelesneeuw.nswatchproxy.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import ttg.gelesneeuw.nswatchproxy.server.NanoHTTPD;

public class WebResourceFactory {
	
	
	private static final Map<String, WebResource> _resources;
    static
    {
        _resources = new HashMap<String, WebResource>();
        _resources.put("/test", new WebResource( NanoHTTPD.MIME_HTML, "/html/test.html") );
        _resources.put("/dashboard", new WebResource(NanoHTTPD.MIME_HTML, "/html/dashboard.html" ) );
        _resources.put("/log", new WebResource(NanoHTTPD.MIME_HTML, "/html/log.html" ) );
        _resources.put("/favicon.ico", new WebResource(NanoHTTPD.MIME_ICO, "/images/icon.ico", true, false ) );
    }
    
	private WebResourceFactory() {
	}
	
	public static WebResource getWebResource( String uri ) {
		WebResource result = null;
		
		if( _resources.containsKey(uri) ) {
			result = _resources.get( uri );
		
			if( result.isStream() ) {
				result.setStream( getStream(result.getResource() ));
			} else {
				result.setData( readInternalPage(result.getResource() ));
			}
		}
		
		return result;
	}
	
	
	private static InputStream getStream( String resource ) {
		return WebResourceFactory.class.getResourceAsStream(resource);
	}
	
	private static String readInternalPage( String resource ) {

//		InputStream in = getClass().getResourceAsStream(resource);
		InputStream in = WebResourceFactory.class.getResourceAsStream(resource);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader( in ) );
		StringBuilder html = new StringBuilder();
		String line;
		try {
			while( null != ( line = reader.readLine() ) )
				html.append( line );
			reader.close();
			in.close();
		} catch (IOException e) {
		} finally {
		}
		
		return html.toString();
	}

}
