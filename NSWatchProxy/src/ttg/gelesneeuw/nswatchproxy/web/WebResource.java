package ttg.gelesneeuw.nswatchproxy.web;

import java.io.InputStream;

public class WebResource {
	private String _mime;
	private String _resource;
	private String _data;
	private InputStream _stream;
	private boolean _isStream;
	private boolean _log;
	
	public WebResource( String mime, String resource ) {
		_mime = mime;
		_resource = resource;
		_stream = null;
		_data = null;			
		_isStream = false;
		_log = true;
	}

	public WebResource( String mime, String resource, boolean isStream ) {
		_mime = mime;
		_resource = resource;
		_stream = null;
		_data = null;			
		_isStream = isStream;
		_log = true;
	}
	
	public WebResource( String mime, String resource, boolean isStream, boolean log ) {
		_mime = mime;
		_resource = resource;
		_stream = null;
		_data = null;			
		_isStream = isStream;
		_log = log;
	}


	public String getMime() { return _mime; }
	public WebResource setMime( String value ) { _mime = value; return this; }
	
	public String getResource() { return _resource; }
	public WebResource setResource( String value ) { _resource = value; return this; }
	
	public boolean isStream() { return _isStream; }
	public WebResource setIsStream( boolean value ) { _isStream = value; return this; }
	
	public String getData() { return _data; }
	public WebResource setData( String value ) { _data = value; return this; }
	
	public InputStream getStream() { return _stream; }
	public WebResource setStream( InputStream value ) { _stream = value; return this; }
	
	public boolean getLog() { return _log; }
	public WebResource setLog( boolean value ) { _log = value; return this; }
}
