package ttg.gelesneeuw.nswatchproxy.util;

import java.util.Calendar;

public class Deadline {
	private long _deadline = 0;
	private long _interval = 0;
	
	/** Initialize a deadline counting from NOW
	 * @param interval The interval in milliseconds
	 */
	public Deadline( long interval ) {
		_deadline = Calendar.getInstance().getTimeInMillis();
		_interval = interval;
	}
	
	
	/** Initialize a deadline
	 * @param start The start count for the deadline interval
	 * @param interval The interval in milliseconds
	 */
	public Deadline( long start, long interval ) {
		_deadline = start;
		_interval = interval;
	}
	
	
	public long getDeadline() { return _deadline; }
	public long getInterval() { return _interval; }
	
	/** Checks for a deadline hit
	 * @return 0 when no deadline has been hit, 1 when the deadline is now, 1+ when some deadlines were missed
	 */
	public long check() {
		long result = 0;
		long now = Calendar.getInstance().getTimeInMillis();
		
		if( _deadline < now ) {
			while( _deadline < now ) {
				_deadline += _interval;
				++result;
			}
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		Calendar now = Calendar.getInstance();
		
		return String.format( "Deadline every %d seconds; next deadline at %d in T%d second(s)",
				_interval / 1000L,
				_deadline / 1000L,
				( now.getTimeInMillis() - _deadline ) / 1000L
			);
	}
}
