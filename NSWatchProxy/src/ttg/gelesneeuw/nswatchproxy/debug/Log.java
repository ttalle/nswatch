package ttg.gelesneeuw.nswatchproxy.debug;

import ttg.gelesneeuw.nswatchproxy.NSWatchConfig;

import com.j256.ormlite.logger.LocalLog;

public class Log {
	public enum LogLevel {
		Fatal,
		Error,
		Debug,
		Info
	}
	
	private static boolean _debug = false;
	
	public Log() {
	}
	
	public static void init() {
		_debug = NSWatchConfig.getInstance().getDebug();
		if( _debug ) {
			System.setProperty(LocalLog.LOCAL_LOG_LEVEL_PROPERTY, "debug" );
		} else {
			System.setProperty(LocalLog.LOCAL_LOG_LEVEL_PROPERTY, "info" );
		}
	}

	public static void f( String tag, String msg  ) {
		log( LogLevel.Fatal, tag, msg );
	}

	public static void f( String tag, String msg, Object... args ) {
		log( LogLevel.Fatal, tag, msg, args );
	}

	
	public static void e( String tag, String msg ) {
		log( LogLevel.Error, tag, msg );
	}
	public static void e( String tag, String msg, Object... args ) {
		log( LogLevel.Error, tag, msg, args );
	}

	
	public static void d( String tag, String msg ) {
		log( LogLevel.Debug, tag, msg );
	}

	public static void d( String tag, String msg, Object... args ) {
		log( LogLevel.Debug, tag, msg, args );
	}

	public static void i( String tag, String msg ) {
		log( LogLevel.Info, tag, msg );
	}

	public static void i( String tag, String msg, Object... args ) {
		log( LogLevel.Info, tag, msg, args );
	}

	public static void log( LogLevel level, String tag, String msg  ) {
		if( !_debug && level == LogLevel.Debug )
			return;
		
		System.out.println( String.format( "[%s] (%s): %s", level.toString(), tag, msg ) );
	}

	public static void log( LogLevel level, String tag, String msg, Object... args ) {
		if( !_debug && level == LogLevel.Debug )
			return;

		System.out.println( String.format( "[%s] (%s): %s", level.toString(), tag, String.format( msg, args ) ) );
	}
}
