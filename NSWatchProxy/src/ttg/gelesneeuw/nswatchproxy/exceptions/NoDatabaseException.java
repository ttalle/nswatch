package ttg.gelesneeuw.nswatchproxy.exceptions;

public class NoDatabaseException extends Exception {

	private static final long serialVersionUID = 1L;

	public NoDatabaseException() {
		super();
	}
	
	public NoDatabaseException( String message ) {
		super(message);
	}

}
