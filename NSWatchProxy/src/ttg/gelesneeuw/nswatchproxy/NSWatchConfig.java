package ttg.gelesneeuw.nswatchproxy;

import java.nio.file.InvalidPathException;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.exceptions.NoDatabaseException;

public class NSWatchConfig {
    private static final String TAG = NSWatchConfig.class.getName();

	// Preference keys for this package
//    private static final String CONF_BACKING_STORE_AVAIL = "BackingStoreAvail";
    
    private static final String CONF_CONFIGURED = "configured";
    private static final String CONF_DATABASE = "database";
	private static final String CONF_DATABASE_USER = "database_user";
	private static final String CONF_DATABASE_PASS = "database_pass";
    private static final String CONF_DEBUG = "debug";
    private static final String CONF_WEBROOT = "webroot";
    private static final String CONF_WEBPORT = "port";
	private static final String CONF_MAXSUBSCRIPTIONS = "max_subscriptions";
	private static final String CONF_EXPIRETIME = "expire_time";
	private static final String CONF_FIXDSTOFFSET = "fix_dst_offset";
	
    private static NSWatchConfig _instance = null;

	private boolean _configured = false;
    private String _database = "";
    private String _databaseUser = "";
    private String _databasePass = "";
    private boolean _debug = false;
	private int _port = 6969;
	private String _webroot = ".";
	private long _maxSubscriptions = 0;
	private long _expireTime = 0;
	private int _fixDSTOffset = 0;
	
    public String getDatabase() { return _database; }
    public NSWatchConfig setDatabase( String value ) { _database = value; return this; }
    
    public String getDatabaseUser() { return _databaseUser; }
    public NSWatchConfig setDatabaseUser( String value ) { _databaseUser = value; return this; }
    
    public String getDatabasePassword() { return _databasePass; }
    public NSWatchConfig setDatabasePassword( String value ) { _databasePass = value; return this; }
    
    public boolean getDebug() { return _debug; }
    public NSWatchConfig setDebug( boolean value ) { _debug = value; return this; }
    
    public int getWebPort() { return _port; }
    public NSWatchConfig setWebPort( int value ) { _port = value; return this; }
    
    public String getWebRoot() { return _webroot; }
    public NSWatchConfig setWebRoot( String value ) { _webroot = value; return this; }
    
	public long getMaxSubscriptions() { return _maxSubscriptions; }
	public NSWatchConfig setMaxSubscriptions( long value ) { _maxSubscriptions = value; return this; }
	
	public long getExpireTime() { return _expireTime; }
	public NSWatchConfig setExpireTime( long value ) { _expireTime = value; return this; }

	public int getFixDSTOffset() { return _fixDSTOffset; }
	public NSWatchConfig setFixDSTOffset( int value ) { _fixDSTOffset = value; return this; }
	
	private NSWatchConfig() {}
    
    public static NSWatchConfig getInstance() {
    	if( null == _instance ) {
    		_instance = new NSWatchConfig();
//    		_instance.load();
    	}
    	
    	return _instance;
    }
    
	public void load() throws NoDatabaseException {
    	/*
    	if( !backingStoreAvailable() )
    		Log.e( TAG, "Warning: no backing store available" );
    	*/
        Preferences prefs = Preferences.userNodeForPackage(NSWatchConfig.class);

        _configured = prefs.getBoolean(CONF_CONFIGURED, false );
        _database = prefs.get(CONF_DATABASE, "");
        _databaseUser = prefs.get(CONF_DATABASE_USER, "");
        _databasePass = prefs.get(CONF_DATABASE_PASS, "");
        _debug = prefs.getBoolean(CONF_DEBUG, false );
        _port = prefs.getInt(CONF_WEBPORT, 20699);
        _webroot = prefs.get(CONF_WEBROOT, "." );
        _maxSubscriptions = prefs.getLong( CONF_MAXSUBSCRIPTIONS, 10 );
        _expireTime = prefs.getLong( CONF_EXPIRETIME, 5*60 ); // Default is 5 minutes
        _fixDSTOffset = prefs.getInt(CONF_FIXDSTOFFSET, 0 );
        
        Log.init();

        if( !_configured ) {
        	_configured = true;
        	save();
        }
        
        if( null == _database || _database.isEmpty() )
        	throw new NoDatabaseException( "Database path not set or invalid, please configure");
    }
    
    public void save() {
        Preferences prefs = Preferences.userNodeForPackage(NSWatchConfig.class);
        
        prefs.putBoolean(CONF_CONFIGURED, _configured );
        prefs.put(CONF_DATABASE, _database );
        prefs.put(CONF_DATABASE_USER, _databaseUser );
        prefs.put(CONF_DATABASE_PASS, _databasePass );
        prefs.putBoolean(CONF_DEBUG, _debug );
        prefs.putInt(CONF_WEBPORT, _port );
        prefs.put(CONF_WEBROOT, _webroot );
        prefs.putLong(CONF_MAXSUBSCRIPTIONS, _maxSubscriptions );
        prefs.putLong(CONF_EXPIRETIME, _expireTime );
        prefs.putInt(CONF_FIXDSTOFFSET, _fixDSTOffset );
        
        try {
			prefs.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
    }    

    /*
    private static boolean backingStoreAvailable() {
        Preferences prefs = Preferences.userRoot().node("");
        
        try {
            boolean oldValue = prefs.getBoolean(CONF_BACKING_STORE_AVAIL, false);
            prefs.putBoolean(CONF_BACKING_STORE_AVAIL, !oldValue);
            prefs.flush();
        } catch(BackingStoreException e) {
            return false;
        }
        
        return true;
    }
	*/
    
}
