package ttg.gelesneeuw.nswatchproxy;

public final class NSWatchProxy {
	private NSWatchProxy() {}
	
	public static final String website = "https://www.gelesneeuw.nl";
	public static final String proxy = "ns.gelesneeuw.nl";
	
	public static final String nsUsername = "username@example.com";
	public static final String nsPassword = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";

	public static final int
		USER_LEVEL_NORMAL = 0,
		USER_LEVEL_PLUS1 = 100,
		USER_LEVEL_PLUS2 = 200,
		USER_LEVEL_PLUS3 = 300,
		USER_LEVEL_PLUS4 = 400,
		USER_LEVEL_PLUS5 = 500,
		USER_LEVEL_PLUS6 = 600,
		USER_LEVEL_PLUS7 = 700,
		USER_LEVEL_PLUS8 = 800,
		USER_LEVEL_PLUS9 = 900,
		USER_LEVEL_ADMIN = 1000;
}
