package ttg.gelesneeuw.nswatchproxy.workers;

import java.util.Calendar;
import java.util.List;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.database.DbDeparture;
import ttg.gelesneeuw.nswatchproxy.database.DbSubscription;
import ttg.gelesneeuw.nswatchproxy.database.DbUpdateLog;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.UpdateLogItem;
import ttg.gelesneeuw.nswatchproxy.nsapi.NSAPIService;
import ttg.gelesneeuw.nswatchproxy.util.Deadline;

public class DepartureUpdater implements Runnable, IUpdater {
	private final static String TAG = DepartureUpdater.class.getName(); 
		
	private boolean _stop = false;
	
	public DepartureUpdater() {
		
	}
	
	public void stop() {
		_stop = true;
	}
	
	@Override
	public void run() {
		Calendar start = Calendar.getInstance();
		start.add( Calendar.MINUTE, 5 );
		
		final Deadline deadline = new Deadline( start.getTimeInMillis(), ( 5 * 60 * 1000L ) );

		Log.i( TAG, "Departure updater started: %s", deadline.toString() );
		
		while( !_stop ) {
			
			if( deadline.check() < 1 ) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				continue;
			}
				
			Log.d(TAG, "Deadline hit, updating departures" );

			update();
			
			Log.d(TAG, "Departure update done, sleeping: %s", deadline.toString() );
		}
	}

//	private final DbStation dbs = new DbStation();
	private final DbDeparture dbd = new DbDeparture();
	private final DbSubscription dbs = new DbSubscription();

	@Override
	public void update() {

		List<String> subscriptions = dbs.getSubscribedStations();
		
		for( String s : subscriptions ) {
			(new DbUpdateLog()).add(new UpdateLogItem("departure", s ) );

			List<Departure> departures = NSAPIService.getDepartures( s );
			
			dbd.addDepartures(departures);
		}
	}
	
	public void update( Station station ) {
		(new DbUpdateLog()).add(new UpdateLogItem("departure", station.getId() ) );

		List<Departure> departures = NSAPIService.getDepartures( station.getId() );
		dbd.addDepartures(departures);
	}
}
