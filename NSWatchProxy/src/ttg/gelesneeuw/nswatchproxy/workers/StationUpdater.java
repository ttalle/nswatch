package ttg.gelesneeuw.nswatchproxy.workers;

import java.util.Calendar;
import java.util.List;

import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.database.DbStation;
import ttg.gelesneeuw.nswatchproxy.database.DbUpdateLog;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.UpdateLogItem;
import ttg.gelesneeuw.nswatchproxy.nsapi.NSAPIService;
import ttg.gelesneeuw.nswatchproxy.util.Deadline;

public class StationUpdater implements Runnable, IUpdater {
	private static final String TAG = StationUpdater.class.getName();
	
	private boolean _stop = false;
	
	public StationUpdater() {
		
	}
	
	public void stop() {
		_stop = true;
	}
	
	@Override
	public void run() {
		Calendar start = Calendar.getInstance();
		start.add( Calendar.MINUTE, 30 );
		
		final Deadline deadline = new Deadline( start.getTimeInMillis(), 24 * 3600 * 1000 );
		
		Log.i( TAG, "Station updater started: %s", deadline.toString() );
		while( !_stop ) {
			
			if( deadline.check() < 1 ) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				continue;
			}
				
			try
			{
				Log.d(TAG, "Deadline hit, updating stations" );
				
				update();
				
				Log.d(TAG, "Station update done, sleeping: %s", deadline.toString() );
			} catch( Exception e ) {
				e.printStackTrace();
			}
		}
	}

	private final DbStation dbs = new DbStation();

	@Override
	public void update() {

		(new DbUpdateLog()).add(new UpdateLogItem("station", null ) );

		List<Station> stations = NSAPIService.getStations();
		
		dbs.setStations(stations);
	}

}
