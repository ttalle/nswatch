package ttg.gelesneeuw.nswatchproxy.proxy;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.NSWatchConfig;
import ttg.gelesneeuw.nswatchproxy.database.DbDeparture;
import ttg.gelesneeuw.nswatchproxy.database.DbStation;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.workers.DepartureUpdater;

public class DepartureProxy {
	private final static String TAG = DepartureProxy.class.getName();
	
	private DbDeparture _dbd = new DbDeparture();
	private DbStation _dbs = new DbStation();
	
	public DepartureProxy() {
		
	}
	
	public List<Departure> getDepartures( String stationid ) {
		Station station = _dbs.getStation(stationid);
		
		if( null == station )
			return null;
				
		return this.getDepartures(station);
	}
	
	public List<Departure> getDepartures( Station station ) {

		update(station);
		return _dbd.getDepartures(station);
	}
	
	public List<Departure> getDepartures( Registration registration ) {
		List<Station> stations = _dbs.getStations(registration);
		
		for( Station station : stations )
			update( station );
		
		return _dbd.getDepartures(registration);
	}
	
//	private static Object _updateLock = new Object();
	
	private void update( Station station ) {
		synchronized (station.getId().intern()) {
			
			Calendar expiretime = Calendar.getInstance();
			
			expiretime.add( Calendar.SECOND, -1 * (int)NSWatchConfig.getInstance().getExpireTime() );
			
			if( station.getLastUpdate().before( expiretime.getTime() ) ) {
				Log.i( TAG, "Departures for station %s expired (%s < %s); updating...", station.getId(), station.getLastUpdate().toString(), expiretime.getTime().toString() );
				
				DepartureUpdater updater = new DepartureUpdater();
				
				updater.update( station );
				
				_dbs.updateStation( station.setLastUpdate(new Date()) );
			} else {
				Log.d( TAG, "Cache for station %s still valid for %d seconds...", station.getId(), ( station.getLastUpdate().getTime() - expiretime.getTimeInMillis() ) / 1000L );
			}
		}
	}
}
