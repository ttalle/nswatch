package ttg.gelesneeuw.nswatchproxy.nsapi;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import nl.gelesneeuw.utils.encoding.Base64;

import org.xmlpull.v1.XmlPullParserException;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.NSWatchProxy;
import ttg.gelesneeuw.nswatchproxy.database.DbUpdateLog;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.UpdateLogItem;

public class NSAPIService {
	private final static String TAG = NSAPIService.class.getName();
	
	private static long _counter = 0;
	private static long _stationRequestCounter = 0;
	private static long _departureRequestCounter = 0;
	
	public long getStationRequestCount() { return _stationRequestCounter; }
	public long getDepartureRequestCount() { return _departureRequestCounter; }
	
	public static List<Departure> getDepartures( String station) {
		String url = String.format("http://webservices.ns.nl/ns-api-avt?station=%s", station );

		List<Departure> result = null;

		InputStream stream = null;

		
		try {
			Log.i( TAG, "Departure download #%d", _departureRequestCounter++ );
			
			stream = downloadUrl(url);
			// stream =  .getResources().openRawResource(R.raw.zoetermeer);
			
			result = NSDepartureParser.parse( station, stream);
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Makes sure that the InputStream is closed after the app is
			// finished using it.
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}


	public static List<Station> getStations() {
		String url = String.format("http://webservices.ns.nl/ns-api-stations-v2" );

		List<Station> result = null;
		
		InputStream stream = null;

		try {
			Log.i( TAG, "Station download #%d", _stationRequestCounter++ );

			stream = downloadUrl(url);
			// stream =  .getResources().openRawResource(R.raw.zoetermeer);
			
			result = NSStationParser.parse(stream);
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Makes sure that the InputStream is closed after the app is
			// finished using it.
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return result;
	}
	
	// Given a string representation of a URL, sets up a connection and gets
	// an input stream.
	private static InputStream downloadUrl(String urlString) throws IOException {
		Log.i( TAG, "Download request #%d", _counter++ );
		
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(10000 /* milliseconds */);
		conn.setConnectTimeout(20000 /* milliseconds */);
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		
		conn.setRequestProperty("Authorization",
				"Basic " + Base64.encodeToString(
						String.format("%s:%s",
							NSWatchProxy.nsUsername,
							NSWatchProxy.nsPassword ).getBytes(), false ));
		// Starts the query
		conn.connect();
		return conn.getInputStream();
	}
}
