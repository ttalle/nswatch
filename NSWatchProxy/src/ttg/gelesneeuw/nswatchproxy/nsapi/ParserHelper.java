package ttg.gelesneeuw.nswatchproxy.nsapi;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import ttg.gelesneeuw.nswatchproxy.NSWatchConfig;

public class ParserHelper {
	protected static final String ns = null;

	// Processes title tags in the feed.
	protected static String readText(XmlPullParser parser, String tag ) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, tag);
		String title = readTextNode(parser);
		parser.require(XmlPullParser.END_TAG, ns, tag);
		return title;
	}

	// Processes title tags in the feed.
	protected static Date readDate(XmlPullParser parser, String tag ) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, tag);
		Date result = readDateNode(parser);
		parser.require(XmlPullParser.END_TAG, ns, tag);
		return result;
	}

	// Processes title tags in the feed.
	protected static long readLong(XmlPullParser parser, String tag ) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, tag);
		long result = readLongNode(parser);
		parser.require(XmlPullParser.END_TAG, ns, tag);
		return result;
	}

	// Processes title tags in the feed.
	protected static double readDouble(XmlPullParser parser, String tag ) throws IOException, XmlPullParserException {
		parser.require(XmlPullParser.START_TAG, ns, tag);
		double result = readDoubleNode(parser);
		parser.require(XmlPullParser.END_TAG, ns, tag);
		return result;
	}

	protected static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
	    if (parser.getEventType() != XmlPullParser.START_TAG) {
	        throw new IllegalStateException();
	    }
	    int depth = 1;
	    while (depth != 0) {
	        switch (parser.next()) {
	        case XmlPullParser.END_TAG:
	            depth--;
	            break;
	        case XmlPullParser.START_TAG:
	            depth++;
	            break;
	        }
	    }
	 }


	// For the tags title and summary, extracts their text values.
	private static String readTextNode(XmlPullParser parser) throws IOException, XmlPullParserException {
		String result = "";
		if (parser.next() == XmlPullParser.TEXT) {
			result = parser.getText();
			parser.nextTag();
		}
		return result;
	}

	// For the tags title and summary, extracts their text values.
	private static Date readDateNode(XmlPullParser parser) throws IOException, XmlPullParserException {
		Date result = null;
		if (parser.next() == XmlPullParser.TEXT) {
			String value = parser.getText();
			final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-d'T'HH:mm:ssZ");
			try {
				Calendar tmpdate = Calendar.getInstance();
				tmpdate.setTime( format.parse(value) );
				tmpdate.add( Calendar.MINUTE, NSWatchConfig.getInstance().getFixDSTOffset() );
				result = tmpdate.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}  
			parser.nextTag();
		}
		return result;
	}

	// For the tags title and summary, extracts their text values.
	private static long readLongNode(XmlPullParser parser) throws IOException, XmlPullParserException {
		long result = 0;
		if (parser.next() == XmlPullParser.TEXT) {
			result = Long.parseLong( parser.getText() );
			parser.nextTag();
		}
		return result;
	}

	private static double readDoubleNode(XmlPullParser parser) throws IOException, XmlPullParserException {
		double result = 0;
		if (parser.next() == XmlPullParser.TEXT) {
			result = Double.parseDouble( parser.getText() );
			parser.nextTag();
		}
		return result;
	}

}
