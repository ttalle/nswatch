package ttg.gelesneeuw.nswatchproxy.nsapi;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatchproxy.debug.Log;

public class NSDepartureParser extends ParserHelper {
	public static final String TAG = NSDepartureParser.class.toString();
	
	public static List<Departure> parse(String station, InputStream in) throws XmlPullParserException, IOException {
		List<Departure> result = null;
		
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			
			String tag = parser.getName();
			if( tag.equalsIgnoreCase("ActueleVertrekTijden") ) {
				result = readDepartures(station, parser);
			} else if( tag.equalsIgnoreCase("Error") ) {
				readError(station, parser);
			}
//		} catch ( XmlPullParserException e ) {
//			Log.e( TAG, "Error while parsing opening tag, invalid file? Excerpt:\n%s", in.read(arg0) );
//			throw e;
		} finally {
			in.close();
		}
		
		return result;
	}

	private static void readError( String station, XmlPullParser parser) throws XmlPullParserException, IOException {
		parser.require(XmlPullParser.START_TAG, ns, "error");

		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			
			String name = parser.getName();

			// Starts by looking for the entry tag
			if (name.equalsIgnoreCase("message")) {
				Log.e( TAG, "Error reading departures: %s", readText(parser,"message" ) );
			}
		}
	}
	
	private static List<Departure> readDepartures( String station, XmlPullParser parser) throws XmlPullParserException, IOException {
		List<Departure> entries = new ArrayList<Departure>();
		
		parser.require(XmlPullParser.START_TAG, ns, "ActueleVertrekTijden");
		
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();

			// Starts by looking for the entry tag
			if (name.equals("VertrekkendeTrein")) {
				entries.add(readEntry( station, parser));
			} else {
				skip(parser);
			}
		}
		return entries;
	}

	// Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
	// to their respective "read" methods for processing. Otherwise, skips the tag.
	private static Departure readEntry( String station, XmlPullParser parser) throws XmlPullParserException, IOException {
	    parser.require(XmlPullParser.START_TAG, ns, "VertrekkendeTrein");

	    Departure departure = new Departure();
	    
	    departure.setStation( new Station( station ) );
  
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        
	        String name = parser.getName();
	  	        
	        if (name.equals("RitNummer")) {
	            departure.setId( readLong( parser, "RitNummer") );
	        } else if (name.equals("VertrekTijd")) {
	            departure.setDeparture( readDate(parser,"VertrekTijd") );
	        } else if (name.equals("VertrekVertraging")) {
	            departure.setDelay( readText(parser,"VertrekVertraging" ));
	        } else if (name.equals("VertrekVertragingTekst")) {
	            departure.setDelayText(readText(parser,"VertrekVertragingTekst" ));
	        } else if (name.equals("EindBestemming")) {
	            departure.setDestination( readText(parser,"EindBestemming"));
	        } else if (name.equals("TreinSoort")) {
	            departure.setTrainType(readText(parser,"TreinSoort"));
	        } else if (name.equals("RouteTekst")) {
	            departure.setRoute( readText(parser,"RouteTekst"));
	        } else if (name.equals("Vervoerder")) {
	            departure.setTransporter(readText(parser,"Vervoerder"));
	        } else if (name.equals("VertrekSpoor")) {
	        	
	        	String trackchange = parser.getAttributeValue(ns, "wijziging" ).trim();

        		if( trackchange.equals("true") )
	        		departure.setTrackChange(true);
	        	
	            departure.setTrack(readText(parser,"VertrekSpoor"));
	        } else if (name.equals("ReisTip")) {
	            departure.setTip( readText(parser,"ReisTip"));
	        } else if (name.equals("Opmerkingen")) {
	        	parseNotes(departure, parser);
	        } else {
	            skip(parser);
	        }
	    }

    	Calendar realdepature = Calendar.getInstance();
    	realdepature.setTime(departure.getDeparture());

	    if( null != departure.getDelay() && !departure.getDelay().equals("") ) {
	    	int delay = parseDelay( departure.getDelay() );
	    	realdepature.add( Calendar.MINUTE, delay );
	    }
	    
    	departure.setRealDeparture( realdepature.getTime() );
	    
	    return departure;
	}

	private static void parseNotes(Departure departure, XmlPullParser parser) throws XmlPullParserException, IOException {
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}

			String name = parser.getName();

			if (name.equals("Opmerking")) {
				String note = readText( parser, "Opmerking").trim();
				if( note.length() > 0 && !note.equals(""))
					departure.getNotes().add(note);
			}
			else
				skip(parser);
		}
	}
	
	private static final Pattern pattern =  Pattern.compile("^PT(\\d+)M$");
	public static int parseDelay( String delaystring ) {
		int result = 0;
		
		Matcher matcher = pattern.matcher(delaystring);

		if( matcher.find() ) {
			result = Integer.parseInt(matcher.group(1));
//			Log.i(TAG, String.format( "Parsed the delay \"%s\" as a delay of %d minutes", delaystring, result ) );
		}
		
		return result;
	}
}
