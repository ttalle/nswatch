package ttg.gelesneeuw.nswatchproxy.nsapi;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import ttg.gelesneeuw.nswatch.data.Station;

public class NSStationParser extends ParserHelper {

	public static List<Station> parse(InputStream in) throws XmlPullParserException, IOException {
		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();
			
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readFeed(parser);
		} finally {
			in.close();
		}
	}

	private static List<Station> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
		List<Station> entries = new ArrayList<Station>();

		parser.require(XmlPullParser.START_TAG, ns, "Stations");
		
		while (parser.next() != XmlPullParser.END_TAG) {
			if (parser.getEventType() != XmlPullParser.START_TAG) {
				continue;
			}
			String name = parser.getName();

			// Starts by looking for the entry tag
			if (name.equals("Station")) {
				entries.add(readEntry(parser));
			} else {
				skip(parser);
			}
		}
		return entries;
	}

	// Parses the contents of an entry. If it encounters a title, summary, or link tag, hands them off
	// to their respective "read" methods for processing. Otherwise, skips the tag.
	private static Station readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
	    parser.require(XmlPullParser.START_TAG, ns, "Station");

	    Station station = new Station();
  
	    while (parser.next() != XmlPullParser.END_TAG) {
	        if (parser.getEventType() != XmlPullParser.START_TAG) {
	            continue;
	        }
	        
	        String name = parser.getName();
	  	        
	        if (name.equals("Code")) {
	        	station.setId( readText( parser, "Code") );
	        } else if (name.equals("UICCode")) {
	        	station.setUICCode( readLong( parser, "UICCode") );
	        } else if (name.equals("Type")) {
	        	station.setType( readText(parser,"Type") );
	        } else if (name.equals("Namen")) {
	        	readNamen(parser,"Namen",station );
	        } else if (name.equals("Land")) {
	        	station.setCountry(readText(parser,"Land" ));
	        } else if (name.equals("Lat")) {
	        	station.setLat( readDouble(parser,"Lat"));
	        } else if (name.equals("Lon")) {
	        	station.setLon( readDouble(parser,"Lon"));
	        } else if (name.equals("Synoniemen")) {
	        	station.setSynonymList( readSynoniemen(parser, "Synoniemen") );
	        } else {
	            skip(parser);
	        }
	    }
	    
	    return station;
	}

	private static List<String> readSynoniemen(XmlPullParser parser, String string) throws XmlPullParserException, IOException {
		List<String> result = new ArrayList<String>();
		
		 parser.require(XmlPullParser.START_TAG, ns, "Synoniemen");

		    while (parser.next() != XmlPullParser.END_TAG) {
		        if (parser.getEventType() != XmlPullParser.START_TAG) {
		            continue;
		        }
		        
		        String name = parser.getName();
		  	        
		        if (name.equals("Synoniem")) {
		        	result.add( readText( parser, "Synoniem") );
		        } else {
		        	skip( parser );
		        }
		    }
		
		return result;
	}

	private static void readNamen(XmlPullParser parser, String string, Station station ) throws XmlPullParserException, IOException {
		 parser.require(XmlPullParser.START_TAG, ns, "Namen");

		    while (parser.next() != XmlPullParser.END_TAG) {
		        if (parser.getEventType() != XmlPullParser.START_TAG) {
		            continue;
		        }
		        
		        String name = parser.getName();
		  	        
		        if (name.equals("Kort")) {
		        	station.setNameShort( readText( parser, "Kort") );
		        } else if (name.equals("Middel")) {
		        	station.setName( readText(parser,"Middel") );
		        } else if (name.equals("Lang")) {
		        	station.setNameLong( readText(parser,"Lang" ));
		        } else {
		        	skip(parser);
		        }
		    }
	}

}
