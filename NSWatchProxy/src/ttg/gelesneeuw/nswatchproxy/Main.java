package ttg.gelesneeuw.nswatchproxy;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Locale;

import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.exceptions.NoDatabaseException;
import ttg.gelesneeuw.nswatchproxy.server.NSWatchServer;
import ttg.gelesneeuw.nswatchproxy.util.Deadline;
import ttg.gelesneeuw.nswatchproxy.util.TimeSpanFormater;
import ttg.gelesneeuw.nswatchproxy.workers.DepartureUpdater;
import ttg.gelesneeuw.nswatchproxy.workers.StationUpdater;

public abstract class Main {
	public final static String TAG = Main.class.getName();
	
	private static boolean _stop = false;
	
	public static void Stop() {
		_stop = true;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
//		System.setProperty(
//				"java.util.prefs.PreferencesFactory",
//				"ttg.gelesneeuw.nswatchproxy.preferences.DisabledSystemPreferencesFactory" );
		
		
		// Reset the system preference path to the user path to prevent buggy java implementation exceptions
		String systempref = System.getProperty( "java.util.prefs.systemRoot" );
		
		if( null != systempref )
			System.setProperty( "java.util.prefs.systemRoot", System.getProperty("java.util.prefs.userRoot"));
		
		NSWatchConfig config;
		
		try {
			config = NSWatchConfig.getInstance();
			config.load();
		} catch (NoDatabaseException e1) {
			e1.printStackTrace();
			System.out.println("Configuration is incomplete, please edit by hand: " + e1.getMessage() );
			return;
		}
		
		final int port = config.getWebPort();
		final String webroot = config.getWebRoot(); 
		
		NSWatchServer webserver = null;
		Thread stationUpdaterThread = null;
		Thread departureUpdaterThread = null;

		StationUpdater supdater = new StationUpdater();
		DepartureUpdater dupdater = new DepartureUpdater();
		
		try
		{
			webserver = new NSWatchServer( port, new File( webroot ) );
			( stationUpdaterThread = new Thread(supdater) ).start();
			// ( departureUpdaterThread = new Thread(dupdater) ).start();
			
		}
		catch( IOException ioe )
		{
			Log.f( TAG, "Couldn't start server:\n%s", ioe );
			System.exit( -1 );
		}
		
		Log.i( TAG, "Listening on port %d.", port);
		Log.i( TAG, "Serving: %s", webroot );
		logShortKeys();
		
		int count = 0;
		final Date startTime = new Date();
		final Deadline deadline = new Deadline(3600 * 1000L);
		
		
		while(!_stop) {
			try {
				if( System.in.available() > 0 ) {
					int input = System.in.read();
				
					switch( input ) {
					case 'q':
						Log.i( TAG, "Quit requested");
						_stop = true; break;
					case 'r':
						Log.i( TAG, "(Re)loading configuration" );
						NSWatchConfig.getInstance().load(); break;
					case 's':
						Log.i( TAG, "Saving configuration" );
						NSWatchConfig.getInstance().save(); break;
					case 'u':
						Log.i( TAG, "Up since: %s", TimeSpanFormater.format(startTime, new Date() )  ); break;
					case 'm':
						Log.i( TAG, "Memory usage: %s", getMemoryUsage() ); break;
					case '?':
						logShortKeys(); break;
					default:
						break;
					}
				}
				
				if( deadline.check() > 0 )
					Log.i( TAG, "Up since: %s", TimeSpanFormater.format(startTime, new Date() )  );

				Thread.sleep(1*1000);
				
			} catch( Throwable t ) {};
		}
		
		Log.i( TAG, "Stopping webserver..." );
		webserver.stop();
		Log.i( TAG, "Stopping webserver... done" );
//		
//		try {
//			Log.i( TAG, "Stopping departure updater..." );
//			dupdater.stop();
//			departureUpdaterThread.join();
//			Log.i( TAG, "Stopping departure updater... done" );
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
		
		try {
			Log.i( TAG, "Stopping station updater..." );
			supdater.stop();
			stationUpdaterThread.join();
			Log.i( TAG, "Stopping departure updater... done" );
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		Log.i( TAG, "Shutdown complete" );

	}
	
	private static void logShortKeys() {
		Log.i( TAG, "Shortkeys:" );
		Log.i( TAG, "\t%s:\tStop", 'q' );
		Log.i( TAG, "\t%s:\tUptime", 'u' );
		Log.i( TAG, "\t%s:\tMemory usage", 'm' );
		Log.i( TAG, "\t%s:\tReload configuration", 'r' );
		Log.i( TAG, "\t%s:\tSave configuration", 's');
		Log.i( TAG, "\t%s:\tThis overview", '?');
	}
	
	private static String getMemoryUsage() {
		Runtime runtime = Runtime.getRuntime();

	    NumberFormat format = NumberFormat.getInstance( Locale.US );

	    StringBuilder sb = new StringBuilder();

	    long maxMemory = runtime.maxMemory();
	    long allocatedMemory = runtime.totalMemory();
	    long freeMemory = runtime.freeMemory();

	    sb.append("[");
	    sb.append( format.format( ( allocatedMemory - freeMemory ) / 1024) );
	    sb.append("KB / ");
	    sb.append( format.format(freeMemory / 1024) );
	    sb.append("KB / ");
	    sb.append( format.format(allocatedMemory / 1024) );
	    sb.append("KB / ");
	    sb.append( format.format(maxMemory / 1024) );
	    sb.append("KB / ");
	    sb.append( format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024) );
	    sb.append("] [Usage / Free / Allocated / Max / Total Free]");

//	    sb.append("free memory: " + format.format(freeMemory / 1024) + "<br/>");
//	    sb.append("allocated memory: " + format.format(allocatedMemory / 1024) + "<br/>");
//	    sb.append("max memory: " + format.format(maxMemory / 1024) + "<br/>");
//	    sb.append("total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / 1024) + "<br/>");
	    
	    return sb.toString();
	}
}
