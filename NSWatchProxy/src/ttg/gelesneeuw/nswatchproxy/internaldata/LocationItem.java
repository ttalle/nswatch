package ttg.gelesneeuw.nswatchproxy.internaldata;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "location")
public class LocationItem {

	@DatabaseField( columnName = "_id", generatedId = true )
	private long _id = 0;

	@DatabaseField( columnName = "registration", foreign = true, foreignAutoRefresh = true )
	private Registration _registration = null;
			
	@DatabaseField( columnName = "timestamp" )
	private Date _timestamp = null;
	
	@DatabaseField( columnName = "latitude" )
	private double _latitude;
	
	@DatabaseField( columnName = "longitude" )
	private double _longitude;

	public long getId() { return _id; }
	public LocationItem setId( long value ) { _id = value; return this; }
	
	public Registration getRegistration() { return _registration; }
	public LocationItem setRegistration( Registration value ) { _registration = value; return this; }
	
	public Date getTimestamp() { return _timestamp; }
	public LocationItem setTimestamp( Date value ) { _timestamp = value; return this; }
	
	public double getLatitude() { return _latitude; }
	public LocationItem setLatitude( double value ) { _latitude= value; return this; }
	
	public double getLongitude() { return _longitude; }
	public LocationItem setLongitude( double value ) { _longitude = value; return this; }
}
