package ttg.gelesneeuw.nswatchproxy.internaldata;

import java.util.UUID;

import ttg.gelesneeuw.nswatch.data.Station;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "subscription")
public class Subscription {

	@JsonIgnore()
	@DatabaseField( columnName = "_id", generatedId = true )
	private long _id;
	
	@JsonIgnore()
	@DatabaseField( columnName = "registration", canBeNull = false, foreign = true, foreignAutoRefresh = true)
	private Registration _registration;

	@JsonIgnore()
	@DatabaseField( columnName = "station", canBeNull = false, foreign = true, foreignAutoRefresh = true)
	private Station _station;
	

	public long getId() { return _id; }
	public Subscription setId( long value ) { _id = value; return this; }
	
	public Registration getRegistration() { return _registration; }
	public Subscription setRegistration( Registration value ) { _registration = value; return this; }
	
	public Station getStation() { return _station; }
	public Subscription setStation( Station value ) { _station = value; return this; }
	
	@JsonProperty("registration")
	public UUID getRegistrationKey() {
		if( null != _registration )
			return _registration.getKey();
		else
			return null;
	}
	
	@JsonProperty("station")
	public String getStationId() {
		if( null != _station )
			return _station.getId();
		else
			return null;
	}

}
