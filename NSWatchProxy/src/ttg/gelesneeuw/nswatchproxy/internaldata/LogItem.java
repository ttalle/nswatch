package ttg.gelesneeuw.nswatchproxy.internaldata;

import java.util.Date;

import ttg.gelesneeuw.nswatchproxy.rpc.RPCHandler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "request")
public class LogItem {
	
	@DatabaseField(columnName = "requestid", generatedId = true )
	private long _id;

	@DatabaseField(columnName = "timestamp")
	private Date _date;

	@DatabaseField(columnName = "host")
	private String _host;

	@DatabaseField(columnName = "client", foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 1)
	private Registration _client;

	@DatabaseField(columnName = "method")
	private String _method;
	
	@DatabaseField(columnName = "url")
	private String _url;
	
	@DatabaseField(columnName = "arguments")
	private String _arguments;
	
	@DatabaseField(columnName = "handler")
	private String _handler;
	
	@DatabaseField(columnName = "success")
	private boolean _success;
	
	@DatabaseField(columnName = "data")
	private String _data;
	
	@JsonIgnore()
	private boolean _skip = false;
	
	public LogItem() {
	}
	
	public long getId() { return _id; }
	public LogItem setId( long value ) { _id = value; return this; }

	public Date getDate() { return _date; }
	public LogItem setDate( Date value ) { _date = value; return this; }
	
	public String getHost() { return _host; }
	public LogItem setHost( String value ) { _host = value; return this; }
	
	public Registration getClient() { return _client; }
	public LogItem setClient( Registration value ) { _client = value; return this; }
	
	public String getMethod() { return _method; }
	public LogItem setMethod( String value ) { _method = value; return this; }
	
	public String getUrl() { return _url; }
	public LogItem setUrl( String value ) { _url = value; return this; }
	
	public String getArguments() { return _arguments; }
	public LogItem setArguments( String value ) { _arguments = value; return this; }
	
	public String getHandler() { return _handler; }
	public LogItem setHandler( String value ) { _handler = value; return this; }
	
	public boolean getSuccess() { return _success; }
	public LogItem setSuccess( boolean value ) { _success = value; return this; }
	
	public String getData() { return _data; }
	public LogItem setData( String value ) { _data = value; return this; }
	
	@JsonIgnore()
	public boolean getSkip() { return _skip; }
	@JsonIgnore()
	public LogItem setSkip( boolean value ) { _skip = value; return this; }
}
