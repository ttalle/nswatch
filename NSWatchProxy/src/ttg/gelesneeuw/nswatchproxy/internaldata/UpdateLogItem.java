package ttg.gelesneeuw.nswatchproxy.internaldata;

import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "updatelog")
public class UpdateLogItem {

	@DatabaseField( columnName = "_id", generatedId = true )
	private long _id = 0;

	@DatabaseField( columnName = "timestamp", canBeNull = false )
	private Date _timestamp = null;

	@DatabaseField( columnName = "type", canBeNull = false, width = 10 )
	private String _type = null;
	
	@DatabaseField( columnName = "data", canBeNull = true, width = 255 )
	private String _data = null;

	
	public UpdateLogItem() {
	}
	
	public UpdateLogItem( String type, String data ) {
		_timestamp = new Date();
		_type = type;
		_data = data;
	}
	
	public long getId() { return _id; }
	public UpdateLogItem setId( long value ) { _id = value; return this; }

	public Date getTimestamp() { return _timestamp; }
	public UpdateLogItem setTimestamp( Date value ) { _timestamp = value; return this; }
	
	public String getType() { return _type; }
	public UpdateLogItem setType( String value ) { _type = value; return this; }
	
	public String getData() { return _data; }
	public UpdateLogItem setData( String value ) { _data = value; return this; }
	
}
