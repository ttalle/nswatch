package ttg.gelesneeuw.nswatchproxy.internaldata;

import java.util.Date;
import java.util.UUID;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "registration")
public class Registration {
	
//	@DatabaseField(id = true, columnName = "_id" )
//	private long _id = 0;
	
	@DatabaseField( columnName = "key", id = true )
	private UUID _key = null;

	@DatabaseField( columnName = "name" )
	private String _name = null;
	
	@DatabaseField( columnName = "username", unique = true, canBeNull = true )
	private String _username = null;
	
	@DatabaseField( columnName = "password" )
	private String _password = null;
	
	@DatabaseField( columnName = "device", unique = true, canBeNull = false )
	private String _device = null;
	
	@DatabaseField( columnName = "created" )
	private Date _created = null;
	
	@DatabaseField( columnName = "enabled" )
	private boolean _enabled = true;

	@DatabaseField( columnName = "level", canBeNull = false, defaultValue = "0" )
	private int _level = 0;
	
	public Registration() {
	}
	
//	public long getId() { return _id; }
//	public Registration setId( long value ) { _id = value; return this; }
	
	public UUID getKey() { return _key; }
	public Registration setKey( UUID value ) { _key = value; return this; }
	
	public String getName() { return _name; }
	public Registration setName( String value ) { _name = value; return this; }
	
	public String getUsername() { return _username; }
	public Registration setUsername( String value ) { _username = value; return this; }
	
	public String getPassword() { return _password; }
	public Registration setPassword( String value ) { _password = value; return this; }
	
	public String getDevice() { return _device; }
	public Registration setDevice( String value ) { _device = value; return this; }
	
	public Date getCreated() { return _created; }
	public Registration setCreated( Date value ) { _created = value; return this; }
	
	public boolean getEnabled() { return _enabled; }
	public Registration setEnabled( boolean value ) { _enabled = value; return this; }
	
	public int getLevel() { return _level; }
	public Registration setLevel( int value ) { _level = value; return this; }
}
