package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.Properties;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatchproxy.database.DbLog;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.LogItem;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;

public abstract class RPCHandler {
	public final static String TAG = RPCHandler.class.getName();

	private Registration _registration = null;
	
	public Registration getRegistration() { return _registration; }
	public RPCHandler setRegistration( Registration value ) { _registration = value; return this; }
	
	public String getName() { return this.getClass().getSimpleName(); }
	
	public static boolean checkRequiredParameters( Properties parms, String[] parameters ) {
		
		for( String key : parameters ) {
			if( !parms.containsKey(key) ) {
				Log.d( TAG, "Missing request parameter: %s", key );
				return false;
			}
			
			String value = parms.getProperty(key);
			
			if( null == value || value.trim().length() < 1 ) {
				Log.d( TAG, "Empty required parameter: %s", key );
				return false;
			}
		}
		
		return true;
	}
	
	public abstract RPCResponseBase handleRequest(Properties header, Properties parms, Properties files);
}
