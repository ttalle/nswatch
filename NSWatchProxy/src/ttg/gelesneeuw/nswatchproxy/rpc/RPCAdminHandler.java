package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.Properties;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatchproxy.Main;
import ttg.gelesneeuw.nswatchproxy.NSWatchProxy;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internalresponse.RPCAdminResponse;

public class RPCAdminHandler extends RPCHandler {

	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {

		RPCAdminResponse response = new RPCAdminResponse();
		
		Registration registration = getRegistration();
		
		if( null == registration || registration.getLevel() < NSWatchProxy.USER_LEVEL_ADMIN )
			return response.setSuccess(false).setMessage("Access denied");
		
		if( !checkRequiredParameters(parms, new String[] { "action" } ) )
			return response.setSuccess(false).setMessage("Missing parameters");
			
		String action = parms.getProperty("action").trim();
		
		if( action.equalsIgnoreCase( "stop" ) )
			Main.Stop();
		
		response.setSuccess(true).setMessage("Done");
		
		return response;
	}
}
