package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.List;
import java.util.Properties;
import java.util.UUID;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatchproxy.NSWatchProxy;
import ttg.gelesneeuw.nswatchproxy.database.DbDeparture;
import ttg.gelesneeuw.nswatchproxy.database.DbLog;
import ttg.gelesneeuw.nswatchproxy.database.DbStation;
import ttg.gelesneeuw.nswatchproxy.internaldata.LogItem;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internalresponse.RPCLogResponse;

public class RPCLogHandler extends RPCHandler {

	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {
		RPCLogResponse response = new RPCLogResponse();
		
		if( !checkRequiredParameters(parms, new String[] { "key" } ))
			return response.setSuccess(false).setMessage("Invalid or missing parameters");
		
		Registration registration = getRegistration();
		
		if( null == registration || registration.getLevel() < NSWatchProxy.USER_LEVEL_ADMIN )
			return response.setSuccess(false).setMessage("Access denied");

		DbLog dblog = new DbLog();
		UUID user = null;
		
		if( checkRequiredParameters(parms, new String[] { "user" } ) ) {
			try {
				user = UUID.fromString(parms.getProperty("user"));
			} catch ( IllegalArgumentException ex ) { }
		}

		List<LogItem> logs = dblog.getLatest( user );
		
		if( null == logs )
			return response.setSuccess(false).setMessage("Error retreiving logitems");
		
		response
			.setLogItems(logs)
			.setSuccess(true)
			.setMessage("Lastest logitems" );
		
		return response;
	}

}
