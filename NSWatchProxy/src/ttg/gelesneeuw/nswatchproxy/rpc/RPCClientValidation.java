package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.UUID;

import ttg.gelesneeuw.nswatchproxy.database.DbRegistration;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;

public class RPCClientValidation {
	public final static String TAG = RPCClientValidation.class.getName();
	
	public static Registration isValid( String keyinput ) {
		if( null == keyinput ) 
			return null;

		if( !keyinput.matches("[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}") )
			return null;
		
		UUID key;
		
		try {
			key = UUID.fromString(keyinput);
		} catch( IllegalArgumentException e ) {
			return null;
		}
		
		DbRegistration dbr = new DbRegistration();
		
		Registration result = dbr.get(key);
		
		Log.d(TAG, "Validation for key \"%s\": %b", key, result );
		
		return result;
	}
}
