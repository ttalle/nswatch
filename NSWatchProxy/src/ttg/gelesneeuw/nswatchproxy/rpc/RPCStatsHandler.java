package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.List;
import java.util.Properties;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatchproxy.NSWatchProxy;
import ttg.gelesneeuw.nswatchproxy.database.DbUpdateLog;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internalresponse.RPCStatsResponse;
import ttg.gelesneeuw.nswatchproxy.internalresponse.RPCStatsResponse.UpdateStatsItem;

public class RPCStatsHandler extends RPCHandler {

	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {

		RPCStatsResponse response = new RPCStatsResponse();
		
		Registration registration = getRegistration();
		
		if( null == registration || registration.getLevel() < NSWatchProxy.USER_LEVEL_ADMIN )
			return response.setSuccess(false).setMessage("Access denied");
		
		//if( !checkRequiredParameters(parms, new String[] { "type" } ) )
		//	return response.setSuccess(false).setMessage("Missing parameters");
			
		//String action = parms.getProperty("type").trim();
		
		// SELECT YEAR(timestamp) As Year, MONTH(timestamp) AS Month, DAY(timestamp) AS Day, type, COUNT(*) AS Count FROM updatelog GROUP BY YEAR(timestamp), MONTH(timestamp), DAY(timestamp), type;
		
		
		// if( action.equalsIgnoreCase( "requests" ) ) {
			List<UpdateStatsItem> updates = (new DbUpdateLog()).getCounts();
			response.setUpdateStatsItems(updates);
		// }
			
		response.setSuccess(true).setMessage("Done");
		
		return response;
	}
}
