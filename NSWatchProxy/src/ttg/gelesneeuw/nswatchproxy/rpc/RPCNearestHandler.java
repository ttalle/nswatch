package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatch.rpc.response.RPCNearestResponse;
import ttg.gelesneeuw.nswatchproxy.database.DbLocation;
import ttg.gelesneeuw.nswatchproxy.database.DbStation;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.LocationItem;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.proxy.DepartureProxy;

public class RPCNearestHandler extends RPCHandler {

	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {
		DbStation dbs = new DbStation();
		// DbDeparture dbd = new DbDeparture();
		
		RPCNearestResponse response = new RPCNearestResponse();
		
		if( !checkRequiredParameters(parms, new String[] { "key", "lat", "lon" } ))
			return response.setSuccess(false).setMessage("Invalid or missing parameters");
		
		Registration registration = getRegistration();
		
		if( null == registration )
			return response.setSuccess(false).setMessage("Access denied");

		double lat = 0;
		double lon = 0;

		try {
			lat = Double.parseDouble( parms.getProperty("lat") );
			lon = Double.parseDouble( parms.getProperty("lon") );
		} catch( NumberFormatException ex ) {
			Log.e(TAG, ex.getMessage() );
		}
		
		if( lat == 0 || lon == 0 )
			return response.setSuccess(false).setMessage("Invalid or missing parameters" );
		
		(new DbLocation()).add( new LocationItem().setTimestamp(new Date()).setRegistration(registration).setLatitude(lat).setLongitude(lon) );
		
		Station station = dbs.getStation( lat, lon );
	
		if( null == station )
			return response.setSuccess(false).setMessage("Couldn't locate a suitable station");
		
		DepartureProxy proxy = new DepartureProxy();
		
		List<Departure> departures =  proxy.getDepartures( station );
		
		for( Departure d : departures )
			d.setStation(new Station("NEAREST"));
		
		response
			.setStation(station)
			.setDepartures( departures )
			.setSuccess(true)
			.setMessage("Departures for station: " + station.getName() );
	
		
		return response;
	}

}
