package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.Properties;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatchproxy.NSWatchProxy;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.workers.DepartureUpdater;
import ttg.gelesneeuw.nswatchproxy.workers.StationUpdater;

public class RPCUpdateHandler extends RPCHandler {

	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {
		RPCResponseBase response = new RPCResponseBase();
		
		Registration registration = getRegistration();
		
		if( null == registration || registration.getLevel() < NSWatchProxy.USER_LEVEL_ADMIN )
			return response.setSuccess(false).setMessage("Access denied");

		(new StationUpdater()).update();
		(new DepartureUpdater()).update();
		
		return response.setSuccess(true).setMessage("Update complete");
	}

}
