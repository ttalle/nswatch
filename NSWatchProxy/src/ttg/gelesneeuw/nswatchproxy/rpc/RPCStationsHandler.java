package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.List;
import java.util.Properties;

import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.response.RPCStationsResponse;
import ttg.gelesneeuw.nswatchproxy.database.DbStation;

public class RPCStationsHandler extends RPCHandler {

	@Override
	public RPCStationsResponse handleRequest(Properties header, Properties parms, Properties files) {

		RPCStationsResponse response = new RPCStationsResponse();

		if( !checkRequiredParameters(parms, new String[] { "key" } ) ) {
			response.setSuccess(false).setMessage("Invalid or missing parameters");
			return response;
		}
		
		if( null == RPCClientValidation.isValid( parms.getProperty("key") ) ) {
			response.setSuccess(false).setMessage("Access denied");
			return response;
		}
		
		DbStation dbs = new DbStation();
		
		List<Station> stations = dbs.getStations(); 
		
		stations.add( new Station("NEAREST")
			.setCountry("NL")
			.setLat(0)
			.setLon(0)
			.setName("Nearest station")
			.setNameShort("Nearest")
			.setNameLong("Nearest station (from GPS/Cell coordinates)" )
			.setUICCode(0) );
		
		response.setStations( stations )
			.setSuccess(true).setMessage("List of known stations");
		
		return response;
	}
}
