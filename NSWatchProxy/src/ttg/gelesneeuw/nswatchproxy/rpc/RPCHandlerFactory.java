package ttg.gelesneeuw.nswatchproxy.rpc;

public abstract class RPCHandlerFactory {

	private RPCHandlerFactory() {}
	
	public static final String REQUEST_STATIONS = "/stations";
	public static final String REQUEST_DEPARTURES = "/departures";
	public static final String REQUEST_REGISTER = "/register";
	public static final String REQUEST_SUBSCRIBE = "/subscribe";
	public static final String REQUEST_UPDATE = "/update";
	public static final String REQUEST_NEAREST = "/nearest";
	public static final String REQUEST_ADMIN = "/admin";
	public static final String REQUEST_STATS = "/stats";
	public static final String REQUEST_LOG = "/logitems";
	public static final String REQUEST_VERSION = "/version";
	
	public static RPCHandler getHandler( String uri ) {
		RPCHandler handler = null;

		if( uri.equalsIgnoreCase(REQUEST_REGISTER))
			handler = new RPCRegisterHandler();
		else if( uri.equalsIgnoreCase(REQUEST_STATIONS))
			handler = new RPCStationsHandler();
		else if( uri.equalsIgnoreCase(REQUEST_DEPARTURES))
			handler = new RPCDeparturesHandler();
		else if( uri.equalsIgnoreCase(REQUEST_SUBSCRIBE))
			handler = new RPCSubscribeHandler();
		else if( uri.equalsIgnoreCase(REQUEST_UPDATE))
			handler = new RPCUpdateHandler();
		else if( uri.equalsIgnoreCase(REQUEST_NEAREST))
			handler = new RPCNearestHandler();
		else if( uri.equalsIgnoreCase(REQUEST_LOG))
			handler = new RPCLogHandler();
		else if( uri.equalsIgnoreCase(REQUEST_STATS))
			handler = new RPCStatsHandler();
		else if( uri.equalsIgnoreCase(REQUEST_ADMIN))
			handler = new RPCAdminHandler();
		else if( uri.equalsIgnoreCase(REQUEST_VERSION))
			handler = new RPCVersionHandler();
		
		return handler;
	}
}
