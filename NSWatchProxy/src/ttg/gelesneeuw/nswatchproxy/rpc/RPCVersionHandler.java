package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.Properties;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatch.rpc.response.RPCVersionResponse;
import ttg.gelesneeuw.nswatchproxy.database.DbVersion;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;

public class RPCVersionHandler extends RPCHandler {

	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {
		RPCVersionResponse response = new RPCVersionResponse();
		
//		if( !checkRequiredParameters(parms, new String[] { "key" } ))
//			return response.setSuccess(false).setMessage("Invalid or missing parameters");
		
		Registration registration = getRegistration();
		
		if( null == registration )
			return response.setSuccess(false).setMessage("Access denied");
		
		DbVersion dbv = new DbVersion();
		
		response.setLatest(dbv.getLatest());
		
		if( checkRequiredParameters(parms, new String[] { "current" } )) {
			long version = Long.parseLong(parms.getProperty("current"));
			response.setCurrent(dbv.getVersion(version));
		}
		
		response
			.setSuccess(true)
			.setMessage("Release information" );
		
		return response;
	}

}
