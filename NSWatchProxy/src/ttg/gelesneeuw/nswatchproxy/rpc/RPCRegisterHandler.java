package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.Date;
import java.util.Properties;
import java.util.UUID;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatch.rpc.response.RPCRegistrationResponse;
import ttg.gelesneeuw.nswatchproxy.database.DbRegistration;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;

public class RPCRegisterHandler extends RPCHandler {
	private final static String TAG = RPCRegisterHandler.class.getName();
	
	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {
		RPCRegistrationResponse response = new RPCRegistrationResponse();
		
		if( !checkRequiredParameters(parms, new String[] { "device", "name" } ) ) {
			response.setSuccess(false).setMessage("Invalid or missing parameters");
		} else {

			String devid = parms.getProperty("device");
			String name = parms.getProperty("name");
			
			boolean result = createRegistration(response, devid, name);
			
			Log.e(TAG, "Registration result: %b!", result );
		}
		
		return response;
	}
	
	
	private boolean createRegistration( RPCRegistrationResponse response, String devid, String name ) {
		
		DbRegistration dbr = new DbRegistration();
		Registration registration = dbr.get(devid);

		if( null == registration ) {
			registration = new Registration();

			UUID key = UUID.randomUUID();

			registration
				.setCreated( new Date() )
				.setEnabled(true)
				.setKey(key)
				.setName(name)
				.setDevice(devid);
			
			registration = dbr.register(registration);
			
			response.setMessage("New registration");
		} else {
			response.setMessage("Existing registration");
		}
		
		if( null != registration )
			response.setKey( registration.getKey() ).setSuccess(true);
		else
			response.setKey( null ).setSuccess(false).setMessage("Failed registration");

		return response.getSuccess();
	}

}
