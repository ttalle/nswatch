package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatch.rpc.response.RPCSubscriptionResponse;
import ttg.gelesneeuw.nswatchproxy.NSWatchConfig;
import ttg.gelesneeuw.nswatchproxy.database.DbStation;
import ttg.gelesneeuw.nswatchproxy.database.DbSubscription;
import ttg.gelesneeuw.nswatchproxy.debug.Log;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.internaldata.Subscription;

public class RPCSubscribeHandler extends RPCHandler {
	private final static String TAG = RPCSubscribeHandler.class.getName();
	
	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {
		RPCSubscriptionResponse response = new RPCSubscriptionResponse();
		
		if( !checkRequiredParameters(parms, new String[] { "key" } ) )
			return response.setSuccess(false).setMessage("Invalid or missing parameters");

		Registration registration = getRegistration();
		
		if( null == registration )
			return response.setSuccess(false).setMessage("Access denied");
			
		if( !checkRequiredParameters(parms, new String[] { "subscribe", "station" } ) ) {
			return listSubscriptions( response, registration );
		} else {
		
			boolean subscribe = parms.getProperty("subscribe").equals("true");
			String station = parms.getProperty("station");
			
			boolean result = false;
			
			if( subscribe )
				result = subscribe(response, registration, station );
			else
				result = unsubscribe(response, registration, station );
			
			Log.e(TAG, "Subscription result: %b!", result );
			
		}
		return response;
	}
	
	
	private RPCResponseBase listSubscriptions(RPCSubscriptionResponse response, Registration registration) {
		DbSubscription dbSub = new DbSubscription();
		
		List<Subscription> subscriptions = dbSub.getSubscriptions(registration);
		List<Station> stations = new ArrayList<Station>( subscriptions.size() );
		
		for( Subscription s : subscriptions )
			stations.add( s.getStation() );
		
		response
			.setStations( stations )
			.setSuccess(true)
			.setMessage("Your subscriptions");
		
		return response;
	}


	private boolean subscribe( RPCSubscriptionResponse response, Registration registration, String stationid ) {
		DbSubscription dbsub = new DbSubscription();
		DbStation dbstat = new DbStation();
		
		Station station = dbstat.getStation( stationid );

		if(stationid.equalsIgnoreCase( "NEAREST" ) || null == station ) {
			response.setSuccess(false).setMessage("Station does not exist");
			return false;
		}

		long count = dbsub.getSubscriptionCount( registration );
		
		if( count > NSWatchConfig.getInstance().getMaxSubscriptions() ) {
			Log.d( TAG, "Too many subscriptions: %d > %d", count, NSWatchConfig.getInstance().getMaxSubscriptions() );
			response.setSuccess(false).setMessage("Subscribed to too many stations");
			return false;
		}
		
		Subscription subscription = dbsub.getSubscription(registration, station);
		
		if( null == subscription )
			subscription = new Subscription();
		else
			response.getErrors().add( "Warning: you were already subscribed to this station" );
		
		subscription
			.setRegistration( registration )
			.setStation( station );
		
		boolean result = dbsub.subscribe(subscription);
		
		if( result )
			response.setSuccess(result).setMessage("You are now subscribed to " + stationid );
		else
			response.setSuccess(result).setMessage("Failed to subscribe");

		return result;
	}
	
	private boolean unsubscribe(  RPCSubscriptionResponse response, Registration registration, String station  ) {
		DbSubscription dbs = new DbSubscription();
		
		boolean result = dbs.remove( registration, station );
		
		if( result )
			response.setSuccess(true).setMessage("You are no longer subscribed to " + station );
		else
			response.setSuccess(false).setMessage("Failed to unsubscribe");
		
		return result;
	}

}
