package ttg.gelesneeuw.nswatchproxy.rpc;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import nl.gelesneeuw.utils.text.StringUtils;
import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatch.rpc.response.RPCDepartureResponse;
import ttg.gelesneeuw.nswatchproxy.database.DbDeparture;
import ttg.gelesneeuw.nswatchproxy.internaldata.Registration;
import ttg.gelesneeuw.nswatchproxy.proxy.DepartureProxy;

public class RPCDeparturesHandler extends RPCHandler {

	private DepartureProxy _proxy = new DepartureProxy();
	
	public RPCDeparturesHandler() {
	}
	
	@Override
	public RPCResponseBase handleRequest(Properties header, Properties parms, Properties files) {
		DbDeparture dbd = new DbDeparture();
		RPCDepartureResponse response = new RPCDepartureResponse();
		
		if( !checkRequiredParameters(parms, new String[] { "key" } ))
			return response.setSuccess(false).setMessage("Invalid or missing parameters");
		
		Registration registration = getRegistration();
		
		if( null == registration )
			return response.setSuccess(false).setMessage("Access denied");

		List<Departure> departures = new ArrayList<Departure>();

		if( checkRequiredParameters(parms, new String[] { "station" } )) {
			String stationparam = parms.getProperty("station");
			
			List<String> stations = StringUtils.split(stationparam, ";" );
			
			if( stations.size() > 5 )
				return response.setSuccess(false).setMessage( "Too many stations requested");
			
			for( String station: stations ) {
				List<Departure> d = _proxy.getDepartures( station );
				
				if( null != d )
					departures.addAll( d );
			}
			response
				.setDepartures( departures )
				.setSuccess(true)
				.setMessage("Departures for stations: " + StringUtils.join( stations, "," ) );
		} else {
			departures = _proxy.getDepartures( registration );
	
			response
				.setDepartures( departures )
				.setSuccess(true)
				.setMessage("Departures");
		}
		
		return response;
	}

}
