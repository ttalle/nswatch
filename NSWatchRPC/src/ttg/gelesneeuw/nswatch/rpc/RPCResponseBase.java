package ttg.gelesneeuw.nswatch.rpc;

import java.util.ArrayList;
import java.util.List;

public class RPCResponseBase {
	private boolean _success = false;
	private String _message = null;
	private long _code = 0;
	private List<String> _errors = new ArrayList<String>();
	
	public boolean getSuccess() { return _success; }
	public RPCResponseBase setSuccess( boolean value ) { _success = value; return this; }
	
	public String getMessage() { return _message; }
	public RPCResponseBase setMessage( String value ) { _message = value; return this; }
	
	public long getCode() { return _code; }
	public RPCResponseBase setCode( long value ) { _code = value; return this; }
	
	public List<String> getErrors() { return _errors; }
	public RPCResponseBase setErrors( List<String> value ) { _errors = value; return this; }
	
	public RPCResponseBase() {
	}
	
	@Override
	public String toString() {
		StringBuilder errors = new StringBuilder();
		
		if( _errors != null ) {
			for( String error : _errors ) {
				errors.append( error ).append("; ");
			}
		}
		else
			errors.append("null");
		
		return String.format( "RPCResponseBase { success: %s, message: \"%s\", code: %d, errors: \"%s\" }",
				( _success ? "true" : "false" ),
				_message,
				_code,
				errors.toString()
			);
	}
}
