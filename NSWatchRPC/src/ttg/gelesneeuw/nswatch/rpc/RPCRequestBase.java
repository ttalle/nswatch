package ttg.gelesneeuw.nswatch.rpc;

public class RPCRequestBase {
	public String _request;
	
	public String getRequest() { return _request; }
	public RPCRequestBase setRequest( String value ) { _request = value; return this; }
	
	public RPCRequestBase( String request ) {
		_request = request;
	}
}
