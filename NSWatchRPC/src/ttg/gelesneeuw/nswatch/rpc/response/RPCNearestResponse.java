package ttg.gelesneeuw.nswatch.rpc.response;

import java.util.List;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCNearestResponse extends RPCResponseBase {
	private Station _station = null;
	private List<Departure> _departures = null;

	public Station getStation() { return _station; }
	public RPCNearestResponse setStation( Station value ) { _station = value; return this; }

	public List<Departure> getDepartures() { return _departures; }
	public RPCNearestResponse setDepartures( List<Departure> value ) { _departures = value; return this; }

	
	public RPCNearestResponse() {
	}
	
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCNearestResponse { station: %s }", parent, ( null == _station ? "null" : _station.getId() ) );
	}
}
