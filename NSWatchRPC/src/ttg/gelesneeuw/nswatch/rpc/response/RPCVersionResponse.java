package ttg.gelesneeuw.nswatch.rpc.response;

import ttg.gelesneeuw.nswatch.data.Version;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCVersionResponse extends RPCResponseBase {
	private Version _current = null;
	private Version _latest = null;

	public Version getCurrent() { return _current; }
	public RPCVersionResponse setCurrent( Version value ) { _current = value; return this; }
	
	public Version getLatest() { return _latest; }
	public RPCVersionResponse setLatest( Version value ) { _latest = value; return this; }
	
	public RPCVersionResponse() {
	}
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCVersionResponse { current: %d, latest: %d }", parent,
			( null == _current ? 0 : _current.getVersion() ),
			( null == _latest ? 0 : _latest.getVersion() )
		);
	}
}
