package ttg.gelesneeuw.nswatch.rpc.response;

import java.util.List;

import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCSubscriptionResponse extends RPCResponseBase {
	
	private List<Station> _stations = null;
	
	public List<Station> getStations() { return _stations; }
	public RPCSubscriptionResponse setStations( List<Station> value ) { _stations = value; return this; } 
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCSubscriptionResponse { }", parent );
	}
}
