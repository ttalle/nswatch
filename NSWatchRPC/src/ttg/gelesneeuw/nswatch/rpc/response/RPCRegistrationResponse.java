package ttg.gelesneeuw.nswatch.rpc.response;

import java.util.UUID;

import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCRegistrationResponse extends RPCResponseBase {
	private UUID _key;
	
	public UUID getKey() { return _key; }
	public RPCRegistrationResponse setKey( UUID value ) { _key = value; return this; }
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCRegistrationResponse { key: %s }", parent, _key );
	}
}
