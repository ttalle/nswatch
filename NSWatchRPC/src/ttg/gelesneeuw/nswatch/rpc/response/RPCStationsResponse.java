package ttg.gelesneeuw.nswatch.rpc.response;

import java.util.List;

import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCStationsResponse extends RPCResponseBase {
	private List<Station> _stations;
	
	public List<Station> getStations() { return _stations; }
	public RPCStationsResponse setStations( List<Station> value ) { _stations = value; return this; }
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\nRPCStationResponse { stations: %s }", parent, ( null == _stations ) ? "null" : String.valueOf( _stations.size() ) );
	}
}
