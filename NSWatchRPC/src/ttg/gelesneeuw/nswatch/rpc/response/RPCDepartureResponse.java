package ttg.gelesneeuw.nswatch.rpc.response;

import java.util.List;

import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;

public class RPCDepartureResponse extends RPCResponseBase {
	private List<Departure> _departures = null;

	public List<Departure> getDepartures() { return _departures; }
	public RPCDepartureResponse setDepartures( List<Departure> value ) { _departures = value; return this; }

	
	public RPCDepartureResponse() {
	}
	
	
	@Override
	public String toString() {
		String parent = super.toString();
		return String.format( "%s\n%s { departures: %s }", parent, RPCDepartureResponse.class.getSimpleName(), ( null == _departures ? "null" : String.valueOf( _departures.size() ) ) );
	}
}
