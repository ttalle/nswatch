package ttg.gelesneeuw.nswatch.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.gelesneeuw.utils.text.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
Code (1): Identificerende afkorting van het station.
Type (1): Typering van het station.
Namen (1): Namen van het station.
Land (1): Landcode van het station:
UICCode (1): UIC-code van het station, identificerende code volgens de standaard van de Internationale Spoorwegunie, de UIC: Union Internationale des Chemins de fer.
Lat (1): Breedtegraad (latitude) van het station.
Lon (1): Lengtegraad (longitude) van het station.
Synoniemen (1): Synoniemen van het station.

Het element Namen kent de volgende structuur:

Kort (1): Korte naam van het station, bestaande uit maximaal 10  karakters.
Middel (1): Middellange naam van het station, bestaande uit maximaal 16 karakters.
Lang (1): Lange naam van het station, bestaande uit maximaal 25 karakters.

Het element Synoniemen kent de volgende structuur:

Synoniem (0..n): Naam die als synoniem wordt gebruikt voor de lange stationsnaam.
*/


@DatabaseTable(tableName = "station")
public class Station {

	@DatabaseField(columnName = "stationid", id = true )
	private String _id = "";

	@DatabaseField(columnName = "uiccode")
	private long _uicCode = 0;
	
	@DatabaseField(columnName = "type" )
	private String _type = "";
	
	@DatabaseField(columnName = "nameshort" )
	private String _nameshort = "";
	
	@DatabaseField(columnName = "name" )
	private String _name = "";
	
	@DatabaseField(columnName = "namelong" )
	private String _namelong = "";
	
	@DatabaseField(columnName = "country" )
	private String _country = "";
	
	@JsonIgnore
	@DatabaseField(columnName = "latitude" )
	private double _lat = 0;
	
	@JsonIgnore
	@DatabaseField(columnName = "longitude" )
	private double _lon = 0;

	@DatabaseField(persisted = false)
	private List<String> _synonyms = new ArrayList<String>();

	@DatabaseField(columnName = "lastupdate" )
	private Date _lastUpdate = new Date();
	
	//, columnDefinition = "VARCHAR"
	@JsonIgnore
	@DatabaseField(columnName = "synonyms", useGetSet = true, dataType = DataType.STRING, width = 255  )
	private String synonymstring = "";

	
	public Station() {}
	public Station( String id ) { _id = id; }
	
	public long getUICCode() { return _uicCode;	}
	public Station setUICCode(long _id) { this._uicCode = _id; return this;	}
	
	public String getId() {return _id;}
	public Station setId(String value) { this._id = value; return this; }
	
	public String getType() {		return _type;	}
	public Station setType(String value) {		this._type = value; return this;	}
	
	public String getNameShort() {		return _nameshort;	}
	public Station setNameShort(String value) {		this._nameshort = value; return this;	}
	
	public String getName() {		return _name;	}
	public Station setName(String value ) {		this._name = value;	return this;}
	
	public String getNameLong() {		return _namelong;	}
	public Station setNameLong(String value ) {		this._namelong = value;	return this;}
	
	public String getCountry() {		return _country;	}
	public Station setCountry(String _country) {		this._country = _country;	return this;}
	
	@JsonIgnore
	public double getLat() { return _lat; }
	public Station setLat(double _lat) { this._lat = _lat; return this;}
	
	@JsonIgnore
	public double getLon() { return _lon; }
	public Station setLon(double _lon) { this._lon = _lon; return this;	}
	
	public List<String> getSynonymList() { return _synonyms;	}
	public Station setSynonymList(List<String> _synonyms) {		this._synonyms = _synonyms; return this;	}
	
	public Date getLastUpdate() { return _lastUpdate; }
	public Station setLastUpdate( Date value ) { _lastUpdate = value; return this; }
	
	@JsonIgnore
	public String getSynonymstring() { return StringUtils.join( _synonyms, ";" ); }
	public void setSynonymstring( String value ) { this._synonyms = StringUtils.split( value, ";" ); }
	
	
}
