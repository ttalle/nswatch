package ttg.gelesneeuw.nswatch.data;

import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "version")
public class Version {

	/*
		- id (db)
		- version
		- numeric_version
		- releasedate
		- releasenotes
		- expiredate
		- downloadurl?
	 */
	
	@DatabaseField( columnName = "version", id = true, canBeNull = false )
	private long _version = 0;

	@DatabaseField( columnName = "versionname", width = 20 )
	private String _versionName;
	
	@DatabaseField( columnName = "releasedate", canBeNull = false )
	private Date _releaseDate;
	
	@DatabaseField( columnName = "notes", canBeNull = true, dataType = DataType.LONG_STRING )
	private String _releaseNotes;
	
	@DatabaseField( columnName = "expire", canBeNull = true )
	private Date _expireDate;

	@DatabaseField( columnName = "url", canBeNull = true )
	private String _downloadUrl;
	
	@DatabaseField( columnName = "visible", canBeNull = false, defaultValue = "0")
	private boolean _visible = false;
	
	public long getVersion() { return _version; }
	public Version setVersion( long value ) { _version = value; return this; }
	
	public String getVersionName() { return _versionName; }
	public Version setVersionName( String value ) { _versionName = value; return this; }
	
	public Date getReleaseDate() { return _releaseDate; }
	public Version setReleaseDate( Date value ) { _releaseDate = value; return this; }
	
	public String getReleaseNotes() { return _releaseNotes; }
	public Version setReleaseNotes( String value ) { _releaseNotes = value; return this; }
	
	public Date getExpireDate() { return _expireDate; }
	public Version setExpireDate( Date value ) { _expireDate = value; return this; }
	
	public String getDownloadUrl() { return _downloadUrl; }
	public Version setDownloadUrl( String value ) { _downloadUrl = value; return this; }
	
	public boolean IsVisible() { return _visible; }
	public Version setVisible( boolean value ) { _visible = value; return this; }
}
