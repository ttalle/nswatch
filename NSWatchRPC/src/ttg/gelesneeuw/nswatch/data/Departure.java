package ttg.gelesneeuw.nswatch.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nl.gelesneeuw.utils.text.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "departure")
public class Departure {
	@JsonIgnore()
	@DatabaseField(columnName = "_id", generatedId = true )
	private long _id = 0;
	
	@JsonIgnore()
	@DatabaseField(columnName = "station", canBeNull = false, foreign = true, foreignAutoRefresh = false )
	private Station _station = null;
	
	@DatabaseField(columnName = "departure" )
	private Date _departure = new Date();
	
	@DatabaseField(columnName = "realdeparture" )
	private Date _realdeparture = new Date();
	
	@DatabaseField(columnName = "delay", width = 20 )
	private String _delay  = "";
	
	@DatabaseField(columnName = "delaytext" )
	private String _delaytext = "";
	
	@DatabaseField(columnName = "destination" )
	private String _destination = "";
	
	@DatabaseField(columnName = "traintype" )
	private String _traintype = "";
	
	@DatabaseField(columnName = "route" )
	private String _route = "";
	
	@DatabaseField(columnName = "transporter" )
	private String _transporter = "";
	
	@DatabaseField(columnName = "track" )
	private String _track = "";
	
	@DatabaseField(columnName = "trackchange" )
	private boolean _trackchange = false;
	
	@DatabaseField(columnName = "tip" )
	private String _tip = "";
	
	@DatabaseField( persisted = false )
	private List<String> _notes = new ArrayList<String>();

	// columnDefinition = "VARCHAR",
	@JsonIgnore()
	@DatabaseField(columnName = "notes", useGetSet = true, dataType = DataType.STRING, width = 255 )
	private String notestring = "";
	
	public Departure() {}
	
	@JsonIgnore()
	public long getId() { return _id; }
	public Departure setId( long value ) { _id = value; return this; }
	
	public Station getStation() { return _station; }
	public Departure setStation( Station value ) { _station = value; return this; }
	
	@JsonProperty("station")
	public String getStationId() { return _station.getId(); }
	
	public Date getDeparture() { return _departure; }
	public Departure setDeparture( Date value ) { _departure = value; return this; }

	public Date getRealDeparture() { return _realdeparture; }
	public Departure setRealDeparture( Date value ) { _realdeparture = value; return this; }

	public String getDelay() { return _delay; }
	public Departure setDelay( String value ) { _delay = value; return this; }

	public String getDelayText() { return _delaytext; }
	public Departure setDelayText( String value ) { _delaytext = value; return this; }

	public String getDestination() { return _destination; }
	public Departure setDestination( String value ) { _destination = value; return this; }

	public String getTrainType() { return _traintype; }
	public Departure setTrainType( String value ) { _traintype = value; return this; }

	public String getRoute() { return _route; }
	public Departure setRoute( String value ) { _route = value; return this; }

	public String getTransporter() { return _transporter; }
	public Departure setTransporter( String value ) { _transporter = value; return this; }

	public String getTrack() { return _track; }
	public Departure setTrack( String value ) { _track = value; return this; }

	public boolean getTrackChange() { return _trackchange; }
	public Departure setTrackChange( boolean value ) { _trackchange = value; return this; }
	
	public String getTip() { return _tip; }
	public Departure setTip( String value ) { _tip = value; return this; }

	public List<String> getNotes() { return _notes; }
	public Departure setNotes( List<String> value ) { _notes = value; return this; }
	
	public String getNotestring() { return StringUtils.join( _notes, ";" ); }
	public void setNotestring( String value ) { _notes = StringUtils.split(value, ";" ); }
}
