# NSWatch

## What is this

This is an very old project created around 2013 when I was
a regular user of the NS (Nederlandse Spoorwegen) trains
in the Netherlands.

Since the trains were on a regular, but not THAT regular, 
schedule it was always good to know if there was an delay
or if there were any cancelled trains.

Around that time the NS published an API that offered
details about the current trains, station times and any
delays or obstructions on the rails.

NSWatch is a software combination of a Java based
proxy-service and an Android App. The proxy is an service
for the NS API that functions as a caching proxy and allows
special queries to search for nearest station and the
latest schedule information.

The much more lightweight Android app queries the proxy for
information about the nearest station or any favorited
stations as chosen by the user. The nearest station
information is resolved using the current location
information of the device in combination with the latitude
and longitude of all known NS stations.

## What state is it in?

The application was fully functional at the time. A few
friends were testing it in real life and I used it myself
for up-to-date information.

But, about a year after the introduction of the API, my
personal need for any NS station information was gone.

This meant both testing was getting less intuitive and
a lot less motivation to get it out there. In the
meantime the NS had introduced its own app to the market.

So, in 2013, it was fully functional both proxy and app.

Today, I am not really sure if it compiles and whether
the API is even still available.

## License

Until I've decided what to do its still under copyright.
I will probably release it under the
[GNU Affero General Public License](https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License)
soon.

There are some external libraries included like
[ActionBarSherlock](http://actionbarsherlock.com). These
libraries are all copyright by their respective owners
and you should refer to their respective README or
LICENSE or source files for more information.

## Repository

This repository is a conversion from an old SVN based
repository. Because there was some sentitive data (not
that sensitive though) in the files I couldn't include
the full history and this is just a snapshot.
