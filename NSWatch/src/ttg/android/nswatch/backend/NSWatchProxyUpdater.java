package ttg.android.nswatch.backend;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import nl.gelesneeuw.utils.text.StringUtils;

import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.database.DbDeparture;
import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.DbSubscription;
import ttg.android.nswatch.proxyapi.NSWatchAPIService;
import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.response.RPCNearestResponse;
import ttg.gelesneeuw.nswatch.rpc.response.RPCVersionResponse;
import android.content.Context;
import android.location.Location;
import android.provider.Settings.Secure;
import android.util.Log;

public class NSWatchProxyUpdater extends NSWatchUpdater implements  INSWatchSubscriber {
	private final static String TAG = NSWatchProxyUpdater.class.getName(); 
	
	private static int _registrationAttempts = 0;
	
	private Context _context;
	private NSWatchAPIService _service = null;

	private static final Object _lockUpdate = new Object();

	public NSWatchProxyUpdater( Context context ) {
		super(context);
		
		_context = context;
	}
	
	
	public void UpdateStations() {
		if( !ensureRegistration() )
			return;
		
		Log.d( TAG, "Updating stations..." );
		DbStation dbs = new DbStation(_context);

		List<Station> stations = _service.getStations();

		if( null != stations )
			dbs.setStations(stations);
	}
	
	
	public void UpdateDepartures( List<Station> stations ) {
		if( null == stations || stations.size() < 1 )
			return;
		
		if( !ensureRegistration() )
			return;
		
		DbDeparture dbd = new DbDeparture(_context);
//		DbStation dbs = new DbStation(_context);
//		DbSubscription dbsub = new DbSubscription(_context);
		
		synchronized (_lockUpdate) {
			Log.d( TAG, "Updating departures..." );
	
			try {
				List<String> updatestations = new ArrayList<String>( stations.size() );
				
				for( Station s: stations )
					if( updatestations.add( s.getId() ) );
				
				NSWatchApplication.getInstance().setUpdateRunning(true);
			
				List<Departure> departures = _service.getDepartures( StringUtils.join( updatestations, ";" ) );
		
				if( null != departures ) {
					NSWatchApplication.getInstance().setLastUpdate(new Date() ).save();
					dbd.setDepartures( stations, departures);
				} else {
					NSWatchApplication.getInstance().setLastUpdate(new Date(0) ).save();
				}
			}
			finally {
				NSWatchApplication.getInstance().setUpdateRunning(false);
			}
		}
	}

	public void UpdateDepartures( String station) {
		if( !ensureRegistration() )
			return;
		
		/*
		DbDeparture dbd = new DbDeparture(_context);

		synchronized (_lockUpdate) {
			Log.d( TAG, "Updating departures..." );
	
			try {
				List<Departure> departures = _service.getDepartures( station );
		
				if( null != departures ) {
					dbd.setDepartures( departures);
				}
			}
			finally {
				NSWatchApplication.getInstance().setUpdateRunning(false);
			}
		}
		*/
	}

	@Override
	public void UpdateNearest( Location location ) {
		if( !ensureRegistration() )
			return;
		
		DbDeparture dbd = new DbDeparture(_context);

		synchronized (_lockUpdate) {
			Log.d( TAG, "Updating nearest..." );
	
			try {
				NSWatchApplication.getInstance().setUpdateRunning(true);
			
				// List<Departure> departures = _service.getNearest(location);
				RPCNearestResponse response = _service.getNearest(location);
		
				if( response != null && response.getSuccess() ) {
					List<Departure> departures = response.getDepartures();
					Station station = response.getStation(); 
					
					if( null != departures && null != station ) {
						NSWatchApplication.getInstance().setNearestStation(station.getId() ).save();

						List<Station> stations = new ArrayList<Station>();
						stations.add( new Station("NEAREST") );
						dbd.setDepartures( stations, departures );
					}
				}
			}
			finally {
				NSWatchApplication.getInstance().setUpdateRunning(false);
			}
		}
	}

	@Override
	public RPCVersionResponse getVersionInfo( long version ) {
		if( !ensureRegistration() )
			return null;

		return _service.getVersionInfo(version);
	}


	
	@Override
	public boolean subscribe(String stationid) {
		if( !ensureRegistration() )
			return false;

		return _service.subscribe(stationid);
	}

	@Override
	public boolean unsubscribe(String stationid) {
		if( !ensureRegistration() )
			return false;

		return _service.unsubscribe(stationid);
	}
	

	public boolean ensureRegistration() {

		boolean result = false;
		
		if( null != _service ) {
			result = true;
		} else {
			UUID key = NSWatchApplication.getInstance().getRegistration();
			
			if( null == key ) {
				if( _registrationAttempts++ > 10 ) {
					Log.e( TAG, String.format( "Registration attempt count exceeded max: %d", _registrationAttempts ) );
					return false;
				}
						
				Log.d(TAG, "Activating service" );
				
				NSWatchAPIService tmpservice = new NSWatchAPIService();
				
				UUID newkey = tmpservice.register(
						Secure.getString(_context.getContentResolver(), Secure.ANDROID_ID),
						"anonymous"
					);
				
				if( null != newkey ) {
					Log.d(TAG, "New registration:\n" + newkey.toString() );
					NSWatchApplication.getInstance().setRegistration(newkey).save();
					key = newkey;
				}
			}
			
			if( null != key ) {
				Log.d( TAG, "Initializing NSWatch service..." );
				_service = new NSWatchAPIService( key );
				result = true;
			} else {
				throw new ExceptionInInitializerError("No registration available");
			}
		}
		
		return result;
	}
}
