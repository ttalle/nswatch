package ttg.android.nswatch.backend;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ttg.android.nswatch.NSWatch;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.debug.NSWatchExceptionReporter;
import ttg.android.nswatch.service.NSWatchUpdateService;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.response.RPCVersionResponse;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

public abstract class NSWatchUpdater {
	private static final String TAG = NSWatchUpdater.class.getName();
	
	private final Context _context;
	
	public NSWatchUpdater( Context context ) {
		NSWatchExceptionReporter.register(context);

		_context = context;
	}
	
	public boolean AutoUpdate() {
		boolean result = false;
		
		// Don't evey try to update when we're not connected
		if( !NSWatchApplication.getInstance().isConnected() ) {
			Log.i(TAG, "Update skipped, no connection");
			return false;
		}
		
		synchronized( _lockAutoUpdate ) {
			Calendar deadline = Calendar.getInstance();
			
			Date lastUpdate = NSWatchApplication.getInstance().getLastUpdate();
			
			long delta = ( deadline.getTimeInMillis() - lastUpdate.getTime() ) / 1000L;
			
			deadline.add(Calendar.SECOND, (int)NSWatch.UPDATE_DELTA * -1 );
			
			if( lastUpdate.before( deadline.getTime() ) && !NSWatchApplication.getInstance().getUpdateRunning() ) {
				Log.d( TAG, String.format( "Last update was %d seconds ago, updating", delta ) );
				
				NSWatchApplication.getInstance().setLastUpdate(new Date() ).save();
				
				Intent i = new Intent( _context, NSWatchUpdateService.class );
				_context.startService(i);
				
				result = true;
			} else {
				Log.d(TAG, String.format( "Last update was %d seconds ago or updater running, not updating", delta ) );
				result = false;
			}
		}
		
		return result;
	};


	public final static Object _lockAutoUpdate = new Object();

	public abstract void UpdateStations();
	public abstract void UpdateDepartures( List<Station> stations );
	public abstract void UpdateNearest(Location location);
	public abstract RPCVersionResponse getVersionInfo(long version);
}
