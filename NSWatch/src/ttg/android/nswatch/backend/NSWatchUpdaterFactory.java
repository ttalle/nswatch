package ttg.android.nswatch.backend;

import android.content.Context;

public abstract class NSWatchUpdaterFactory {
	
	private NSWatchUpdaterFactory() { throw new IllegalAccessError(); }
	
	public static NSWatchUpdater getUpdater( Context context ) {
		return new NSWatchProxyUpdater(context);
	}
}
