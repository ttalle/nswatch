package ttg.android.nswatch.backend;

public interface INSWatchSubscriber {
	public boolean subscribe( String stationid );
	public boolean unsubscribe( String stationid );
}
