package ttg.android.nswatch.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ttg.android.nswatch.R;
import ttg.android.nswatch.database.DbDeparture;
import ttg.android.nswatch.utils.TUtils;
import ttg.gelesneeuw.nswatch.data.Departure;
import android.annotation.TargetApi;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
class NSWatchRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
	private static final String TAG = NSWatchRemoteViewsFactory.class.toString();
	private Context _context;
	private int _widgetId;
	
	private List<Departure> _data = new ArrayList<Departure>();

	public NSWatchRemoteViewsFactory(Context context, Intent intent) {
		_context = context;
		_widgetId = intent.getIntExtra( AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
	        
		Log.d( TAG, "Factory created");
	}

	@Override
	public void onCreate() {
		Log.d( TAG, "onCreate called for widgetid: " + _widgetId );
	}

	@Override
	public void onDestroy() {
		Log.d( TAG, "onDestroy called for widgetid: " + _widgetId );
	}

	@Override
	public void onDataSetChanged() {
		Log.d( TAG, "onDataSetChanged called for widgetid: " + _widgetId );
		
        DbDeparture dbd = new DbDeparture( _context );
        
        _data = dbd.getDepartures(_widgetId);
	}

	@Override
	public int getCount() {
		if( null == _data )
			return 0;
		
		return _data.size();
	}

	private int _defaultColor = 0;
	
	public RemoteViews getViewAt(int position) {
//		Log.d(TAG, "getViewAt called:" + position);
		
		RemoteViews rv = new RemoteViews(this._context.getPackageName(), R.layout.item_departure );
		
		Departure item = getItem(position);
		
		final SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault() );
		rv.setTextViewText(R.id.tvTime, format.format( item.getDeparture() ) );
		rv.setTextViewText(R.id.tvDestination, item.getDestination() );
		rv.setTextViewText(R.id.tvTrack, String.format(  "%s: %s", _context.getResources().getString(R.string.text_label_track), item.getTrack() ) );

		if( item.getTrackChange() ) {
			rv.setTextColor(R.id.tvTrack, Color.RED );
		} else {
			rv.setTextColor(R.id.tvTrack, Color.WHITE );
		}
			
		
		if( item.getNotes().size() > 0 ) {
			rv.setTextViewText(R.id.tvNotes, TUtils.join(item.getNotes(), "\n" ) );
			rv.setViewVisibility(R.id.tvNotes, View.VISIBLE );
			rv.setTextColor(R.id.tvNotes, Color.RED );
		}
		else
			rv.setViewVisibility(R.id.tvNotes, View.GONE );

		if( !item.getTip().equals("") )
		{
			rv.setTextViewText(R.id.tvTip, item.getTip() );
			rv.setViewVisibility(R.id.tvTip, View.VISIBLE );
		}
		else
			rv.setViewVisibility(R.id.tvTip, View.GONE );
			
		rv.setTextViewText(R.id.tvDelay, item.getDelayText() );
		rv.setTextColor( R.id.tvDelay, Color.RED );
		
		this.loadItemOnClickExtras(rv, item.getId() );
		
		return rv;
	}

	private void loadItemOnClickExtras(RemoteViews rv, long departureid ) {
		Intent i = new Intent();
		i.putExtra("departure", departureid );
		rv.setOnClickFillInIntent(R.id.rlDepartureItem, i);
		return;
	}

	@Override
	// This allows for the use of a custom loading view
	// which appears between the time that getViewAt(int)
	// is called and returns. If null is returned,
	// a default loading view will be used.
	public RemoteViews getLoadingView() {
		return null;
	}

	@Override
	public int getViewTypeCount() {
		return 1;
	}

	public Departure getItem(int position) {
		return _data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return _data.get(position).getId();
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}
	
	
//	
//	
//	private static final String SERVICE_NAME = "ttg.android.nswatch.service.NSWatchUpdateServiceListView";
//	
//	private INSWatchUpdateService _service;
//	private NSWatchUpdateServiceConnection _connection;
//
//	/**
//	 * This class represents the actual service connection. It casts the bound
//	 * stub implementation of the service to the AIDL interface.
//	 */
//	class NSWatchUpdateServiceConnection implements ServiceConnection {
//
//		public void onServiceConnected(ComponentName name, IBinder boundService) {
//			_service = INSWatchUpdateService.Stub.asInterface((IBinder) boundService);
//			
//			Log.d( TAG, "onServiceConnected() connected");
//			
//			Toast.makeText(_context, "Service connected", Toast.LENGTH_LONG).show();
//		}
//
//		public void onServiceDisconnected(ComponentName name) {
//			_service = null;
//			Log.d( TAG, "onServiceDisconnected() disconnected");
//			Toast.makeText(_context, "Service disconnected", Toast.LENGTH_LONG).show();
//		}
//	}
//
//	/** Binds this activity to the service. */
//	private void initService() {
//		_connection = new NSWatchUpdateServiceConnection();
//		
//		Intent i = new Intent(SERVICE_NAME);
//
//		boolean ret = _context.bindService(i, _connection, Context.BIND_AUTO_CREATE);
//		Log.d(TAG, "initService() bound to " + SERVICE_NAME + " with " + ret);
//	}
//
//	/** Unbinds this activity from the service. */
//	private void releaseService() {
//		_context.unbindService(_connection);
//		_connection = null;
//		Log.d(TAG, "releaseService() unbound.");
//	}
	
};
