package ttg.android.nswatch.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nl.gelesneeuw.utils.text.StringUtils;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.backend.NSWatchUpdater;
import ttg.android.nswatch.backend.NSWatchUpdaterFactory;
import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.debug.NSWatchExceptionReporter;
import ttg.android.nswatch.location.BasicLocationListener;
import ttg.android.nswatch.location.LocationTracker;
import ttg.android.nswatch.widget.WidgetBuilderListView;
import ttg.android.nswatch.widget.WidgetBuilderStatic;
import ttg.gelesneeuw.nswatch.data.Station;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

// public class NSWatchUpdateService extends Service {
public class NSWatchUpdateService extends IntentService {
	private static final String TAG = NSWatchUpdateService.class.toString();

	public NSWatchUpdateService() {
		super(TAG);
	}

	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		NSWatchExceptionReporter.register(this);

		List<Station> stations = null;
		
		if( intent.hasExtra("stations" ) ) {
			stations = parseStationString(intent.getStringExtra("stations"));
		} else {
			stations = (new DbStation(this)).getSubscribedStations(); 
		}		
		
		NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(this);
	
		internalUpdateStations(updater, stations );
//		internalUpdateNearest(this, updater);
		internalUpdateNearest(this);
		
		NSWatchUpdateService.getBuilder().notifyWidgets(getApplicationContext());
	}
	
	private List<Station> parseStationString( String stations ) {
		DbStation dbs = new DbStation(this);

		List<Station> result = new ArrayList<Station>();
		
		List<String> ids = StringUtils.split(stations, ";" );
		
		for( String i: ids ) {
			Station s = dbs.getStation( i );
			
			if( null != s )
				result.add( s );
		}

		return result;
	}
	
	public void updateStations( Context context, String stations ) {
		List<Station> s = parseStationString(stations);
		updateStations( context, stations);
	}
	
	public void updateStations( Context context, Station station ) {
		List<Station> stations = new ArrayList<Station>();
		stations.add( station );
		updateStations( context, stations);
	}
	
	public void updateStations( Context context, List<Station> stations ) {
		NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(context);
		
		internalUpdateStations(updater, stations );
		
		NSWatchUpdateService.getBuilder().notifyWidgets(context.getApplicationContext());
	}
	
	public void updateNearest( Context context ) {
//		NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(context);
		
		internalUpdateNearest( context );
	}
	
	private void internalUpdateStations( NSWatchUpdater updater, List<Station> stations ) {
		if( null != stations && stations.size() > 0 )
			updater.UpdateDepartures( stations );
	}
	
	private void internalUpdateNearest( Context context ) {
		if( NSWatchApplication.getInstance().getUpdateNearest() ) {
			/*
			GPSTracker tracker = new GPSTracker( this );
			
			Location location = tracker.getCoarseLocation();
			
			if( null != location )
				updater.UpdateNearest( location );
			else
				Log.e(TAG, "No known location... not updating nearest");
			*/
			LocationTracker tracker = new LocationTracker( context );
			
//			final NSWatchUpdater u = updater;
			final Context appcontext = context.getApplicationContext();
			
			tracker.setChangedLocationListener(new BasicLocationListener() {
				@Override
				public void onLocationChanged(Location location) {
					if( null != location ) {
						
						new AsyncTask<Location, Void, Void>() {

							@Override
							protected Void doInBackground(Location... params) {
								NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(appcontext);
								updater.UpdateNearest( params[0] );
								NSWatchUpdateService.getBuilder().notifyWidgets(appcontext);
								return null;
							}
						}.execute(location);
						
					}
				}
			});
			
			Calendar limit = Calendar.getInstance();
			
			limit.add( Calendar.MINUTE, -15 );
			
			tracker.getLastBestLocation( 500, limit.getTimeInMillis() );
		}
	}

	/*
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		Log.i(TAG, "Update service started!" );
		
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				GPSTracker tracker = new GPSTracker( NSWatchUpdateService.this );

				// NSWatchUpdater updater = new NSWatchUpdater(NSWatchUpdateService.this);
				NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(NSWatchUpdateService.this);
				
				updater.UpdateDepartures();
				
				if( NSWatchApplication.getInstance().getUpdateNearest() ) {
					
					if( !tracker.canGetLocation() ) {
						tracker.showSettingsAlert();
					} else {
						Location location = tracker.getLocation();
						
						if( null != location )
							updater.UpdateNearest( location );
						else
							Log.e(TAG, "No known location... not updating nearest");
					}
				}

				if( null != tracker )
					tracker.stopUsingGPS();
				
//				IWidgetBuilder builder = getBuilder();
//				builder.notifyWidgets(getApplicationContext());
				NSWatchUpdateService.getBuilder().notifyWidgets(getApplicationContext());
				
				return null;
			}
			
		}.execute();
		
		return START_NOT_STICKY;
	}
	*/
	
	
	
	
	public static IWidgetBuilder getBuilder() {

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Log.d( TAG, "Created NSWatchUpdateServiceStatic implementation");
			return new WidgetBuilderStatic();
		}
		else {
			Log.d( TAG, "Created NSWatchUpdateServiceListView implementation");
			return new WidgetBuilderListView();
		}
	}


}
