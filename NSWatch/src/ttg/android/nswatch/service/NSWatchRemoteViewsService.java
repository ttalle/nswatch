package ttg.android.nswatch.service;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class NSWatchRemoteViewsService extends RemoteViewsService {

	@Override
	public RemoteViewsFactory onGetViewFactory(Intent intent) {
		return new NSWatchRemoteViewsFactory( this.getApplicationContext(), intent );
	}

}
