package ttg.android.nswatch.service;

import android.content.Context;

public interface IWidgetBuilder {
	public void notifyWidgets( Context context );
}
