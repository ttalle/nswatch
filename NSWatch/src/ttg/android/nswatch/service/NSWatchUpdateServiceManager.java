package ttg.android.nswatch.service;


//public class NSWatchUpdateServiceManager implements INSWatchUpdateService {
//
//	private static final String TAG = "UpdateServiceConnection"; 
//	private static final String SERVICE_NAME = "ttg.android.nswatch.service.NSWatchUpdateService";
//	
//	private INSWatchUpdateService _service;
//	private NSWatchUpdateServiceConnection _connection;
//	private Context _context;
//
//	public NSWatchUpdateServiceManager( Context context) {
//		_context = context;
//	}
//	
//	/**
//	 * This class represents the actual service connection. It casts the bound
//	 * stub implementation of the service to the AIDL interface.
//	 */
//	class NSWatchUpdateServiceConnection implements ServiceConnection {
//
//		public void onServiceConnected(ComponentName name, IBinder boundService) {
//			_service = INSWatchUpdateService.Stub.asInterface((IBinder) boundService);
//			
//			Log.d( TAG, "onServiceConnected() connected");
//			
//			Toast.makeText(_context, "Service connected", Toast.LENGTH_LONG).show();
//		}
//
//		public void onServiceDisconnected(ComponentName name) {
//			_service = null;
//			Log.d( TAG, "onServiceDisconnected() disconnected");
//			Toast.makeText(_context, "Service disconnected", Toast.LENGTH_LONG).show();
//		}
//	}
//
//	/** Binds this activity to the service. */
//	public void initService() {
//		_connection = new NSWatchUpdateServiceConnection();
//		
//		Intent i = new Intent(SERVICE_NAME);
//
//		boolean ret = _context.bindService(i, _connection, Context.BIND_AUTO_CREATE);
//		Log.d(TAG, "initService() bound to " + SERVICE_NAME + " with " + ret);
//	}
//
//	/** Unbinds this activity from the service. */
//	public void releaseService() {
//		_context.unbindService(_connection);
//		_connection = null;
//		Log.d(TAG, "releaseService() unbound.");
//	}
//
//	@Override
//	public IBinder asBinder() {
//		return null;
//	}
//
//	@Override
//	public List<Departure> getDepartures() throws RemoteException {
//		List<Departure> result = null;
//		
//		if( null != _service)
//			result = _service.getDepartures();
//		
//		return result;
//	}
//}
