package ttg.android.nswatch;

public abstract class NSWatch {
	public static final String ACTION_REFRESH = "ttg.android.nswatch.REFRESH";
	public static final String ACTION_UPDATE = "ttg.android.nswatch.UPDATE";
	
	public static final long UPDATE_DELTA = 300;
}
