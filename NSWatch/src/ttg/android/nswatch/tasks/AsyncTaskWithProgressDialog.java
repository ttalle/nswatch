package ttg.android.nswatch.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public abstract class AsyncTaskWithProgressDialog<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

	protected Context _context;
	private ProgressDialog _dialog;

	private String _title = "Working";
	private String _message = "Executing background task...";
	
	protected Context getContext() { return _context; }
	
	public AsyncTaskWithProgressDialog( Context context ) {
		_context = context;
	}

	public AsyncTaskWithProgressDialog( Context context, String title, String message ) {
		_context = context;
		_title = title;
		_message = message;
	}

	public AsyncTaskWithProgressDialog<Params, Progress, Result> setTitle( String value ) { _title = value; return this; }
	public AsyncTaskWithProgressDialog<Params, Progress, Result> setMessage( String value ) { _message = value; return this; }

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		_dialog = new ProgressDialog(_context);
		_dialog.setTitle(_title);
		_dialog.setMessage(_message);
		_dialog.show();
	}

	@Override
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		
		_dialog.dismiss();
	}
	
	@Override
	protected abstract Result doInBackground(Params... params);
	
}
