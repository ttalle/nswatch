package ttg.android.nswatch.broadcast;

import java.util.Calendar;

import ttg.android.nswatch.NSWatch;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.backend.NSWatchUpdaterFactory;
import ttg.android.nswatch.debug.NSWatchExceptionReporter;
import ttg.android.nswatch.service.IWidgetBuilder;
import ttg.android.nswatch.service.NSWatchUpdateService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NSWatchEventReceiver extends BroadcastReceiver {
	private static final String TAG = NSWatchEventReceiver.class.toString();
	@Override
	public void onReceive(Context context, Intent intent) {

		NSWatchExceptionReporter.register(context);
		
		Log.i(TAG, String.format( "Event received: %s", intent.getAction() ) );

		String action = intent.getAction();
		
		if( action.equalsIgnoreCase(NSWatch.ACTION_UPDATE) ) {

			if (isInUpdateWindow()) {
				Log.i(TAG, "Update request received; starting Auto Update...");

				NSWatchUpdaterFactory.getUpdater(context).AutoUpdate();
			}

		} else if( action.equalsIgnoreCase(NSWatch.ACTION_REFRESH )
				|| action.equalsIgnoreCase(Intent.ACTION_USER_PRESENT) ) {
		
			Log.i(TAG, "User present event or refresh received; starting Auto Update");
			
			IWidgetBuilder builder = NSWatchUpdateService.getBuilder();
			builder.notifyWidgets(context.getApplicationContext());
			
			if (isInUpdateWindow()) {
				NSWatchUpdaterFactory.getUpdater(context).AutoUpdate();
			}

		} else if( action.equalsIgnoreCase( ConnectivityManager.CONNECTIVITY_ACTION ) ){
			if( isInUpdateWindow() ) {
				ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo ni = cm.getActiveNetworkInfo();
				
				if( ni != null && ni.getState() == NetworkInfo.State.CONNECTED  ) {
					Log.d( TAG, "Connection sate changed to CONNECTED; starting Auto Update");
					NSWatchUpdaterFactory.getUpdater(context).AutoUpdate();
				}
			}	
		} else {
			Log.e(TAG, String.format("Error: no handler for event: %s", intent.getAction() ));
		}
	}


	
	private boolean isInUpdateWindow() {
		NSWatchApplication app = NSWatchApplication.getInstance();
		
		boolean result = true;
		
		if( app.getEnableAutoUpdateWindow() ) {
			Calendar cal = Calendar.getInstance();
				
			int start = ( app.getAutoUpdateWindowStartHour() * 100 ) + app.getAutoUpdateWindowStartMinute();
			int end = ( app.getAutoUpdateWindowEndHour() * 100 ) + app.getAutoUpdateWindowEndMinute();
			int now = ( cal.get( Calendar.HOUR_OF_DAY ) * 100 ) + cal.get(Calendar.MINUTE );

			Log.d( TAG, String.format( "Update window check: %d <= %d < %d", start, now, end ) ); 
			
			result = ( now >= start && now < end );
		}
		
		return result;
	}

}
