package ttg.android.nswatch.widget;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ttg.android.nswatch.MainActivity;
import ttg.android.nswatch.NSWatchAppWidgetProvider;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.R;
import ttg.android.nswatch.database.DbDeparture;
import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.DbSubscription;
import ttg.android.nswatch.service.IWidgetBuilder;
import ttg.android.nswatch.utils.TUtils;
import ttg.gelesneeuw.nswatch.data.Departure;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.widget.RemoteViews;

public class WidgetBuilderStatic implements IWidgetBuilder {
	@Override
	public void notifyWidgets( Context context ) {
		final AppWidgetManager manager = AppWidgetManager.getInstance(context);

		List<Integer> appWidgetIds = new ArrayList<Integer>();

		// int appWidgetIds[] = manager.getAppWidgetIds(new ComponentName(context, NSWatchAppWidgetProvider.class));

		for( int id: manager.getAppWidgetIds(new ComponentName(context, NSWatchAppWidgetProvider.class)))
			appWidgetIds.add( id );

		for( int id: manager.getAppWidgetIds(new ComponentName(context, NSWatchAppWidgetProvider_TwoByFour.class)))
			appWidgetIds.add( id );

		for( int id: manager.getAppWidgetIds(new ComponentName(context, NSWatchAppWidgetProvider_FourByFour.class)))
			appWidgetIds.add( id );

		for (int widget : appWidgetIds) {
			RemoteViews updateViews = buildUpdate(context, widget);

			// Push update for this widget to the home screen
			// ComponentName thisWidget = new ComponentName(context,
			// NSWatchAppWidgetProvider.class);
			manager.updateAppWidget(widget, updateViews);
		}
         

//		appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.lvDepartures);

	}
	
	private static final int[][] ids = {
		 { R.id.tvTime1, R.id.tvDestination1, R.id.tvTrack1, R.id.tvNotes1, R.id.tvTip1, R.id.tvDelay1 }
		,{ R.id.tvTime2, R.id.tvDestination2, R.id.tvTrack2, R.id.tvNotes2, R.id.tvTip2, R.id.tvDelay2 }
		,{ R.id.tvTime3, R.id.tvDestination3, R.id.tvTrack3, R.id.tvNotes3, R.id.tvTip3, R.id.tvDelay3 }
		,{ R.id.tvTime4, R.id.tvDestination4, R.id.tvTrack4, R.id.tvNotes4, R.id.tvTip4, R.id.tvDelay4 }
		,{ R.id.tvTime5, R.id.tvDestination5, R.id.tvTrack5, R.id.tvNotes5, R.id.tvTip5, R.id.tvDelay5 }
		,{ R.id.tvTime6, R.id.tvDestination6, R.id.tvTrack6, R.id.tvNotes6, R.id.tvTip6, R.id.tvDelay6 }
		,{ R.id.tvTime7, R.id.tvDestination7, R.id.tvTrack7, R.id.tvNotes7, R.id.tvTip7, R.id.tvDelay7 }
		,{ R.id.tvTime8, R.id.tvDestination8, R.id.tvTrack8, R.id.tvNotes8, R.id.tvTip8, R.id.tvDelay8 }
		,{ R.id.tvTime9, R.id.tvDestination9, R.id.tvTrack9, R.id.tvNotes9, R.id.tvTip9, R.id.tvDelay9 }
		,{ R.id.tvTime10, R.id.tvDestination10, R.id.tvTrack10, R.id.tvNotes10, R.id.tvTip10, R.id.tvDelay10 }
	};

	private RemoteViews buildUpdate(Context context, int widget ) {
		 // Pick out month names from resources
        Resources res = context.getResources();

        DbDeparture dbd = new DbDeparture( context );
        DbStation dbs = new DbStation(context);
        DbSubscription dbsub = new DbSubscription(context);
        
        List<Departure> departures = dbd.getDepartures(widget);
        
        String station = dbsub.getStationForWidget(widget);
        
     // Create an Intent to launch ExampleActivity
        Intent intent = new Intent( context, MainActivity.class );
//        Intent intent = new Intent( NSWatch.ACTION_REFRESH );
    	intent.putExtra( "station", station );
        
        PendingIntent pendingIntent = PendingIntent.getActivity(context, widget, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT );
        
        if( null == departures || departures.size() < 1 ) {
            RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_compat_loading );
            rv.setOnClickPendingIntent( R.id.widget_layout, pendingIntent);
            return rv;
        }

        // Build an update that holds the updated widget contents
        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_compat);
        rv.setOnClickPendingIntent( R.id.widget_layout, pendingIntent);

        if( station.equalsIgnoreCase("NEAREST" ) && !NSWatchApplication.getInstance().getNearestStation().equalsIgnoreCase("") )
        	rv.setTextViewText(R.id.tvTitle, String.format( "%s: %s", res.getString(R.string.text_label_station), dbs.getStationName( NSWatchApplication.getInstance().getNearestStation() ) ) );
        else
        	rv.setTextViewText(R.id.tvTitle, String.format( "%s: %s", res.getString(R.string.text_label_station), dbs.getStationName(station) ) );

        final SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        
        for( int i = 0; i < Math.min( departures.size(), 10 ); ++i ) {
	        Departure item = departures.get(i);
	        
			rv.setTextViewText(ids[i][0], format.format( item.getDeparture() ) );
			rv.setTextViewText(ids[i][1], item.getDestination() );
			rv.setTextViewText(ids[i][2], String.format(  "%s: %s", context.getResources().getString(R.string.text_label_track), item.getTrack() ) );
			
			if( item.getTrackChange() ) 
				rv.setTextColor(ids[i][2], Color.RED );
			else
				rv.setTextColor(ids[i][2], Color.WHITE );

			if( item.getNotes().size() > 0 ) {
				rv.setTextViewText(ids[i][3], TUtils.join(item.getNotes(), "\n" ) );
				rv.setTextColor(ids[i][3], Color.RED );
				rv.setViewVisibility(ids[i][3], View.VISIBLE );
			}
			else
				rv.setViewVisibility(ids[i][3], View.GONE );
	
			if( !item.getTip().equals("") )
			{
				rv.setTextViewText(ids[i][4], item.getTip() );
				rv.setViewVisibility(ids[i][4], View.VISIBLE );
			}
			else
				rv.setViewVisibility(ids[i][4], View.GONE );
				
			rv.setTextViewText(ids[i][5], item.getDelayText() );
			rv.setTextColor(ids[i][5], Color.RED );
        }


        return rv;
	}
}
