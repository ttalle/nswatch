package ttg.android.nswatch.widget;

import ttg.android.nswatch.service.IWidgetBuilder;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;

public class NSWatchAppWidgetProviderStatic extends AppWidgetProvider {
	private static final String TAG = NSWatchAppWidgetProviderStatic.class.getSimpleName();

//	private NSWatchUpdateServiceManager _service;
//	private static final String SERVICE_NAME = "ttg.android.nswatch.service.NSWatchUpdateServiceStatic";

	public void onEnabled(Context context) {
		// _service = new NSWatchUpdateServiceManager(context);
		// _service.initService();
		// Intent i = new Intent(SERVICE_NAME);
		// context.startService( i );
		
		IWidgetBuilder builder = new WidgetBuilderStatic();
		builder.notifyWidgets(context.getApplicationContext());
	}

	public void onDisabled(Context context) {
		//if( null != _service )
		//	_service.releaseService();
//		Intent i = new Intent(SERVICE_NAME);
//		context.stopService(i);
	}
	
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// update each of the app widgets with the remote adapter
		
		//IWidgetBuilder builder = NSWatchUpdateService.getBuilder();
		IWidgetBuilder builder = new WidgetBuilderStatic();
		builder.notifyWidgets(context);
		
		/*
		for (int i = 0; i < appWidgetIds.length; ++i) {

			Log.w(TAG, "onUpdate method called");
			
			// Get all ids
			ComponentName thisWidget = new ComponentName(context, NSWatchAppWidgetProvider.class);

			int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

			// Build the intent to call the service
			Intent intent = new Intent(context.getApplicationContext(), NSWatchUpdateServiceStatic.class);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);
		}
		*/
	}
}
