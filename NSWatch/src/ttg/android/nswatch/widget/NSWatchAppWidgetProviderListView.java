package ttg.android.nswatch.widget;

import ttg.android.nswatch.R;
import ttg.android.nswatch.R.id;
import ttg.android.nswatch.R.layout;
import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.DbSubscription;
import ttg.android.nswatch.service.NSWatchRemoteViewsService;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class NSWatchAppWidgetProviderListView extends AppWidgetProvider {
	
	private static final int ACTION_DEPARTURE_DETAILS = 10;
	
	public void onEnabled(Context context) {
		super.onEnabled(context);
	}

	public void onDisabled(Context context) {
	}
	
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		// update each of the app widgets with the remote adapter
		for (int i = 0; i < appWidgetIds.length; ++i) {

			// Set up the intent that starts the NSWatchRemoteViewsService,
			// which will
			// provide the views for this collection.
			Intent intent = new Intent(context, NSWatchRemoteViewsService.class);

			// Add the app widget ID to the intent extras.
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
			intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

			// Instantiate the RemoteViews object for the App Widget layout.
			RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget);

			// Set up the RemoteViews object to use a RemoteViews adapter.
			// This adapter connects
			// to a RemoteViewsService through the specified intent.
			// This is how you populate the data.
			rv.setRemoteAdapter( R.id.lvDepartures, intent);

			// The empty view is displayed when the collection has no items.
			// It should be in the same layout used to instantiate the
			// RemoteViews
			// object above.
			rv.setEmptyView(R.id.lvDepartures, R.id.empty_view);

			// Intent dialogintent = new Intent(context, null );
			Intent dialogintent = new Intent();
			PendingIntent pi = PendingIntent.getActivity(context, ACTION_DEPARTURE_DETAILS, dialogintent, Intent.FLAG_ACTIVITY_NEW_TASK );

			rv.setPendingIntentTemplate(R.id.lvDepartures, pi );
			
			
			DbStation dbstat = new DbStation(context);
			DbSubscription dbsub = new DbSubscription(context);
			
			rv.setTextViewText(R.id.tvStation, dbstat.getStationName(dbsub.getStationForWidget(appWidgetIds[i])));
			//
			// Do additional processing specific to this app widget...
			//
			appWidgetManager.updateAppWidget(appWidgetIds[i], rv);
		}
	}

}

