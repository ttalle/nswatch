package ttg.android.nswatch.widget;

import ttg.android.nswatch.NSWatchAppWidgetProvider;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.R;
import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.DbSubscription;
import ttg.android.nswatch.service.IWidgetBuilder;
import android.annotation.TargetApi;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.widget.RemoteViews;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class WidgetBuilderListView implements IWidgetBuilder {
	@Override
	public void notifyWidgets( Context context ) {
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		int appWidgetIds[] = appWidgetManager.getAppWidgetIds( new ComponentName(context, NSWatchAppWidgetProvider.class));
		
        DbSubscription dbsub = new DbSubscription(context);

		for( int widget : appWidgetIds ) {
			String station = dbsub.getStationForWidget(widget);
			
			if( station.equalsIgnoreCase("NEAREST") )
				updateStationName(context, widget, NSWatchApplication.getInstance().getNearestStation() );
		}
		
		appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.lvDepartures);
	}
	
	private void updateStationName( Context context, int widget, String station  ) {
		if( null == station || station.equalsIgnoreCase("") )
			return;
		
		AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
		
		RemoteViews rv = new RemoteViews( context.getPackageName(), R.layout.widget );
		Resources res = context.getResources();
		
        DbStation dbs = new DbStation(context);
        
        String name = dbs.getStationName(station);
		
       	rv.setTextViewText(R.id.tvTitle, String.format( "%s: %s", res.getString(R.string.text_label_station), name ) );

		appWidgetManager.partiallyUpdateAppWidget(widget, rv );
	}

}
