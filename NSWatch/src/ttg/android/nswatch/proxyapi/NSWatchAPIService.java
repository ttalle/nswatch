package ttg.android.nswatch.proxyapi;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import ttg.android.nswatch.NSWatchApplication;
import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import ttg.gelesneeuw.nswatch.rpc.RPCResponseBase;
import ttg.gelesneeuw.nswatch.rpc.response.RPCDepartureResponse;
import ttg.gelesneeuw.nswatch.rpc.response.RPCNearestResponse;
import ttg.gelesneeuw.nswatch.rpc.response.RPCRegistrationResponse;
import ttg.gelesneeuw.nswatch.rpc.response.RPCStationsResponse;
import ttg.gelesneeuw.nswatch.rpc.response.RPCSubscriptionResponse;
import ttg.gelesneeuw.nswatch.rpc.response.RPCVersionResponse;
import android.location.Location;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NSWatchAPIService {
	private final static String TAG = NSWatchAPIService.class.getName();
	
	private UUID _key;
	
	public NSWatchAPIService() {
		_key = null;
	}

	public NSWatchAPIService( UUID key ) {
		_key = key;
	}
	
	public UUID register( String device, String name ) {
        List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("device", device ));
        values.add(new BasicNameValuePair("name", name ));

        RPCRegistrationResponse response = request("register", values, RPCRegistrationResponse.class );
        
        if( null != response ) {
	        if( response.getSuccess() ) {
	        	_key = response.getKey();
	        } else {
	        	Log.e(TAG, "Failed requesting registration: " + response.getMessage() );
	        }
        } else {
        	Log.e(TAG, "Response is null" );
        }
        
		return _key;
	}
	
	public boolean subscribe( String stationid ) {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("key", _key.toString() ));
        values.add(new BasicNameValuePair("subscribe", "true"));
        values.add(new BasicNameValuePair("station", stationid ));

        RPCSubscriptionResponse response = request("subscribe", values, RPCSubscriptionResponse.class );

        boolean result = false;
        
        if( null != response && response.getSuccess() ) {
        	result = true;
        	Log.i( TAG, response.getMessage() );
        } else {
        	Log.e( TAG, "Failed subscribing: " + response.getMessage() );
        }
        
		return result;
	}
	
	public boolean unsubscribe( String stationid ) {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("key", _key.toString() ));
        values.add(new BasicNameValuePair("subscribe", "false"));
        values.add(new BasicNameValuePair("station", stationid ));

        return false;
	}
	
	public List<Station> getStations() {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("key", _key.toString() ));

        RPCStationsResponse response = request("stations", values, RPCStationsResponse.class );

        List<Station> result = null;
        
        if( null != response && response.getSuccess() ) {
        	result = response.getStations();
        	Log.i( TAG, response.getMessage() );
        } else {
        	Log.e( TAG, "Failed requesting stations: " + response.getMessage() );
        }
        
		return result;
	}
	
	public List<Departure> getDepartures() {
		return getDepartures( null );
	}
	
	public List<Departure> getDepartures( String stations ) {
		List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("key", _key.toString() ));

        if( null != stations )
            values.add(new BasicNameValuePair("station", stations ));

        RPCDepartureResponse response = request("departures", values, RPCDepartureResponse.class );

        List<Departure> result = null;
        
		if (null != response) {
			if (response.getSuccess()) {
				result = response.getDepartures();
				Log.i(TAG, response.getMessage());
			} else {
				Log.e(TAG, "Failed requesting departures: " + response.getMessage());
			}
		} else {
			Log.e(TAG, "Complete failure requesting departures, response = null");
		}
        
		return result;
	}
	
//	public List<Departure> getNearest( Location location ) {
	public RPCNearestResponse getNearest( Location location ) {
		
		
		if( null == location )
			return null;
		
		DecimalFormat nf = (DecimalFormat) NumberFormat.getInstance(Locale.US );
		
		List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("key", _key.toString() ));
//        values.add(new BasicNameValuePair("lat", nf.format( location.getLatitude() ) ) );
//        values.add(new BasicNameValuePair("lon", nf.format( location.getLongitude() ) ) );
        values.add(new BasicNameValuePair("lat", String.format( Locale.US, "%.6f", location.getLatitude()) ) );
        values.add(new BasicNameValuePair("lon", String.format( Locale.US, "%.6f", location.getLongitude()) ) );
 
        
        RPCNearestResponse response = request("nearest", values, RPCNearestResponse.class );

//        List<Departure> result = null;
        RPCNearestResponse result = null;
        
        if( null != response && response.getSuccess() ) {
        	// result = response.getDepartures();
        	result = response;
        	Log.i( TAG, response.getMessage() );
        } else if( null != response ) {
        	Log.e( TAG, "Failed requesting departures for nearest: " + response.getMessage() );
        } else {
        	Log.e( TAG, "Complete failure requesting departures for nearest, response = null" );
        }
        
		return result;
	}

	public RPCVersionResponse getVersionInfo(long version) {
		DecimalFormat nf = (DecimalFormat) NumberFormat.getInstance(Locale.US );
		
		List<NameValuePair> values = new ArrayList<NameValuePair>();
        values.add(new BasicNameValuePair("key", _key.toString() ));
        values.add(new BasicNameValuePair("current", String.format( Locale.US, "%d", version ) ) );
        
        RPCVersionResponse response = request("version", values, RPCVersionResponse.class );

        RPCVersionResponse result = null;
        
        if( null != response && response.getSuccess() ) {
        	result = response;
        	Log.i( TAG, response.getMessage() );
        } else if( null != response ) {
        	Log.e( TAG, "Failed requesting version information: " + response.getMessage() );
        } else {
        	Log.e( TAG, "Complete failure requesting version information, response = null" );
        }
        
		return result;
	}

	private <T extends RPCResponseBase> T request( String method, List<NameValuePair> values, Class<T> cls ) {
		String url = String.format("http://ns.gelesneeuw.nl:20699/%s", method );
		T result = null;
		
		try {
			HttpResponse response = doRequest( url, values );
		
			if( null != response ) {
				ObjectMapper mapper = new ObjectMapper();
				
				result = mapper.readValue( response.getEntity().getContent(), cls );
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	private HttpResponse doRequest( String uri, List<NameValuePair> values ) {
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost( uri );
	    HttpResponse response = null;
	    try {
	  
	        httppost.setEntity(new UrlEncodedFormEntity(values));

	        // Execute HTTP Post Request
	        response = httpclient.execute(httppost);
	        
	    } catch (ClientProtocolException e) {
	    	e.printStackTrace();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	    
	    return response;
	}
}
