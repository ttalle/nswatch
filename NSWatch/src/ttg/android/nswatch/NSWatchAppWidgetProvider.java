package ttg.android.nswatch;

import java.util.Calendar;

import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.DbSubscription;
import ttg.android.nswatch.debug.NSWatchExceptionReporter;
import ttg.android.nswatch.provider.NSWatchProvider;
import ttg.android.nswatch.proxyapi.NSWatchAPIService;
import ttg.android.nswatch.service.IWidgetBuilder;
import ttg.android.nswatch.service.NSWatchUpdateService;
import ttg.android.nswatch.widget.NSWatchAppWidgetProviderListView;
import ttg.android.nswatch.widget.NSWatchAppWidgetProviderStatic;
import ttg.gelesneeuw.nswatch.data.Station;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

public class NSWatchAppWidgetProvider extends AppWidgetProvider {

	private static final String ACTION_CLICK = "ACTION_CLICK";
	private static final String TAG = "NSWatchAppWidgetProvider";

	private final AppWidgetProvider _implementation;
	
	public NSWatchAppWidgetProvider() {
		super();

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Log.d( TAG, "Created NSWatchAppWidgetProviderStatic implementation");
			_implementation = new NSWatchAppWidgetProviderStatic();
		}
		else {
			Log.d( TAG, "Created NSWatchAppWidgetProviderListView implementation");
			_implementation = new NSWatchAppWidgetProviderListView();
		}
	}
	
	@Override
	public void onEnabled(Context context) {
		enableAlarmManager(context);
		NSWatchExceptionReporter.register(context);
		
		_implementation.onEnabled(context);
	}
	
	@Override
	public void onDisabled(Context context) {
		disableAlarmManager(context);
		_implementation.onDisabled(context);
	}
	
	
	private class DepartureContentObserver extends ContentObserver {
		private final Context _context;
		
		public DepartureContentObserver( Handler handler, Context context ) {
			super( handler );
			_context = context;
		}
		
		 // Implement the onChange(boolean) method to delegate the change notification to
		 // the onChange(boolean, Uri) method to ensure correct operation on older versions
		 // of the framework that did not have the onChange(boolean, Uri) method.
		 @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
		@Override
		 public void onChange(boolean selfChange) {
		     onChange(selfChange, null);
		 }

		 // Implement the onChange(boolean, Uri) method to take advantage of the new Uri argument.
		 @Override
		 public void onChange(boolean selfChange, Uri uri) {
			IWidgetBuilder builder = NSWatchUpdateService.getBuilder();
			builder.notifyWidgets( _context.getApplicationContext());
		 }
	}
	
	private DepartureContentObserver _observer = null;
	
	protected void enableContentObserver( Context context ) {

		if( null == _observer )
			_observer = new DepartureContentObserver(null, context);
		
		context.getContentResolver().registerContentObserver(NSWatchProvider.DEPARTURE_URI, true, _observer );
	}
	
	protected void disableContentObserver( Context context ) {
		if( null != _observer )
			context.getContentResolver().unregisterContentObserver(_observer);
	}
	
	private PendingIntent _alarm = null;
	
	protected void enableAlarmManager( Context context ) {
		Log.d(TAG, "Setting alarmmanager for update broadcasts");
		
		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		
		// Intent i = new Intent(context, SchedulerEventReceiver.class); // explicit intent
		Intent i = new Intent( NSWatch.ACTION_UPDATE );
		
		if( null == _alarm ) {
			_alarm = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
		}
		
		Calendar now = Calendar.getInstance();
		now.add(Calendar.SECOND, 20);
//		alarmManager.setInexactRepeating(AlarmManager.RTC, now.getTimeInMillis(), AlarmManager.INTERVAL_FIFTEEN_MINUTES, _alarm);
		alarmManager.setInexactRepeating(AlarmManager.RTC, now.getTimeInMillis(), AlarmManager.INTERVAL_HALF_HOUR, _alarm);
		// alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, now.getTimeInMillis(), 60*1000, _alarm);
	}

	protected void disableAlarmManager(Context context) {
		 final AlarmManager m = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);  
		  
		 if( null != _alarm )
			 m.cancel(_alarm);
		 
		 Log.d( TAG, "Disabling alarm for update broadcasts");
	}
	
	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		super.onDeleted(context, appWidgetIds);

		DbSubscription dbs = new DbSubscription(context);
		// DbStation dbstat = new DbStation(context);
		
		for( int id : appWidgetIds )
		{
			dbs.unsubscribeStation(id);
			
			/*
			NSWatchAPIService service = new NSWatchAPIService(NSWatchApplication.getInstance().getRegistration());
			for( Station s : dbstat.getSubscribedStations(id) ) {
				if( s.getId().equalsIgnoreCase("NEAREST") )
					NSWatchApplication.getInstance().setUpdateNearest(false).save();

				service.unsubscribe( s.getId() );
			}
			*/
		}
	}
	
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

		_implementation.onUpdate(context, appWidgetManager, appWidgetIds);
		
	    super.onUpdate(context, appWidgetManager, appWidgetIds);
	}
}
