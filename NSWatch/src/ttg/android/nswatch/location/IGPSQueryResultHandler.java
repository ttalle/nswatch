package ttg.android.nswatch.location;

import android.location.Location;

public interface IGPSQueryResultHandler {
	public abstract void LocationUpdate( Location location );
}
