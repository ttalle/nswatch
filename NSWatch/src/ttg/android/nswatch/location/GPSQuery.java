package ttg.android.nswatch.location;

import android.content.Context;

public class GPSQuery {
	private final Context _context;
	private final IGPSQueryResultHandler _handler;
	
	public GPSQuery( Context context, IGPSQueryResultHandler handler ) {
		_context = context;
		_handler = handler;
	}
	
}
