package ttg.android.nswatch.provider;

import ttg.android.nswatch.database.DbHelper;
import ttg.android.nswatch.database.contract.DepartureContract;
import ttg.android.nswatch.database.contract.StationContract;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

public class NSWatchProvider extends ContentProvider {
	private static final String TAG = NSWatchProvider.class.getName();
	
	private static final String _url = "ttg.android.nswatch.provider";
	
	private static final int STATIONS = 100;
	private static final int STATIONS_ID = 101;

	private static final int DEPARTURES = 200;
	private static final int DEPARTURES_ID = 201;

	public static final Uri STATION_URI = Uri.parse( "content://" + NSWatchProvider._url + "/station");
	public static final Uri DEPARTURE_URI = Uri.parse( "content://" + NSWatchProvider._url + "/departure");

	// Creates a UriMatcher object.
    private static final UriMatcher _uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    private DbHelper _helper = null;
    
    static {
    	_uriMatcher.addURI(_url, "station", STATIONS);
    	_uriMatcher.addURI(_url, "departure", DEPARTURES);
    }
    
    public NSWatchProvider() {
    	super();
	}

	@Override
	public boolean onCreate() {
		Log.d( TAG, "ContentProvider NSWatch created" );
		_helper = new DbHelper(this.getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		Cursor result = null;
		
		SQLiteDatabase db = _helper.getReadableDatabase();
		
		String tablename = "";
		
		switch( _uriMatcher.match( uri ) ) {
		case STATIONS:
			tablename = StationContract.TABLE_NAME;
			if( null == sortOrder )
				sortOrder = StationContract.COLUMN_NAME_NAMELONG + " ASC";
			break;
		case DEPARTURES:
			tablename = DepartureContract.TABLE_NAME;
			if( null == sortOrder )
				sortOrder = DepartureContract.COLUMN_NAME_REALDEPARTURE + " ASC";
			break;
		default:
			Log.e( TAG, String.format( "No provider for: %s", uri ) );
			return null;
		}
		
		Log.d( TAG, String.format( "Querying: %s with select: %s", tablename, selection == null ? "null" : selection ));
		
		result = db.query(
				tablename,					// The table to query
			    projection,					// The columns to return
			    selection,					// The columns for the WHERE clause
			    selectionArgs,				// The values for the WHERE clause
			    null,						// don't group the rows
			    null,						// don't filter by row groups
			    sortOrder					// The sort order
			);

		result.setNotificationUri(getContext().getContentResolver(), uri);
		
		Log.d( TAG, String.format( "Results: %s", result.getCount() ));

		return result;
	}

	@Override
	public String getType(Uri uri) {
		switch( _uriMatcher.match(uri) ) {
		case STATIONS:
			return "vnd.android.cursor.dir/ttg.android.nswatch.provider.station";
		case STATIONS_ID:
			return "vnd.android.cursor.item/ttg.android.nswatch.provider.station";
		case DEPARTURES:
			return "vnd.android.cursor.dir/ttg.android.nswatch.provider.departure";
		case DEPARTURES_ID:
			return "vnd.android.cursor.item/ttg.android.nswatch.provider.departure";
		}
		
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}

}
