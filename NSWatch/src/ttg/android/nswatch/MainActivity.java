package ttg.android.nswatch;

import ttg.android.nswatch.debug.NSWatchExceptionReporter;
import ttg.android.nswatch.fragments.DeparturesFragment;
import ttg.android.nswatch.fragments.SettingsFragment;
import ttg.android.nswatch.fragments.StationsFragment;
import ttg.android.nswatch.fragments.VersionFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class MainActivity extends SherlockFragmentActivity implements IActionListener {
//public class MainActivity extends TabSwipeActivity implements IActionListener {

	private static final String TAG = MainActivity.class.toString();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		NSWatchExceptionReporter.register(this);
		
		setTitle(R.string.title_main);

		initActionBarTabs(savedInstanceState);
	}

	private void initActionBarTabs(Bundle savedInstanceState) {
		final ActionBar bar = getSupportActionBar();

		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);

		bar.addTab(bar.newTab().setText(R.string.tab_stations)
				.setTabListener(new TabListener<StationsFragment>(this, "Stations", StationsFragment.class)));

		bar.addTab(bar.newTab().setText(R.string.tab_departures)
				.setTabListener(new TabListener<DeparturesFragment>(this, "Departures", DeparturesFragment.class)));

		bar.addTab(bar.newTab().setText(R.string.tab_settings)
				.setTabListener(new TabListener<SettingsFragment>(this, "Settings", SettingsFragment.class)));

		bar.addTab(bar.newTab().setText(R.string.tab_version)
				.setTabListener(new TabListener<VersionFragment>(this, "Version", VersionFragment.class)));

		Intent intent = getIntent();
		if( intent.hasExtra("station" ) ) {
			bar.setSelectedNavigationItem(1);
		} else if (savedInstanceState != null) {
			bar.setSelectedNavigationItem(savedInstanceState.getInt("tab", 0));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		 outState.putInt("tab", getSupportActionBar().getSelectedNavigationIndex());
	}

	public static class TabListener<T extends SherlockFragment> implements ActionBar.TabListener {
		private final SherlockFragmentActivity mActivity;
		private final String mTag;
		private final Class<T> mClass;
		private final Bundle mArgs;
		private Fragment mFragment;

		public TabListener(SherlockFragmentActivity activity, String tag, Class<T> clz) {
			this(activity, tag, clz, null);
		}

		public TabListener(SherlockFragmentActivity activity, String tag, Class<T> clz, Bundle args) {
			mActivity = activity;
			mTag = tag;
			mClass = clz;
			mArgs = args;

			// Check to see if we already have a fragment for this tab, probably
			// from a previously saved state. If so, deactivate it, because our
			// initial state is that a tab isn't shown.
			mFragment = mActivity.getSupportFragmentManager().findFragmentByTag(mTag);

			if (mFragment != null && !mFragment.isDetached()) {
				FragmentTransaction ft = mActivity.getSupportFragmentManager().beginTransaction();
				ft.detach(mFragment);
				ft.commit();
			}
		}

		@Override
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			Log.d( TAG, String.format( "Switching tab to %s", mTag ) );
			if (mFragment == null) {
				mFragment = SherlockFragment.instantiate(mActivity, mClass.getName(), mArgs);
				ft.add(android.R.id.tabcontent, mFragment, mTag);
			} else {
				ft.attach(mFragment);
			}
		}

		@Override
		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
			if (mFragment != null) {
				ft.detach(mFragment);
			}
		}

		@Override
		public void onTabReselected(Tab tab, FragmentTransaction ft) {
//			Toast.makeText(mActivity, "Reselected!", Toast.LENGTH_SHORT).show();
		}
	}

	/*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        super.addTab( "Stations", StationsFragment.class, null );
        super.addTab( "Departures", DeparturesFragment.class, null );
    }
    */
	
	@Override
	public void onStationSelected(String code) {
		
		final ActionBar bar = this.getSupportActionBar();
		
		bar.setSelectedNavigationItem(1);
		
		this.getSupportFragmentManager().executePendingTransactions();
		
//		DeparturesFragment fragment = (DeparturesFragment)getSupportFragmentManager().findFragmentById(R.id.tabDepartures );
		DeparturesFragment fragment = (DeparturesFragment)getSupportFragmentManager().findFragmentByTag("Departures");
		
		if( null != fragment ) {
			fragment.showDepartures( code );
		}
		else
			Log.e(TAG, "Can't switch fragment: null" );
	}

}
