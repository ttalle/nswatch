package ttg.android.nswatch.fragments;

import ttg.android.nswatch.IActionListener;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.NSWatchServiceFragment;
import ttg.android.nswatch.R;
import ttg.android.nswatch.database.contract.StationContract;
import ttg.android.nswatch.provider.NSWatchProvider;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TimePicker;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class SettingsFragment extends NSWatchServiceFragment implements LoaderCallbacks<Cursor> {
	private static final String TAG = SettingsFragment.class.toString();
	
	private static final int LOADER_STATIONS = 1;
	
	private int _updateStartHour;
	private int _updateStartMinute;
	
	private int _updateEndHour;
	private int _updateEndMinute;
	
	private SimpleCursorAdapter _adapter;
	private IActionListener _listener;
	
	private CheckBox cbTracking;
	private Button btnAutoUpdateStart;
	private Button btnAutoUpdateEnd;
	private CheckBox cbEnableUpdateWindow;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		return super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate( R.layout.activity_settings, null );
	}
	
    @SuppressLint("NewApi")
	public void  onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
                
        setHasOptionsMenu(true);
        
		final SherlockFragmentActivity activity = this.getSherlockActivity();
        
        cbTracking = (CheckBox)activity.findViewById( R.id.cbTracking );
        btnAutoUpdateStart = (Button)activity.findViewById(R.id.btnAutoUpdateStart);
        btnAutoUpdateEnd = (Button)activity.findViewById(R.id.btnAutoUpdateEnd );
        cbEnableUpdateWindow = (CheckBox)activity.findViewById(R.id.cbEnableUpdateWindow);
        
        final NSWatchApplication app = NSWatchApplication.getInstance();
        
        _updateStartHour = app.getAutoUpdateWindowStartHour();
        _updateStartMinute = app.getAutoUpdateWindowStartMinute();
        _updateEndHour = app.getAutoUpdateWindowEndHour();
        _updateEndMinute = app.getAutoUpdateWindowEndMinute();
        
        updateTimeButtons();
        
        cbTracking.setChecked( app.getUpdateNearest() );
        
        cbTracking.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// NSWatchApplication.getInstance().setUpdateNearest(isChecked).save();
				app.setUpdateNearest(isChecked).save();
			}
		});
        
        cbEnableUpdateWindow.setChecked(app.getEnableAutoUpdateWindow() );

        cbEnableUpdateWindow.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				app.setEnableAutoUpdateWindow(isChecked).save();
			}
		});
        
        btnAutoUpdateStart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new TimePickerDialog(getSherlockActivity(), new OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						setUpdateWindow( hourOfDay, minute, _updateEndHour, _updateEndMinute );
					}
				}, _updateStartHour, _updateStartMinute, true ).show();
			}
		});

        btnAutoUpdateEnd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new TimePickerDialog(getSherlockActivity(), new OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						setUpdateWindow( _updateStartHour, _updateStartMinute, hourOfDay, minute );
					}
				}, _updateEndHour, _updateEndMinute, true ).show();
			}
		});

		// activity.getSupportLoaderManager().initLoader( LOADER_STATIONS, null, this );
    }
    
    private void setUpdateWindow( int starthour, int startminute, int endhour, int endminute ) {
    	boolean error = false;
    	String message = "";
    	
    	if( starthour == endhour ) {
    		if( endminute <= startminute ) {
    			error = true;
    			message = "Error: end should be later than start";
    		}
    	} else if( endhour < starthour ) {
    		error = true;
			message = "Error: end should be later than start";
    	}

    	if( !error ) {
	    	_updateStartHour = starthour;
	    	_updateStartMinute = startminute;
	    	_updateEndHour = endhour;
	    	_updateEndMinute = endminute;
	    	
	    	NSWatchApplication.getInstance()
	    		.setAutoUpdateWindowStartHour(_updateStartHour)
	    		.setAutoUpdateWindowStartMinute(_updateStartMinute)
	    		.setAutoUpdateWindowEndHour(_updateEndHour)
	    		.setAutoUpdateWindowEndMinute(_updateEndMinute)
	    		.save();
	    	
	    	updateTimeButtons();
	    	
    	} else {
    		Toast.makeText(getSherlockActivity(), message, Toast.LENGTH_LONG ).show();
    	}
    }
    
    private void updateTimeButtons() {
    	btnAutoUpdateStart.setText(String.format( "%02d:%02d", _updateStartHour, _updateStartMinute ) );
    	btnAutoUpdateEnd.setText(String.format( "%02d:%02d", _updateEndHour, _updateEndMinute ) );
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            _listener = (IActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IActionListener");
        }
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, com.actionbarsherlock.view.MenuInflater inflater) {
    	super.onCreateOptionsMenu(menu, inflater);
    	
    	inflater.inflate(R.menu.fragment_settings, menu);
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Loader<Cursor> loader = null;
		
		String select = null;
		String[] selectArgs = null;
		
		if( null != args ) {
			if( args.containsKey("filter") ) {
				select = StationContract.COLUMN_NAME_NAMELONG + " LIKE ?";
				selectArgs = new String[] { "%" + args.getString("filter") + "%" };
			}
		}
		
		switch( id ) {
		case LOADER_STATIONS:
			loader = new CursorLoader(getSherlockActivity(), NSWatchProvider.STATION_URI, null, select, selectArgs, null );
		}
		
		Log.d( TAG, "Created loader: " + loader );
		
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		// _adapter.changeCursor(cursor);
		_adapter.swapCursor( cursor );
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
//		_adapter.changeCursor(null);
		_adapter.swapCursor(null);
	}
}
