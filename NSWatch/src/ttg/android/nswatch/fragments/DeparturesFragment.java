package ttg.android.nswatch.fragments;

import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import ttg.android.nswatch.IActionListener;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.NSWatchServiceFragment;
import ttg.android.nswatch.R;
import ttg.android.nswatch.R.id;
import ttg.android.nswatch.R.layout;
import ttg.android.nswatch.R.menu;
import ttg.android.nswatch.R.string;
import ttg.android.nswatch.adapter.DepartureAdapter;
import ttg.android.nswatch.backend.NSWatchUpdater;
import ttg.android.nswatch.backend.NSWatchUpdaterFactory;
import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.contract.DepartureContract;
import ttg.android.nswatch.dialog.DepartureDetailsFragment;
import ttg.android.nswatch.location.GPSTracker;
import ttg.android.nswatch.provider.NSWatchProvider;
import ttg.android.nswatch.service.NSWatchUpdateService;
import ttg.android.nswatch.tasks.AsyncTaskWithProgressDialog;
import ttg.gelesneeuw.nswatch.data.Station;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class DeparturesFragment extends NSWatchServiceFragment implements LoaderCallbacks<Cursor> {
	private static final String TAG = DeparturesFragment.class.toString();
	
	private static final int LOADER_DEPARTURE = 2;

	private ListView lvDepartures;
	private TextView tvStation;

	private IActionListener _listener;
	private DepartureAdapter _adapter;

	private String _stationCode = null;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		return super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate( R.layout.activity_departures, null );
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

		final SherlockFragmentActivity activity = this.getSherlockActivity();
		lvDepartures = (ListView)activity.findViewById(R.id.lvDepartures);
		tvStation = (TextView)activity.findViewById(R.id.tvStation);

		_adapter = new DepartureAdapter(activity, null, DepartureAdapter.FLAG_REGISTER_CONTENT_OBSERVER );
		lvDepartures.setAdapter(_adapter);

		getSherlockActivity().getSupportLoaderManager().initLoader(LOADER_DEPARTURE, null, this );
		
		
		
		
		Intent intent = this.getActivity().getIntent();
		if( intent.hasExtra("station") ) {
			//String code = intent.getStringExtra("station");
			_stationCode = intent.getStringExtra("station");
			showDepartures(_stationCode);
		} else
			tvStation.setText("All");
		
		lvDepartures.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				showDepartureDetails( id );
			}
		});
	}
	
	public void showDepartures(String code) {
		
		String station = code.toUpperCase( Locale.US );
		
		_stationCode = station;

//		DbDeparture dbd = new DbDeparture(this.getActivity());
//		List<Departure> departures = dbd.getDepartures(code);
//		_adapter.setDepartures(departures);
		Bundle args = new Bundle();
		args.putString("filter", station );
		getSherlockActivity().getSupportLoaderManager().restartLoader(LOADER_DEPARTURE, args, this );
	}
	
	private void updateStationHeader( String station ) {

		DbStation dbs = new DbStation(this.getSherlockActivity());
		
		String name = null;
		
		if( null != _stationCode ) {
			if( _stationCode.equalsIgnoreCase("NEAREST") )
				name = dbs.getStationName( NSWatchApplication.getInstance().getNearestStation() );
			else
				name = dbs.getStationName(station);
		}
		if( null != name )
			tvStation.setText( name );
		else
			tvStation.setText( R.string.text_unkown_station );
		
	}
	
	private void showDepartureDetails(long id) {
		FragmentManager fm = this.getSherlockActivity().getSupportFragmentManager();
		DepartureDetailsFragment dialog = new DepartureDetailsFragment();
		
		Bundle args = new Bundle();
		args.putLong("departure", id );
		dialog.setArguments(args);
		
        dialog.show(fm, "departure_details");
	}

	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            _listener = (IActionListener)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IActionListener");
        }
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, com.actionbarsherlock.view.MenuInflater inflater) {
    	super.onCreateOptionsMenu(menu, inflater);
    	
    	inflater.inflate(R.menu.fragment_departures, menu);
    }
    

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.miUpdateDepartures:
				if( null != _stationCode )
					updateDepartures(_stationCode);
				else
					Log.e(TAG, "_stationCode = null");
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

    private void updateDepartures( String code ) {
		new AsyncTaskWithProgressDialog<String,Void,Void>( this.getSherlockActivity(), "Updating...", "Getting an up-to-date list of departures; please wait.." ) {
			@Override
			protected Void doInBackground(String... params) {
				DbStation dbs = new DbStation(DeparturesFragment.this.getSherlockActivity() );
				
				Station station = dbs.getStation(params[0]);

				if( null != station ) {
					/*
					NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(_context);

					if( station.getId().equalsIgnoreCase("NEAREST") ) {
						GPSTracker tracker = new GPSTracker( DeparturesFragment.this.getSherlockActivity() );
						
						Location location = tracker.getCoarseLocation();
						
						if( null != location )
							updater.UpdateNearest( location );
						else
							Log.e(TAG, "No known location... not updating nearest");
					} else {
						updater.UpdateDepartures( Arrays.asList( new Station[] { station } ) );
					}
					*/
					NSWatchUpdateService service = new NSWatchUpdateService();
					
					if( station.getId().equalsIgnoreCase("NEAREST") ) {
						service.updateNearest( _context);
					} else {
						service.updateStations( _context, station );
					}
				} else {
					Log.e( TAG, String.format( "Couldn't update station %s", params[0] ) );
				}
				//Intent i = new Intent( _context, NSWatchUpdateService.class );
				//_context.startService(i);
				
				return null;
			} }.execute( code );
    }

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Loader<Cursor> loader = null;
		
		String select = DepartureContract.COLUMN_NAME_REALDEPARTURE + " > ?";
		String[] selectArgs = new String[] {String.valueOf( (new Date()).getTime() ) };
		
		if( null != args ) {
			if( args.containsKey("filter") ) {
				select = DepartureContract.COLUMN_NAME_STATION + " = ? AND " + DepartureContract.COLUMN_NAME_REALDEPARTURE + " > ?";
				selectArgs = new String[] { args.getString("filter"), String.valueOf( (new Date()).getTime() ) };
			}
		}
		
		switch( id ) {
		case LOADER_DEPARTURE:
			loader = new CursorLoader(getSherlockActivity(), NSWatchProvider.DEPARTURE_URI, null, select, selectArgs, null );
		}
		
		Log.d( TAG, "Created loader: " + loader );
		
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		// _adapter.changeCursor(cursor);
		_adapter.swapCursor( cursor );
		updateStationHeader( _stationCode );
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
//		_adapter.changeCursor(null);
		_adapter.swapCursor(null);
	}

}
