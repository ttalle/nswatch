package ttg.android.nswatch.fragments;

import ttg.android.nswatch.IActionListener;
import ttg.android.nswatch.NSWatchApplication;
import ttg.android.nswatch.NSWatchServiceFragment;
import ttg.android.nswatch.R;
import ttg.android.nswatch.backend.NSWatchUpdater;
import ttg.android.nswatch.backend.NSWatchUpdaterFactory;
import ttg.android.nswatch.database.contract.StationContract;
import ttg.android.nswatch.provider.NSWatchProvider;
import ttg.android.nswatch.tasks.AsyncTaskWithProgressDialog;
import ttg.gelesneeuw.nswatch.rpc.response.RPCVersionResponse;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class VersionFragment extends NSWatchServiceFragment implements LoaderCallbacks<Cursor> {
	private static final String TAG = VersionFragment.class.toString();
	
	private static final int LOADER_STATIONS = 1;
	
	private SimpleCursorAdapter _adapter;
	private IActionListener _listener;
	
	private Button btnCheckUpdate;
	private TextView tvRelease;
	private TextView tvVersionCurrent;
	private TextView tvVersionNew;
	private TextView tvDownload;
	private RelativeLayout rlVersionInformation;
	private TextView tvNoUpdates;
	private TextView tvHeadAbout;
	private TextView tvAbout;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		return super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate( R.layout.activity_version, null );
	}
	
    @SuppressLint("NewApi")
	public void  onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setHasOptionsMenu(false);
        
		final SherlockFragmentActivity activity = this.getSherlockActivity();
        
		btnCheckUpdate = (Button)activity.findViewById(R.id.btnCheckUpdate);
		tvRelease = (TextView)activity.findViewById( R.id.tvRelease);
		tvVersionCurrent = (TextView)activity.findViewById(R.id.tvVersionCurrent);
		tvVersionNew = (TextView)activity.findViewById(R.id.tvVersionNew);
		tvDownload = (TextView)activity.findViewById(R.id.tvDownload );
		tvNoUpdates = (TextView)activity.findViewById(R.id.tvNoUpdates);
		tvAbout = (TextView)activity.findViewById(R.id.tvAbout);
		tvHeadAbout = (TextView)activity.findViewById(R.id.tvHeadAbout);
		rlVersionInformation = (RelativeLayout)activity.findViewById(R.id.rlVersionInformation);
		
		tvAbout.setMovementMethod(LinkMovementMethod.getInstance());
		tvDownload.setMovementMethod(LinkMovementMethod.getInstance());
		
		btnCheckUpdate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				checkUpdate();
			}
		});
		// activity.getSupportLoaderManager().initLoader( LOADER_STATIONS, null, this );
		
		String versionName = NSWatchApplication.getInstance().getVersionName();
		
		tvVersionCurrent.setText( versionName );
		tvHeadAbout.setText( getResources().getString(R.string.version_head_about, versionName ) );
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            _listener = (IActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IActionListener");
        }
    }

    private void checkUpdate() {
    	
    	( new AsyncTaskWithProgressDialog<Long, Void, RPCVersionResponse>(
    			this.getSherlockActivity(),
    			getResources().getString(R.string.version_checking_header ),
    			getResources().getString(R.string.version_checking_message )
    			) {
    		@Override
    		protected RPCVersionResponse doInBackground(Long... params) {
    	    	NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(getContext());
    	    	return updater.getVersionInfo(params[0]);
    		}
    		
    		protected void onPostExecute(RPCVersionResponse result) {
    			if( null != result && null != result.getLatest() && null != result.getCurrent() ) {

	    			btnCheckUpdate.setVisibility(View.GONE);

    				if( result.getLatest().getVersion() == result.getCurrent().getVersion() ) {
    					tvNoUpdates.setVisibility( View.VISIBLE );
    				} else {
		    			tvRelease.setText( Html.fromHtml(result.getLatest().getReleaseNotes() ) );
		    			tvVersionNew.setText( result.getLatest().getVersionName() );
		    			
		    			String url = result.getLatest().getDownloadUrl();
		    			if( null != url && !url.equalsIgnoreCase("") )
		    				tvDownload.setText( Html.fromHtml( String.format( "<a href=\"%s\">Download new version...</a>", url, url) ));
	
	    				rlVersionInformation.setVisibility(View.VISIBLE);
    				}
    			}
    			
    			super.onPostExecute(result);
    		};
    	} ).execute(NSWatchApplication.getInstance().getVersion());
    	
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, com.actionbarsherlock.view.MenuInflater inflater) {
    	super.onCreateOptionsMenu(menu, inflater);
    	
    	inflater.inflate(R.menu.fragment_settings, menu);
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Loader<Cursor> loader = null;
		
		String select = null;
		String[] selectArgs = null;
		
		if( null != args ) {
			if( args.containsKey("filter") ) {
				select = StationContract.COLUMN_NAME_NAMELONG + " LIKE ?";
				selectArgs = new String[] { "%" + args.getString("filter") + "%" };
			}
		}
		
		switch( id ) {
		case LOADER_STATIONS:
			loader = new CursorLoader(getSherlockActivity(), NSWatchProvider.STATION_URI, null, select, selectArgs, null );
		}
		
		Log.d( TAG, "Created loader: " + loader );
		
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		// _adapter.changeCursor(cursor);
		_adapter.swapCursor( cursor );
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
//		_adapter.changeCursor(null);
		_adapter.swapCursor(null);
	}
}
