package ttg.android.nswatch.fragments;

import java.util.ArrayList;
import java.util.List;

import ttg.android.nswatch.IActionListener;
import ttg.android.nswatch.NSWatchServiceFragment;
import ttg.android.nswatch.R;
import ttg.android.nswatch.R.id;
import ttg.android.nswatch.R.layout;
import ttg.android.nswatch.R.menu;
import ttg.android.nswatch.backend.NSWatchUpdater;
import ttg.android.nswatch.backend.NSWatchUpdaterFactory;
import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.contract.StationContract;
import ttg.android.nswatch.provider.NSWatchProvider;
import ttg.android.nswatch.tasks.AsyncTaskWithProgressDialog;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;


public class StationsFragment extends NSWatchServiceFragment implements LoaderCallbacks<Cursor> {
	private static final String TAG = StationsFragment.class.toString();
	
	private static final int LOADER_STATIONS = 1;
	
	private EditText etSearch;
	private ListView lvStations;
	private SimpleCursorAdapter _adapter;
	private IActionListener _listener;
	
	private boolean _subscribedOnly = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		return super.onCreateView(inflater, container, savedInstanceState);
		return inflater.inflate( R.layout.activity_stations, null );
	}
	
    @SuppressLint("NewApi")
	public void  onActivityCreated(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
//        LayoutInflater inflater = LayoutInflater.from(this.getActivity() );
        
//        inflater.inflate(R.layout.test, null);
        
        setHasOptionsMenu(true);
        
		final SherlockFragmentActivity activity = this.getSherlockActivity();
        
        lvStations = (ListView)activity.findViewById( R.id.lvStations );
//		btnRefreshStations = (Button)activity.findViewById(R.id.btnRefreshStations);
		etSearch = (EditText)activity.findViewById(R.id.etSearch);
//
//		DbStation dbs = new DbStation(activity);
//		
//		Cursor cursor = dbs.getSubscribedCursor();
//		activity.startManagingCursor(cursor);

		// the desired columns to be bound
		String[] columns = new String[] { StationContract.COLUMN_NAME_NAME, StationContract.COLUMN_NAME_NAMELONG };
		
		// the XML defined views which the data will be bound to
		int[] to = new int[] { android.R.id.text1, android.R.id.text2 };

		// create the adapter using the cursor pointing to the desired data as
		// well as the layout information
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
			_adapter = new SimpleCursorAdapter(activity, android.R.layout.simple_list_item_2 , null, columns, to );
		else
			_adapter = new SimpleCursorAdapter(activity, android.R.layout.simple_list_item_2 , null, columns, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER );
			

		
		// set this adapter as your ListActivity's adapter
		lvStations.setAdapter(_adapter);

		lvStations.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				DbStation dbs = new DbStation(getActivity());

				String code = dbs.getStationCode(id);
				
				_listener.onStationSelected(code);
			}
		});
		
		etSearch.addTextChangedListener( new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				kickLoader(s.toString());
			}
			
			@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override public void afterTextChanged(Editable s) {}
		} );
		
		activity.getSupportLoaderManager().initLoader( LOADER_STATIONS, null, this );
    }

    private void kickLoader( String s ) {
		Bundle args = new Bundle();
		args.putString("filter", s );
		this.getSherlockActivity().getSupportLoaderManager().restartLoader(LOADER_STATIONS, args, StationsFragment.this );
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            _listener = (IActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement IActionListener");
        }
    }
    
    @Override
    public void onCreateOptionsMenu(Menu menu, com.actionbarsherlock.view.MenuInflater inflater) {
    	super.onCreateOptionsMenu(menu, inflater);
    	
    	inflater.inflate(R.menu.fragment_stations, menu);
    }
    
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.miSubscribedOnly:
				_subscribedOnly = !_subscribedOnly;
				kickLoader(etSearch.getText().toString());
				return true;
			case R.id.miUpdateStations:
				updateStations();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

    private void updateStations() {
		new AsyncTaskWithProgressDialog<Void,Void,Void>( this.getSherlockActivity(), "Updating...", "Getting an up-to-date list of stations; please wait.." ) {
			@Override
			protected Void doInBackground(Void... params) {
				NSWatchUpdater updater = NSWatchUpdaterFactory.getUpdater(_context);
				updater.UpdateStations();
				return null;
			} }.execute();
    }
    
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		Loader<Cursor> loader = null;
		
		String select = null;
		List<String> selectArgs = new ArrayList<String>();
		
		if( _subscribedOnly )
			select = "stationid in ( select station from subscription )";
		else
			select = "1=1";
		
		if( null != args ) {
			if( args.containsKey("filter") ) {
				select += " AND ( " + StationContract.COLUMN_NAME_NAMELONG + " LIKE ? OR " + StationContract.COLUMN_NAME_SYNONYMS + " LIKE ? )";
				String query = "%" + args.getString("filter") + "%";
				selectArgs.add( query );
				selectArgs.add( query );
			}
		}
		
		switch( id ) {
		case LOADER_STATIONS:
			loader = new CursorLoader(getSherlockActivity(), NSWatchProvider.STATION_URI, null, select, selectArgs.toArray(new String[selectArgs.size()]), null );
		}
		
		Log.d( TAG, "Created loader: " + loader );
		
		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		// _adapter.changeCursor(cursor);
		_adapter.swapCursor( cursor );
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
//		_adapter.changeCursor(null);
		_adapter.swapCursor(null);
	}
}
