package ttg.android.nswatch;

import ttg.android.nswatch.database.DbStation;
import ttg.android.nswatch.database.DbSubscription;
import ttg.android.nswatch.database.contract.StationContract;
import ttg.android.nswatch.debug.NSWatchExceptionReporter;
import ttg.android.nswatch.proxyapi.NSWatchAPIService;
import ttg.android.nswatch.service.NSWatchUpdateService;
import ttg.android.nswatch.tasks.AsyncTaskWithProgressDialog;
import android.app.ListActivity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class WidgetConfigurationListActivity extends ListActivity {

	private SimpleCursorAdapter _adapter = null;
	private int _widgetid = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setResult(RESULT_CANCELED);
		
		NSWatchExceptionReporter.register(this);

		initWidgetId();
		initAdapter();
	}
	
	private void initWidgetId() {

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
		    _widgetid = extras.getInt(
		            AppWidgetManager.EXTRA_APPWIDGET_ID,
		            AppWidgetManager.INVALID_APPWIDGET_ID );
		}

	}

	private void initAdapter() {
		DbStation dbs = new DbStation(this);
		
		Cursor cursor = dbs.getCursor();
		
		_adapter = new SimpleCursorAdapter(this,
				R.layout.item_station,
				cursor,
				new String[] { StationContract.COLUMN_NAME_NAME, StationContract.COLUMN_NAME_NAMELONG },
				new int[] { R.id.tvName, R.id.tvLongName },
				0 );
		
		setListAdapter( _adapter );
	}
	
	private void updateWidget() {
		Intent service = new Intent( getApplicationContext(), NSWatchUpdateService.class );
		startService(service);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		DbSubscription dbsub = new DbSubscription(this);
		DbStation dbs = new DbStation(this);
		
		String stationid = dbs.getStationCode(id);
		dbsub.subscribeStation(_widgetid, stationid );
		
		if( stationid.equalsIgnoreCase("NEAREST")) {
			NSWatchApplication.getInstance().setUpdateNearest(true).save();
			Intent resultValue = new Intent();
			resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, _widgetid);
			setResult(RESULT_OK, resultValue);
			finish();
			updateWidget();
			return;
		}

		new AsyncTaskWithProgressDialog<String, Void, Boolean>( this, "Subscribing to station", "Sending subscription request to the service..." ) {

			@Override
			protected Boolean doInBackground(String... params) {
				NSWatchExceptionReporter.register( WidgetConfigurationListActivity.this);
				NSWatchAPIService service = new NSWatchAPIService( NSWatchApplication.getInstance().getRegistration() );
				return service.subscribe(params[0]);
			}
			
			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				
				if( result ) {
					Intent resultValue = new Intent();
					resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, _widgetid);
					setResult(RESULT_OK, resultValue);
					finish();
					updateWidget();
				} else {
					Toast.makeText(_context, "Subscribing failed", Toast.LENGTH_LONG ).show();
				}
				
			}
		}.execute(stationid);
	}

}
