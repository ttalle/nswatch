package ttg.android.nswatch.adapter;

import java.text.SimpleDateFormat;
import java.util.Locale;

import ttg.android.nswatch.R;
import ttg.android.nswatch.database.DbDeparture;
import ttg.android.nswatch.utils.TUtils;
import ttg.gelesneeuw.nswatch.data.Departure;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class DepartureAdapter extends CursorAdapter {
	private static final String TAG = DepartureAdapter.class.getName();
	
//	private Context _context;
//	private List<Departure> _departures = new ArrayList<Departure>();
//	private Cursor _cursor;
	
	public DepartureAdapter( Context context, Cursor cursor, int flags ) {
		super(context, cursor, flags );
	}

	/*
	public void setDepartures( List<Departure> departures ) {
		if( null != departures ) {
			_departures = departures;
			notifyDataSetChanged();
		}
		else
			 _departures = new ArrayList<Departure>();
	}
	*/
	

	private static class ViewHolder {
		TextView tvTime;
		TextView tvDestination;
		TextView tvTrack;
		int tvTrackOriginalColor;
		
		TextView tvNotes;
		TextView tvTip;
		TextView tvDelay;
	}
	
	private final SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault() );
	
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);

//		Log.d(TAG, "newView");
		ViewHolder holder = new ViewHolder();
		View view = inflater.inflate(R.layout.item_departure, parent, false);
		
		holder = new ViewHolder();

		holder.tvTime = (TextView) view.findViewById(R.id.tvTime);
		holder.tvDestination = (TextView) view.findViewById(R.id.tvDestination);
		holder.tvTrack = (TextView) view.findViewById(R.id.tvTrack);
		holder.tvTrackOriginalColor = holder.tvTrack.getTextColors().getDefaultColor();
		
		holder.tvNotes = (TextView) view.findViewById(R.id.tvNotes);
		holder.tvTip = (TextView) view.findViewById(R.id.tvTip);
		holder.tvDelay = (TextView) view.findViewById(R.id.tvDelay);			

		view.setTag(holder);
		
		return view;
	}

//	@Override
//	public View getView(int position, View convertView, ViewGroup parent) {
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
//		Log.d(TAG, "bindView");

//		Departure item = (Departure)getItem(position);
		Departure item = (Departure)DbDeparture.readDepartureValues(cursor);
		
		ViewHolder holder = (ViewHolder)view.getTag();
		
		holder.tvTime.setText( format.format( item.getDeparture() ) );
		holder.tvDestination.setText(item.getDestination());
		holder.tvTrack.setText(item.getTrack());
		holder.tvTrack.setText( String.format(  "%s: %s", context.getResources().getString(R.string.text_label_track), item.getTrack() ) );

		if( item.getTrackChange() ) 
			holder.tvTrack.setTextColor( Color.RED );
		else
			holder.tvTrack.setTextColor( holder.tvTrackOriginalColor );

		if( item.getNotes().size() > 0 ) {
			holder.tvNotes.setText( TUtils.join(item.getNotes(), "\n" ) );
			holder.tvNotes.setVisibility( View.VISIBLE );
			holder.tvNotes.setTextColor(Color.RED);
		}
		else
			holder.tvNotes.setVisibility( View.GONE );

		if( item.getTip().length() > 0 && !item.getTip().equals("") )
		{
			holder.tvTip.setText(item.getTip() );
			holder.tvTip.setVisibility( View.VISIBLE );
		}
		else
			holder.tvTip.setVisibility(View.GONE );
			
		holder.tvDelay.setText( item.getDelayText() );
		holder.tvDelay.setTextColor( Color.RED );
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}
}
