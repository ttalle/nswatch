package ttg.android.nswatch;

import java.util.Date;
import java.util.UUID;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

public class NSWatchApplication extends Application {

    //sensible place to declare a log tag for the application
    public static final String LOG_TAG = "NSWatchApplication";

	private static final String PREF_REGISTRATION_KEY = "registrationcode";
	private static final String PREF_LAST_UPDATE = "lastupdate";
	private static final String PREF_UPDATE_NEAREST = "update_nearest";
	private static final String PREF_NEAREST_STATION = "nearest_station";
	
	private static final String PREF_AUTOUPDATE_START_HOUR = "autoupdate_start_hour";
	private static final String PREF_AUTOUPDATE_START_MINUTE = "autoupdate_start_minute";
	private static final String PREF_AUTOUPDATE_END_HOUR = "autoupdate_end_hour";
	private static final String PREF_AUTOUPDATE_END_MINUTE = "autoupdate_end_minute";
	
    private UUID _registration = null;
    private Date _lastUpdate = new Date(0);
    private boolean _updateRunning = false;
    private boolean _updateNearest = false;
    private String _nearestStation = "";
    private long _version = 0;
    private String _versionName = "";
    private boolean _updateAvailable = false;
    private boolean _enableAutoUpdateWindow = false;
    private int _autoUpdateWindowStartHour = 0;
    private int _autoUpdateWindowStartMinute = 0; 
    private int _autoUpdateWindowEndHour = 0;
    private int _autoUpdateWindowEndMinute = 0;
    
    //instance 
    private static NSWatchApplication _instance = null;

    /**
     * Convenient accessor, saves having to call and cast getApplicationContext() 
     */
    public static NSWatchApplication getInstance() {
        checkInstance();
        return _instance;
    }

    private static void checkInstance() {
        if (_instance == null)
            throw new IllegalStateException("Application not created yet!");
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        //provide an instance for our static accessors
        _instance = this;
    
        Log.d( LOG_TAG, "Application Created" );
        loadSharedPreferences();
        loadVersionInfo();
    }
    
    
    public UUID getRegistration() { return _registration; }
    public NSWatchApplication setRegistration( UUID value ) { _registration = value; return this; }
    
    public Date getLastUpdate() { return _lastUpdate; }
    public NSWatchApplication setLastUpdate( Date value ) { _lastUpdate = value; return this; }
    
    public boolean getUpdateRunning() { return _updateRunning; }
    public NSWatchApplication setUpdateRunning( boolean value ) { _updateRunning = value; return this; }

    public boolean getUpdateNearest() { return _updateNearest; }
    public NSWatchApplication setUpdateNearest( boolean value ) { _updateNearest = value; return this; }
    
    public String getNearestStation() { return _nearestStation; }
    public NSWatchApplication setNearestStation( String value ) { _nearestStation = value; return this; }
    
    public long getVersion() { return _version; }
    public NSWatchApplication setVersion( long value ) { _version = value; return this; }

    public String getVersionName() { return _versionName; }
    public NSWatchApplication setVersionName( String value ) { _versionName = value; return this; }

    public boolean getUpdateAvailable() { return _updateAvailable; }
    public NSWatchApplication setUpdateAvailable( boolean value ) { _updateNearest = value; return this; }
    
    public boolean getEnableAutoUpdateWindow() { return _enableAutoUpdateWindow; }
    public NSWatchApplication setEnableAutoUpdateWindow( boolean value ) { _enableAutoUpdateWindow = value; return this; }
    
    public int getAutoUpdateWindowStartHour() { return _autoUpdateWindowStartHour; }
    public NSWatchApplication setAutoUpdateWindowStartHour( int value ) { _autoUpdateWindowStartHour = value; return this; }

    public int getAutoUpdateWindowStartMinute() { return _autoUpdateWindowStartMinute; }
    public NSWatchApplication setAutoUpdateWindowStartMinute( int value ) { _autoUpdateWindowStartMinute = value; return this; }
    
    public int getAutoUpdateWindowEndHour() { return _autoUpdateWindowEndHour; }
    public NSWatchApplication setAutoUpdateWindowEndHour( int value ) { _autoUpdateWindowEndHour = value; return this; }
    
    public int getAutoUpdateWindowEndMinute() { return _autoUpdateWindowEndMinute; }
    public NSWatchApplication setAutoUpdateWindowEndMinute( int value ) { _autoUpdateWindowEndMinute = value; return this; }
    
    
    
    public boolean isConnected() {
    	boolean result = false;
    	ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	 
    	if( null != cm ) {
    		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    		
    		if( null != activeNetwork )
    			result = activeNetwork.isConnected();
    	}
    	
    	return result;
    }
    
    private void loadVersionInfo() {
		PackageManager pm = this.getPackageManager();
		PackageInfo pi;
		try {
			pi = pm.getPackageInfo(this.getPackageName(), 0);
			_version = pi.versionCode;
			_versionName = pi.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
    }

    public void save()
    {
    	saveSharedPreferences();
    }
    

    private void loadSharedPreferences() {
    	// Restore preferences
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences( this );
        _lastUpdate = new Date( settings.getLong(PREF_LAST_UPDATE, 0 ) );
        _updateNearest = settings.getBoolean(PREF_UPDATE_NEAREST, false );
        _nearestStation = settings.getString( PREF_NEAREST_STATION, "" );

        _autoUpdateWindowStartHour = settings.getInt( PREF_AUTOUPDATE_START_HOUR, 7 );
        _autoUpdateWindowStartMinute = settings.getInt( PREF_AUTOUPDATE_START_MINUTE, 0 );
        _autoUpdateWindowEndHour = settings.getInt( PREF_AUTOUPDATE_END_HOUR, 19 );
        _autoUpdateWindowEndMinute = settings.getInt( PREF_AUTOUPDATE_END_MINUTE, 0 );
        
        String registration = settings.getString( PREF_REGISTRATION_KEY, null );
        if( null != registration )
        	_registration = UUID.fromString( registration );
    }
    
    private void saveSharedPreferences() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences( this );
        SharedPreferences.Editor editor = settings.edit();

        editor.putBoolean(PREF_UPDATE_NEAREST, _updateNearest );
        editor.putString( PREF_NEAREST_STATION, _nearestStation );
        
        editor.putInt(PREF_AUTOUPDATE_START_HOUR, _autoUpdateWindowStartHour);
        editor.putInt(PREF_AUTOUPDATE_START_MINUTE, _autoUpdateWindowStartMinute);
        editor.putInt(PREF_AUTOUPDATE_END_HOUR, _autoUpdateWindowEndHour);
        editor.putInt(PREF_AUTOUPDATE_END_MINUTE, _autoUpdateWindowEndMinute);

        if( null != _registration )
        	editor.putString(PREF_REGISTRATION_KEY, _registration.toString() );
        
        editor.putLong(PREF_LAST_UPDATE, _lastUpdate.getTime() );
        editor.commit();
    }
}
