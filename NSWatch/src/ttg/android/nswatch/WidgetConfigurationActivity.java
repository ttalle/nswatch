package ttg.android.nswatch;

import ttg.android.nswatch.database.DbSubscription;
import ttg.android.nswatch.debug.NSWatchExceptionReporter;
import ttg.android.nswatch.proxyapi.NSWatchAPIService;
import ttg.android.nswatch.service.NSWatchUpdateService;
import ttg.android.nswatch.tasks.AsyncTaskWithProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragmentActivity;

public class WidgetConfigurationActivity extends SherlockFragmentActivity implements IActionListener {

	private int _widgetid = 0;
	
	private CheckBox cbNearest;
	private Button btnSelectNearest;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_widget_configuration);

		NSWatchExceptionReporter.register(this);

		setResult(RESULT_CANCELED);
		
		cbNearest = (CheckBox)findViewById(R.id.cbNearest);
		btnSelectNearest = (Button)findViewById(R.id.btnSelectNearest);
		btnSelectNearest.setEnabled(false);
		
		initWidgetId();
		
		cbNearest.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				btnSelectNearest.setEnabled(isChecked);
			}
		});
		
		btnSelectNearest.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				subscribeToNearest();
			}
		});
	}
	
	private void initWidgetId() {

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
		    _widgetid = extras.getInt(
		            AppWidgetManager.EXTRA_APPWIDGET_ID,
		            AppWidgetManager.INVALID_APPWIDGET_ID );
		}
	}
	
	private void updateWidget() {
		Intent service = new Intent( getApplicationContext(), NSWatchUpdateService.class );
		startService(service);
	}
	
	@Override
	public void onStationSelected(String code) {
		subscribeToStation(code);
	}

	private void subscribeToNearest() {
		DbSubscription dbsub = new DbSubscription(this);
		
		dbsub.subscribeStation(_widgetid, "NEAREST" );
		
		NSWatchApplication.getInstance().setUpdateNearest(true).save();
		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, _widgetid);
		setResult(RESULT_OK, resultValue);
		finish();
		updateWidget();
	}
	
	private void subscribeToStation( String code ) {
		
		if( code.equalsIgnoreCase("NEAREST")) {
			subscribeToNearest();
			return;
		}
		
		DbSubscription dbsub = new DbSubscription(this);
		
		String stationid = code;
		dbsub.subscribeStation(_widgetid, stationid );

		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, _widgetid);
		setResult(RESULT_OK, resultValue);
		finish();
		updateWidget();

		/*
		new AsyncTaskWithProgressDialog<String, Void, Boolean>( this, "Subscribing to station", "Sending subscription request to the service..." ) {

			@Override
			protected Boolean doInBackground(String... params) {
				NSWatchAPIService service = new NSWatchAPIService( NSWatchApplication.getInstance().getRegistration() );
				return service.subscribe(params[0]);
			}
			
			@Override
			protected void onPostExecute(Boolean result) {
				super.onPostExecute(result);
				
				if( result ) {
					Intent resultValue = new Intent();
					resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, _widgetid);
					setResult(RESULT_OK, resultValue);
					finish();
					updateWidget();
				} else {
					Toast.makeText(_context, "Subscribing failed", Toast.LENGTH_LONG ).show();
				}
				
			}
		}.execute(stationid);
		*/
	}
}
