package ttg.android.nswatch;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
public class NSWatchServiceActivity extends FragmentActivity {
	private static final String TAG = "NSWatchServiceActivity";
	private static final String SERVICE_NAME = "ttg.android.nswatch.service.NSWatchUpdateService";

//	private NSWatchUpdateServiceConnection _connection;
//	private INSWatchUpdateService _service = null;

	/**
	 * This class represents the actual service connection. It casts the bound
	 * stub implementation of the service to the AIDL interface.
	 */
//	class NSWatchUpdateServiceConnection implements ServiceConnection {
//
//		public void onServiceConnected(ComponentName name, IBinder boundService) {
//			_service = INSWatchUpdateService.Stub.asInterface((IBinder) boundService);
//			
//			Log.d(NSWatchServiceActivity.TAG, "onServiceConnected() connected");
//			
//			Toast.makeText(NSWatchServiceActivity.this, "Service connected", Toast.LENGTH_LONG).show();
//		}
//
//		public void onServiceDisconnected(ComponentName name) {
//			_service = null;
//			Log.d(NSWatchServiceActivity.TAG, "onServiceDisconnected() disconnected");
//			Toast.makeText(NSWatchServiceActivity.this, "Service disconnected", Toast.LENGTH_LONG).show();
//		}
//	}
//
//	/** Binds this activity to the service. */
//	private void initService() {
//		_connection = new NSWatchUpdateServiceConnection();
//		
//		Intent i = new Intent(SERVICE_NAME);
//
//		boolean ret = bindService(i, _connection, Context.BIND_AUTO_CREATE);
//		Log.d(TAG, "initService() bound to " + SERVICE_NAME + " with " + ret);
//	}
//
//	/** Unbinds this activity from the service. */
//	private void releaseService() {
//		unbindService(_connection);
//		_connection = null;
//		Log.d(TAG, "releaseService() unbound.");
//	}
	
//	protected INSWatchUpdateService getService() {
//		return _service;
//	}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        initService();
    }
	

    @Override
    protected void onDestroy() {
    	super.onDestroy();
//    	releaseService();
    }
	
}
