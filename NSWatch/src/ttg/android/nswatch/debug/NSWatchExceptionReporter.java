package ttg.android.nswatch.debug;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Date;
import java.util.Locale;

import ttg.android.nswatch.R;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

public class NSWatchExceptionReporter implements UncaughtExceptionHandler {
	private static final String RECIPIENT = "exceptions@example.com";

	private Thread.UncaughtExceptionHandler _previousHandler;
	private Context _context;

	private NSWatchExceptionReporter(Context context, UncaughtExceptionHandler previous) {
		_previousHandler = previous;
		_context = context;
	}

	public static void register( Context context ) {
		
		UncaughtExceptionHandler previous = Thread.getDefaultUncaughtExceptionHandler();
		
		if( !( previous instanceof NSWatchExceptionReporter ) )
			Thread.setDefaultUncaughtExceptionHandler( new NSWatchExceptionReporter(context, previous ));
	}
	
	private StatFs getStatFs() {
		File path = Environment.getDataDirectory();
		return new StatFs(path.getPath());
	}

	private long getAvailableInternalMemorySize(StatFs stat) {
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	private long getTotalInternalMemorySize(StatFs stat) {
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return totalBlocks * blockSize;
	}

	private void addInformation(StringBuilder message) {
		message.append("Locale: ").append(Locale.getDefault()).append('\n');
		
		try {
			PackageManager pm = _context.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(_context.getPackageName(), 0);
			
			message.append("Version: ").append(pi.versionName).append('\n');
			message.append("Package: ").append(pi.packageName).append('\n');
			
		} catch (Exception e) {
			Log.e("CustomExceptionHandler", "Error", e);
			message.append("Could not get Version information for ").append( _context.getPackageName() );
		}
		
		message.append("Phone Model: ").append(android.os.Build.MODEL).append('\n');
		message.append("Android Version: ").append(android.os.Build.VERSION.RELEASE).append('\n');
		message.append("Board: ").append(android.os.Build.BOARD).append('\n');
		message.append("Brand: ").append(android.os.Build.BRAND).append('\n');
		message.append("Device: ").append(android.os.Build.DEVICE).append('\n');
		message.append("Host: ").append(android.os.Build.HOST).append('\n');
		message.append("ID: ").append(android.os.Build.ID).append('\n');
		message.append("Model: ").append(android.os.Build.MODEL).append('\n');
		message.append("Product: ").append(android.os.Build.PRODUCT).append('\n');
		message.append("Type: ").append(android.os.Build.TYPE).append('\n');
		
		StatFs stat = getStatFs();
		message.append("Total Internal memory: ").append(getTotalInternalMemorySize(stat)).append('\n');
		message.append("Available Internal memory: ").append(getAvailableInternalMemorySize(stat)).append('\n');
	}

	@Override
	public void uncaughtException(Thread t, Throwable e) {
		try {
			StringBuilder report = new StringBuilder();
			Date curDate = new Date();
			report.append("Error Report collected on : ")
					.append(curDate.toString()).append('\n').append('\n');

			report.append("Information:").append('\n');

			addInformation(report);

			report.append('\n').append('\n');
			report.append("Stack:\n");
			
			final Writer result = new StringWriter();
			final PrintWriter printWriter = new PrintWriter(result);
			
			e.printStackTrace(printWriter);
			report.append(result.toString());
			printWriter.close();
			report.append('\n');
			report.append("****  End of current Report ***");
			
			Log.e("UncaughtException", report.toString() );
			
			sendErrorMail(report);
			
		} catch (Throwable ignore) {
			Log.e(NSWatchExceptionReporter.class.getName(), "Error while sending error e-mail", ignore);
		}
		
		if( null != _previousHandler )
			_previousHandler.uncaughtException(t, e);
	}

	private void sendErrorMail(StringBuilder errorContent) {
		Intent sendIntent = new Intent(Intent.ACTION_SEND);
		
		String appname = _context.getResources().getString( R.string.app_name );
		
		String subject = appname + " crashed! Fix it!";
		
		StringBuilder body = new StringBuilder( appname );
		
		body.append('\n').append('\n');
		body.append(errorContent).append('\n').append('\n');
		
		sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { RECIPIENT });
		sendIntent.putExtra(Intent.EXTRA_TEXT, body.toString());
		sendIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		sendIntent.setType("message/rfc822");
		
		_context.startActivity(Intent.createChooser(sendIntent, "Error Report"));
	}
}
