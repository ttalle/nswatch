package ttg.android.nswatch.dialog;

import java.text.SimpleDateFormat;
import java.util.Locale;

import nl.gelesneeuw.utils.text.StringUtils;
import ttg.android.nswatch.R;
import ttg.android.nswatch.database.DbDeparture;
import ttg.android.nswatch.database.DbStation;
import ttg.gelesneeuw.nswatch.data.Departure;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DepartureDetailsFragment extends DialogFragment {

	public DepartureDetailsFragment() {
		// Required default constructor
	}
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// The only reason you might override this method when using onCreateView() is
		// to modify any dialog characteristics. For example, the dialog includes a
		// title by default, but your custom layout might not need it. So here you can
		// remove the dialog title, but you must call the superclass to get the Dialog.
		Dialog dialog = super.onCreateDialog(savedInstanceState);
//		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		return dialog;
	}
	
	/**
	 * The system calls this to get the DialogFragment's layout, regardless of
	 * whether it's being displayed as a dialog or an embedded fragment.
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout to use as dialog or embedded fragment
		return inflater.inflate(R.layout.dialog_departure_details, container, false);
	}

	private final SimpleDateFormat format = new SimpleDateFormat("HH:mm", Locale.getDefault() );

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Bundle args = getArguments();

		if (args.containsKey("departure")) {
			long departureid = args.getLong("departure");

			DbDeparture dbd = new DbDeparture(this.getActivity());
			DbStation dbs = new DbStation(this.getActivity());

			Departure departure = dbd.getDeparture(departureid);

			if (null != departure) {
				
				final Resources res = this.getResources();
				
				String stationname = dbs.getStationName(departure.getStationId());

				this.getDialog().setTitle( String.format( res.getString(R.string.details_departure_title),
					departure.getDestination(),
					format.format( departure.getRealDeparture() )
				) );

				String formatteddeparture = String.format(res.getString(R.string.details_departure_format), 
					format.format( departure.getDeparture() ),
					departure.getDelayText(),
					departure.getTrack()
				);

				((TextView) view.findViewById(R.id.tvStation)).setText( stationname );
				((TextView) view.findViewById(R.id.tvDestination)).setText(departure.getDestination());
				((TextView) view.findViewById(R.id.tvDeparture)).setText( formatteddeparture );
				((TextView) view.findViewById(R.id.tvTrainType)).setText(departure.getTrainType());
				((TextView) view.findViewById(R.id.tvRoute)).setText(departure.getRoute());
				((TextView) view.findViewById(R.id.tvTransporter)).setText(departure.getTransporter());
				((TextView) view.findViewById(R.id.tvTip)).setText(departure.getTip());

				if( departure.getNotes().size() > 0 )
					((TextView) view.findViewById(R.id.tvNotes)).setText( StringUtils.join( departure.getNotes(), "\n" ) );
				else
					((TextView) view.findViewById(R.id.tvNotes)).setText( "-" );
			}
		}
	}
	

}
