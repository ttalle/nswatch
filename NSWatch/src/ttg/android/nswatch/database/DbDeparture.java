package ttg.android.nswatch.database;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ttg.android.nswatch.database.contract.DepartureContract;
import ttg.android.nswatch.database.contract.StationContract;
import ttg.android.nswatch.provider.NSWatchProvider;
import ttg.android.nswatch.utils.TUtils;
import ttg.gelesneeuw.nswatch.data.Departure;
import ttg.gelesneeuw.nswatch.data.Station;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbDeparture {
	private Context _context = null;
	private DbHelper _helper = null;
	
	
	public DbDeparture( Context context ) {
		_context = context;
		_helper = new DbHelper(_context);
	}

	public List<Departure> getDepartures( String code ) {
		List<Departure> departures= new ArrayList<Departure>();
		SQLiteDatabase db = _helper.getReadableDatabase();

		final String sortOrder = DepartureContract.COLUMN_NAME_REALDEPARTURE + " ASC";

		Cursor result = db.query( true, 
				DepartureContract.TABLE_NAME, 
				null, 
				DepartureContract.COLUMN_NAME_STATION + " = ? AND " + DepartureContract.COLUMN_NAME_REALDEPARTURE + " > ?",
				new String[] { code, String.valueOf( new Date().getTime() ) },
				null, null, sortOrder, null );
		
		while( result.moveToNext() )
			departures.add( readDepartureValues( result ) );

		db.close();
		
		return departures;
	}
	
	public List<Departure> getDepartures( int widget ) {
		List<Departure> departures= new ArrayList<Departure>();
		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor result = db.rawQuery( "select departure.* from departure inner join subscription on departure.station = subscription.station where subscription.widget = ? and departure.realdeparture > ? order by departure.departure asc", new String[] { String.valueOf( widget ), String.valueOf( new Date().getTime() ) } );
		
		while( result.moveToNext() )
			departures.add( readDepartureValues( result ) );

		db.close();
		
		return departures;
	}

	public Departure getDeparture( long id ) {
		Departure departure = null;
		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor result = db.rawQuery( "select * from departure where _id = ?", new String[] { String.valueOf(id) } );
		
		if( result.moveToNext() )
			departure = readDepartureValues( result );

		db.close();
		
		return departure;
	}

	public boolean setDepartures( List<Station> stations, List<Departure> departures ) {
		SQLiteDatabase db = _helper.getWritableDatabase();
		
		boolean result = false;
		
		db.beginTransaction();
		
		for( Station station: stations ) {
			db.delete(DepartureContract.TABLE_NAME, DepartureContract.COLUMN_NAME_STATION + " = ?", new String[] { station.getId() } );
		}
		
		try {
			for( Departure d: departures ) {
				ContentValues values = new ContentValues();
				
				writeDepartureValues( d, values );
				
				long newid = db.insert(DepartureContract.TABLE_NAME, null, values );
				
				if( newid > 0 ) {
					d.setId(newid);
				} else {
					throw new Exception("Error while inserting new station in database" );
				}	
			}
			
			db.setTransactionSuccessful();
			
			_context.getContentResolver().notifyChange(NSWatchProvider.DEPARTURE_URI, null);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			db.endTransaction();
			db.close();
		}
		
		return result;
	}

	public boolean setDepartures( List<Departure> departures) {
		SQLiteDatabase db = _helper.getWritableDatabase();
		
		boolean result = false;
		
		db.beginTransaction();

		db.delete(DepartureContract.TABLE_NAME, null, null );
		
		try {
			for( Departure d: departures ) {
				ContentValues values = new ContentValues();
				
				writeDepartureValues( d, values );
				
				long newid = db.insert(DepartureContract.TABLE_NAME, null, values );
				
				if( newid > 0 ) {
					d.setId(newid);
				} else {
					throw new Exception("Error while inserting new station in database" );
				}	
			}
			
			db.setTransactionSuccessful();
			_context.getContentResolver().notifyChange(NSWatchProvider.DEPARTURE_URI, null);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			db.endTransaction();
			db.close();
		}
		
		return result;
	}

	

	public Cursor getCursor( String code ) {

		String sortOrder = DepartureContract.COLUMN_NAME_REALDEPARTURE + " ASC";

		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor c = db.query(
			StationContract.TABLE_NAME,	// The table to query
		    null,						// The columns to return
		    DepartureContract.COLUMN_NAME_STATION + " = ? AND " + DepartureContract.COLUMN_NAME_REALDEPARTURE + " > ?",	// The columns for the WHERE clause
		    new String[] { code, String.valueOf( new Date().getTime() ) },		// The values for the WHERE clause
		    null,						// don't group the rows
		    null,						// don't filter by row groups
		    sortOrder					// The sort order
		);
		
		return c;
	}

	/*
		private String _station = "";
	private Date _departure = new Date();
	private String _delay  = "";
	private String _delaytext = "";
	private String _destination = "";
	private String _traintype = "";
	private String _route = "";
	private String _transporter = "";
	private String _track = "";
	private boolean _trackchange = false;
	private String _tip = "";
	 */
	
	public static Departure readDepartureValues( Cursor cursor ) {
		return new Departure()
			.setId( cursor.getLong(DepartureContract.COLUMN_INDEX_ID ) )
			.setDeparture(new Date(cursor.getLong(DepartureContract.COLUMN_INDEX_DEPARTURE )))
			.setRealDeparture(new Date(cursor.getLong(DepartureContract.COLUMN_INDEX_REALDEPARTURE )))
			.setStation( new Station( cursor.getString(DepartureContract.COLUMN_INDEX_STATION ) ) )
			.setDelay( cursor.getString(DepartureContract.COLUMN_INDEX_DELAY ) )
			.setDelayText( cursor.getString(DepartureContract.COLUMN_INDEX_DELAYTEXT ) )
			.setDestination( cursor.getString(DepartureContract.COLUMN_INDEX_DESTINATION ) )
			.setTrainType( cursor.getString(DepartureContract.COLUMN_INDEX_TRAINTYPE ) )
			.setRoute( cursor.getString(DepartureContract.COLUMN_INDEX_ROUTE ) )
			.setTransporter( cursor.getString(DepartureContract.COLUMN_INDEX_TRANSPORTER ) )
			.setTrack( cursor.getString(DepartureContract.COLUMN_INDEX_TRACK ) )
			.setTrackChange( cursor.getInt(DepartureContract.COLUMN_INDEX_TRACKCHANGE ) > 0 )
			.setTip( cursor.getString(DepartureContract.COLUMN_INDEX_TIP ) )
			.setNotes( TUtils.split( cursor.getString(DepartureContract.COLUMN_INDEX_NOTES ), ";" ) );
	}
	
	public static void writeDepartureValues( Departure departure, ContentValues values ) {
		values.put( DepartureContract.COLUMN_NAME_STATION, departure.getStation().getId() );
		values.put( DepartureContract.COLUMN_NAME_DEPARTURE, departure.getDeparture().getTime() );
		values.put( DepartureContract.COLUMN_NAME_REALDEPARTURE, departure.getRealDeparture().getTime() );
		values.put( DepartureContract.COLUMN_NAME_DELAY, departure.getDelay() );
		values.put( DepartureContract.COLUMN_NAME_DELAYTEXT, departure.getDelayText() );
		values.put( DepartureContract.COLUMN_NAME_DESTINATION, departure.getDestination() );
		values.put( DepartureContract.COLUMN_NAME_TRAINTYPE, departure.getTrainType() );
		values.put( DepartureContract.COLUMN_NAME_ROUTE, departure.getRoute() );
		values.put( DepartureContract.COLUMN_NAME_TRANSPORTER, departure.getTransporter() );
		values.put( DepartureContract.COLUMN_NAME_TRACK, departure.getTrack() );
		values.put( DepartureContract.COLUMN_NAME_TRACKCHANGE, departure.getTrackChange() );
		values.put( DepartureContract.COLUMN_NAME_TIP, departure.getTip() );
		values.put( DepartureContract.COLUMN_NAME_NOTES, TUtils.join( departure.getNotes(), ";" ) );
	}


	
}
