package ttg.android.nswatch.database;

import ttg.android.nswatch.database.contract.SubscriptionContract;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DbSubscription {
	private final static String TAG = DbSubscription.class.toString();
	
	private Context _context = null;
	private DbHelper _helper = null;
	
	
	public DbSubscription( Context context ) {
		_context = context;
		_helper = new DbHelper(_context);
	}

	public String getStationForWidget( int widget ) {
		String code = "";

		SQLiteDatabase db = _helper.getReadableDatabase();
		
		Cursor result = db.query( true,
				SubscriptionContract.TABLE_NAME,
				new String[] { SubscriptionContract.COLUMN_NAME_STATIONID },
				SubscriptionContract.COLUMN_NAME_WIDGETID + " = ?",
				new String[] { String.valueOf( widget ) },
				null, null, null, null );
		
		while( result.moveToNext() )
			code = result.getString(0);

		result.close();
		db.close();
		
		return code;
	}
	public boolean subscribeStation( int widget, String code ) {

		Log.i(TAG, String.format( "Subscribing widget %d to %s", widget, code ));

		SQLiteDatabase db = _helper.getReadableDatabase();

		ContentValues values = new ContentValues();
		
		values.put( SubscriptionContract.COLUMN_NAME_WIDGETID, widget );
		values.put( SubscriptionContract.COLUMN_NAME_STATIONID, code );

		boolean result = false;
		
		try {
			db.beginTransaction();
			
			db.delete(SubscriptionContract.TABLE_NAME, "widget = ?", new String[] { String.valueOf( widget) } );
			long results = db.insert(SubscriptionContract.TABLE_NAME, null, values );
			
			db.setTransactionSuccessful();
			result = ( results == 1 );
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			db.endTransaction();
			db.close();
		}
	
		return result;
	}
	
	
	public boolean unsubscribeStation( int widget ) {
		
		Log.i(TAG, String.format( "Unsubscribing widget %d", widget ));
		
		SQLiteDatabase db = _helper.getReadableDatabase();

		int results = 0;
		
		try {
			results = db.delete(SubscriptionContract.TABLE_NAME, "widget = ?", new String[] { String.valueOf( widget) } );
		}
		finally {
			db.close();
		}

		return results > 0;
	}
}
