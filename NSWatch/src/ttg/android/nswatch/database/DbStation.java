package ttg.android.nswatch.database;

import java.util.ArrayList;
import java.util.List;

import ttg.android.nswatch.database.contract.StationContract;
import ttg.android.nswatch.provider.NSWatchProvider;
import ttg.android.nswatch.utils.TUtils;
import ttg.gelesneeuw.nswatch.data.Station;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbStation {
	private Context _context = null;
	private DbHelper _helper = null;
	
	
	public DbStation( Context context ) {
		_context = context;
		_helper = new DbHelper(_context);
	}
	
	public List<Station> getStations() {
		// How you want the results sorted in the resulting Cursor
		String sortOrder = StationContract.COLUMN_NAME_NAMELONG + " ASC";
				
		List<Station> stations= new ArrayList<Station>();
		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor result = db.query( true, StationContract.TABLE_NAME, null, null, null, null, null, sortOrder, null );
		
		while( result.moveToNext() )
			stations.add( readStationValues( result ) );

		result.close();
		db.close();
		
		return stations;
	}

	/** Get the station code for the given database id
	 * @param id The database id (_id) column
	 * @return A station code or empty string
	 */
	public String getStationCode( long id ) {
		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor result = db.query( true,
				StationContract.TABLE_NAME,
				new String[] { StationContract.COLUMN_NAME_STATIONID },
				StationContract._ID + " = ?",
				new String[] { String.valueOf( id ) },
				null, null, null, null );
		
		String code = "";
		
		while( result.moveToNext() )
			code = result.getString(0);

		result.close();
		db.close();
		
		return code;
	}
	

	public String getStationName( String code ) {
		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor result = db.query( true,
				StationContract.TABLE_NAME,
				new String[] { StationContract.COLUMN_NAME_NAMELONG },
				StationContract.COLUMN_NAME_STATIONID + " = ?",
				new String[] { code },
				null, null, null, null );
		
		String name = "";
		
		while( result.moveToNext() )
			name = result.getString(0);

		result.close();
		db.close();
		
		return name;
	}

	public Station getStation( String code ) {
		SQLiteDatabase db = _helper.getReadableDatabase();

		Station station = null;

		Cursor result = db.query( true,
				StationContract.TABLE_NAME,
				null,
				StationContract.COLUMN_NAME_STATIONID + " = ?",
				new String[] { code },
				null, null, null, null );
		
		if( result.moveToNext() )
			station = readStationValues(result);

		result.close();
		db.close();
		
		return station;
	}

	public List<Station> getSubscribedStations( ) {
		List<Station> stations= new ArrayList<Station>();
		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor result = db.rawQuery("select station.* from station where stationid in ( select station from subscription where NOT station = 'NEAREST' ) order by station.namelong ASC", null );
		
		while( result.moveToNext() )
			stations.add( readStationValues( result ) );

		result.close();
		db.close();
		
		return stations;
	}
	
	public List<Station> getSubscribedStations( int widget ) {
		List<Station> stations= new ArrayList<Station>();
		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor result = db.rawQuery("select station.* from station inner join subscription on station.stationid = subscription.station where subscription.widget = ? order by station.namelong ASC", new String[] { String.valueOf( widget ) } );

		try {
		while( result.moveToNext() )
			stations.add( readStationValues( result ) );
		}
		finally
		{
			result.close();
			db.close();
		}
		return stations;
	}

	
	public boolean setStations( List<Station> stations ) {
		SQLiteDatabase db = _helper.getWritableDatabase();
		
		boolean result = false;
		
		db.beginTransaction();
		
		db.delete(StationContract.TABLE_NAME, null, null );
		
		try {
			for( Station s: stations ) {
				ContentValues values = new ContentValues();
				
				writeStationValues( s, values );
				
				long newid = db.insert(StationContract.TABLE_NAME, null, values );
				
				if( newid > 0 ) {
					s.setUICCode(newid);
				} else {
					throw new Exception("Error while inserting new station in database" );
				}	
			}
			
			db.setTransactionSuccessful();
			
			_context.getContentResolver().notifyChange(NSWatchProvider.STATION_URI, null);
			result = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			db.endTransaction();
			db.close();
		}
		
		return result;
	}
	
	public Cursor getCursor() {

		// How you want the results sorted in the resulting Cursor
		String sortOrder = StationContract.COLUMN_NAME_NAMELONG + " ASC";

		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor c = db.query(
			StationContract.TABLE_NAME,	// The table to query
		    null,						// The columns to return
		    null,						// The columns for the WHERE clause
		    null,						// The values for the WHERE clause
		    null,						// don't group the rows
		    null,						// don't filter by row groups
		    sortOrder					// The sort order
		);
		
		return c;
	}

	public Cursor getSubscribedCursor() {

		SQLiteDatabase db = _helper.getReadableDatabase();

		Cursor c = db.rawQuery("select * from station where stationid in ( select station from subscription ) order by station.namelong ASC", null );
		
		return c;
	}

	private void writeStationValues(Station station, ContentValues values) {
		values.put(StationContract.COLUMN_NAME_TYPE, station.getType() );
		values.put(StationContract.COLUMN_NAME_STATIONID, station.getId() );
		values.put(StationContract.COLUMN_NAME_NAMESHORT, station.getNameShort() );
		values.put(StationContract.COLUMN_NAME_NAME, station.getName() );
		values.put(StationContract.COLUMN_NAME_NAMELONG, station.getNameLong() );
		values.put(StationContract.COLUMN_NAME_COUNTRY, station.getCountry() );
		values.put(StationContract.COLUMN_NAME_LATITUDE, station.getLat() );
		values.put(StationContract.COLUMN_NAME_LONGITUDE, station.getLon() );
		values.put(StationContract.COLUMN_NAME_SYNONYMS, station.getSynonymstring() );
	}
	
	private Station readStationValues( Cursor c ) {
		Station station = new Station();
		
		station.setUICCode( c.getLong(StationContract.COLUMN_INDEX_ID ));
		station.setId( c.getString(StationContract.COLUMN_INDEX_STATIONID ));
		station.setType( c.getString(StationContract.COLUMN_INDEX_TYPE ));
		station.setNameShort( c.getString(StationContract.COLUMN_INDEX_NAMESHORT ));
		station.setName( c.getString(StationContract.COLUMN_INDEX_NAME ));
		station.setNameLong( c.getString(StationContract.COLUMN_INDEX_NAMELONG ));
		station.setCountry( c.getString(StationContract.COLUMN_INDEX_COUNTRY ));
		station.setLat( c.getLong(StationContract.COLUMN_INDEX_LATITUDE ));
		station.setLon( c.getLong(StationContract.COLUMN_INDEX_LONGITUDE ));
		station.setSynonymstring( c.getString(StationContract.COLUMN_INDEX_SYNONYMS ));
		
		return station;
	}
}
