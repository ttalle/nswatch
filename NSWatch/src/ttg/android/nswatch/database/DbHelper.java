package ttg.android.nswatch.database;

import ttg.android.nswatch.database.contract.DepartureContract;
import ttg.android.nswatch.database.contract.StationContract;
import ttg.android.nswatch.database.contract.SubscriptionContract;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

	// If you change the database schema, you must increment the database version.
	public static final int DATABASE_VERSION = 2;
	public static final String DATABASE_NAME = "nswatch.db";

	public static final String[] SQL_CREATE_TABLES = {
		StationContract.CREATE_TABLE,
		DepartureContract.CREATE_TABLE,
		SubscriptionContract.CREATE_TABLE
	};

	public static final String[] SQL_UPDATE_COMMANDS = { 
		  null // Dummy ignored version
		, "DROP TABLE departure" // V1 -> V2
		, null // V2 -> V3
		, null // V3 -> V4
		, null // V4 -> V5
		, null // V5 -> V6
		, null // V6 -> V7
		, null // V7 -> V8
	};

	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		for (int i = 0; i < SQL_CREATE_TABLES.length; ++i)
			if( null != SQL_CREATE_TABLES[i] )
				db.execSQL(SQL_CREATE_TABLES[i]);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		for (int i = oldVersion; i < newVersion; ++i)
			if( null != SQL_UPDATE_COMMANDS[i] )
				db.execSQL(SQL_UPDATE_COMMANDS[i]);

		onCreate(db);
	}
}
