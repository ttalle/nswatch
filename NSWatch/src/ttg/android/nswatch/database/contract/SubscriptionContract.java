package ttg.android.nswatch.database.contract;

import android.provider.BaseColumns;

public class SubscriptionContract implements BaseColumns {
	public static final String TABLE_NAME = "subscription";
	public static final String CREATE_TABLE = "CREATE  TABLE  IF NOT EXISTS 'subscription' ('_id' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'widget' INTEGER NOT NULL , 'station' TEXT NOT NULL )";

	public static final String COLUMN_NAME_WIDGETID = "widget";
	public static final String COLUMN_NAME_STATIONID = "station";

    public static final int COLUMN_INDEX_ID = 0;
    public static final int COLUMN_INDEX_WIDGET = 1;
    public static final int COLUMN_INDEX_STATION = 2;
}
