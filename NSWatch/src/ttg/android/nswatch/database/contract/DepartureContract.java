package ttg.android.nswatch.database.contract;

import android.provider.BaseColumns;

public class DepartureContract implements BaseColumns {
	/*
	 * CREATE TABLE IF NOT EXISTS 'departure' (
	'_id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE,
	'station' TEXT NOT NULL,
	'departure' NUMERIC,
	'delay' TEXT,
	'delaytext' TEXT,
	'destination' TEXT NOT NULL,
	'traintype' TEXT NOT NULL,
	'route' TEXT,
	'transporter' TEXT NOT NULL,
	'track' TEXT NOT NULL,
	'trackchange' INTEGER NOT NULL DEFAULT 0,
	'tip' TEXT,
	'notes' TEXT )
	 */
	
	public static final String TABLE_NAME = "departure";
	public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS 'departure' ( '_id' INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, 'station' TEXT NOT NULL, 'departure' NUMERIC, 'realdeparture' NUMERIC , 'delay' TEXT, 'delaytext' TEXT, 'destination' TEXT NOT NULL, 'traintype' TEXT NOT NULL, 'route' TEXT, 'transporter' TEXT NOT NULL, 'track' TEXT NOT NULL, 'trackchange' INTEGER NOT NULL DEFAULT 0, 'tip' TEXT, 'notes' TEXT )";

	public static final String COLUMN_NAME_STATION = "station";
	public static final String COLUMN_NAME_DEPARTURE = "departure";
	public static final String COLUMN_NAME_REALDEPARTURE = "realdeparture";
	public static final String COLUMN_NAME_DELAY = "delay";
	public static final String COLUMN_NAME_DELAYTEXT = "delaytext";
	public static final String COLUMN_NAME_DESTINATION = "destination";
	public static final String COLUMN_NAME_TRAINTYPE = "traintype";
	public static final String COLUMN_NAME_ROUTE = "route";
	public static final String COLUMN_NAME_TRANSPORTER = "transporter";
	public static final String COLUMN_NAME_TRACK = "track";
	public static final String COLUMN_NAME_TRACKCHANGE = "trackchange";
	public static final String COLUMN_NAME_TIP = "tip";
	public static final String COLUMN_NAME_NOTES = "notes";

    public static final int COLUMN_INDEX_ID = 0;
    public static final int COLUMN_INDEX_STATION = 1;
    public static final int COLUMN_INDEX_DEPARTURE = 2;
    public static final int COLUMN_INDEX_REALDEPARTURE = 3;
    public static final int COLUMN_INDEX_DELAY = 4;
    public static final int COLUMN_INDEX_DELAYTEXT = 5;
    public static final int COLUMN_INDEX_DESTINATION = 6;
    public static final int COLUMN_INDEX_TRAINTYPE = 7;
    public static final int COLUMN_INDEX_ROUTE = 8;
    public static final int COLUMN_INDEX_TRANSPORTER = 9;
    public static final int COLUMN_INDEX_TRACK = 10;
    public static final int COLUMN_INDEX_TRACKCHANGE = 11;
    public static final int COLUMN_INDEX_TIP = 12;
    public static final int COLUMN_INDEX_NOTES = 13;
	
}
