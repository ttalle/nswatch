package ttg.android.nswatch.database.contract;

import android.provider.BaseColumns;


public class StationContract implements BaseColumns {
	public static final String TABLE_NAME = "station";
	public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS 'station' ('_id' INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL UNIQUE, 'stationid' TEXT NOT NULL , 'type' TEXT NOT NULL , 'nameshort' TEXT NOT NULL , 'name' TEXT NOT NULL , 'namelong' TEXT NOT NULL , 'country' TEXT NOT NULL , 'latitude' DOUBLE NOT NULL , 'longitude' DOUBLE NOT NULL , 'synonyms' TEXT)";

	/*
	"_id" INTEGER PRIMARY KEY  NOT NULL  UNIQUE  check(typeof("_id") = 'integer') ," +
	"stationid" TEXT NOT NULL , 
	"type" TEXT NOT NULL , 
	"nameshort" TEXT NOT NULL , 
	"name" TEXT NOT NULL , 
	"namelong" TEXT NOT NULL , 
	"country" TEXT NOT NULL , 
	"latitude" DOUBLE NOT NULL , 
	"longitude" DOUBLE NOT NULL , 
	 "synonyms" TEXT)
	*/
	
	public static final String COLUMN_NAME_STATIONID = "stationid";
	public static final String COLUMN_NAME_TYPE = "type";
	public static final String COLUMN_NAME_NAMESHORT = "nameshort";
	public static final String COLUMN_NAME_NAME = "name";
	public static final String COLUMN_NAME_NAMELONG = "namelong";
	public static final String COLUMN_NAME_COUNTRY = "country";
	public static final String COLUMN_NAME_LATITUDE = "latitude";
	public static final String COLUMN_NAME_LONGITUDE = "longitude";
	public static final String COLUMN_NAME_SYNONYMS = "synonyms";

    public static final int COLUMN_INDEX_ID = 0;
    public static final int COLUMN_INDEX_STATIONID = 1;
    public static final int COLUMN_INDEX_TYPE = 2;
    public static final int COLUMN_INDEX_NAMESHORT = 3;
    public static final int COLUMN_INDEX_NAME = 4;
    public static final int COLUMN_INDEX_NAMELONG = 5;
    public static final int COLUMN_INDEX_COUNTRY = 6;
    public static final int COLUMN_INDEX_LATITUDE = 7;
    public static final int COLUMN_INDEX_LONGITUDE = 8;
    public static final int COLUMN_INDEX_SYNONYMS = 9;

	
}
