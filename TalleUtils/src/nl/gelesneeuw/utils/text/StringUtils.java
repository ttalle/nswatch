package nl.gelesneeuw.utils.text;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class StringUtils {

	public static List<String> split( String value, String delimiter ) {
//		List<String> result = null;
		
		if( null == value || value.isEmpty() )
			return new ArrayList<String>();
		
		return Arrays.asList( value.split(delimiter) );
		
	}
	
	public static String join( Iterable<String> s, String delimiter ) {
		if (s == null  ) 
			return null;
		
	    Iterator<String> iter = s.iterator();

	    if( !iter.hasNext() )
	    	return "";

	    StringBuilder builder = new StringBuilder(iter.next());
	    
	    while( iter.hasNext() )
	    {
	        builder.append(delimiter).append(iter.next());
	    }
	    return builder.toString();
	}

	public static String join( String[] s, String delimiter ) {
		if (s == null  ) 
			return null;
		
	    if( s.length < 1 )
	    	return "";

	    StringBuilder builder = new StringBuilder(s[0]);
	    
	    for( int i = 1; i < s.length; ++i )
	    {
	        builder.append(delimiter).append(s[i]);
	    }
	    
	    return builder.toString();
	}
}
