* Proxy
V ORM Implemenatie:
	http://ormlite.com/javadoc/ormlite-core/doc-files/ormlite_2.html#SEC7

* App

- Swipen tussen tabs (dep/stat)
V Stations sorteren op lange-naam
V Zoeken/auto completion bij stations
V Selectie station bij departures
V Klikken op station -> departures
V Niet geabonneerd station aanklikken -> eenmalig ophalen + tonen gegevens
- Settings: update frequentie?
- Improve location based on http://android-developers.blogspot.nl/2011/06/deep-dive-into-location.html

* Widget

V Stations boven aan de widget (moet nog bij ListView)
V Stationsselectie: zoeken / auto complete
V Klikken -> updaten (of keuze)
- Letter/achtergrond kleur
- Filteren van bestemmingen
V Lijnen tussen vertrektijden



* Notes
- Jackson Maven repo:
	http://repo1.maven.org/maven2/com/fasterxml/jackson/core/
	