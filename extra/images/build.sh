#!/bin/sh

OUT_PATH="."
NODPI_PATH="$OUT_PATH/res/drawable"

LDPI_PATH="$OUT_PATH/res/drawable-ldpi"
LDPI_SIZE="37.5%"

MDPI_PATH="$OUT_PATH/res/drawable-mdpi"
MDPI_SIZE="50%"

HDPI_PATH="$OUT_PATH/res/drawable-hdpi"
HDPI_SIZE="75%"

XHDPI_PATH="$OUT_PATH/res/drawable-xhdpi"
XHDPI_SIZE="100%"


mkdir -p "$NODPI_PATH"
mkdir -p "$LDPI_PATH"
mkdir -p "$MDPI_PATH"
mkdir -p "$HDPI_PATH"
mkdir -p "$XHDPI_PATH"


for ARG in $*
do
	echo "Converting: $ARG"
	
	BASENAME=`basename "$ARG"`
	
	cp "$ARG" "$NODPI_PATH/$BASENAME"
	
	echo "... $LDPI_SIZE: $LDPI_PATH/$BASENAME"
	convert "$ARG" -scale "$LDPI_SIZE" "$LDPI_PATH/$BASENAME"
	
	echo "... $MDPI_SIZE $MDPI_PATH/$BASENAME"
	convert "$ARG" -scale "$MDPI_SIZE" "$MDPI_PATH/$BASENAME"
	
	echo "... $HDPI_SIZE $HDPI_PATH/$BASENAME"
	convert "$ARG" -scale "$HDPI_SIZE" "$HDPI_PATH/$BASENAME"
	
	echo "... $XHDPI_SIZE: $XHDPI_PATH/$BASENAME"
	convert "$ARG" -scale "$XHDPI_SIZE" "$XHDPI_PATH/$BASENAME"
	
	echo "Done!"

done
